import manage_database as md
import json
import os
import re

from get_data import log

#Annahme: Hashtag endet immer auf bestimmte Zeichen
#Finde alle Hashtags in einem Text
def findallHashtags(text):
    hashtags = re.findall(r"#(\w+)", text.lower())
    return hashtags
    
"""     isHashtag = False
    hashtags = []
    start_position = 0
    end_position = 0
    possibleHashtagEndings = [" ", ".", ",", "!", "?", ":", ";", ")", "(", '"', "\n"]
    for position, letter in enumerate(text):
        if isHashtag:
            if letter in possibleHashtagEndings or letter.isalnum() == False:
                isHashtag = False
                end_position = position
                hashtag = text[start_position:end_position]
                hashtags.append(hashtag)
            elif letter == "#": #new hashtag starts directly
                end_position = position
                hashtag = text[start_position:end_position]
                start_position = position
                isHashtag = True
                hashtags.append(hashtag)
            elif position == len(text)-1: #last letter of text
                end_position = position+1
                hashtag = text[start_position:end_position]
                if letter == '…':
                hashtags.append(hashtag)
        else:
            if letter == "#":
                isHashtag = True
                start_position = position """

def addGeoCoordinates():
    logfile = "log/geomapping/getGeoLocationCoordinates.log"
    con = md.setupConnection()

    for filename in os.listdir("data"):
        if filename.endswith(".json"):
            log(f"Processing {filename}",logfile)
            with open(f"data/{filename}", "r") as json_file:
                data = json.load(json_file)
                counters = {"geo_coordinates_found":0,"geo_bbox_found":0,"geo_not_found":0, "coordinates_added":0}
                for loop in data:
                    for tweet in loop["data"]:
                        tweet_id = tweet["id"]
                        lat, lon = None, None
                        if "geo" in tweet.keys():
                            geo_id = tweet["geo"]["place_id"]
                            if "coordinates" in tweet["geo"].keys():
                                coordinates = tweet["geo"]["coordinates"]["coordinates"]
                                lat = coordinates[1]
                                lon = coordinates[0]
                                counters["geo_coordinates_found"] += 1
                                log(f"Found coordinates for tweet {tweet_id}",logfile)
                            else:
                                if "places" in loop["includes"].keys():
                                    for geo in loop["includes"]["places"]:
                                        if geo["id"] == geo_id:
                                            bbox = geo["geo"]["bbox"]
                                            lat = (bbox[1] + bbox[3])/2
                                            lon = (bbox[0] + bbox[2])/2
                                            counters["geo_bbox_found"] += 1
                                            log(f"Found bbox for tweet {tweet_id}",logfile)
                            if lat == None and lon == None:
                                counters["geo_coordinates_found"] += 1
                                log(f"Found no coordinates for tweet {tweet_id}",logfile)
                            else:
                                abroad = md.checkIfLoactionIsAbroad(con,geo_id)
                                lines_inserted = md.insertTweetCoordinatesIntoLocations(con,geo_id,lat,lon,abroad)
                                counters["coordinates_added"] += lines_inserted
                
                print("------------------------------------------")
                log("In total, {} tweets were processed".format(counters["geo_coordinates_found"]+counters["geo_bbox_found"]+counters["geo_not_found"]),logfile)
                log("In total, {} tweets had coordinates".format(counters["geo_coordinates_found"]),logfile)
                log("In total, {} tweets had bbox".format(counters["geo_bbox_found"]),logfile)
                log("In total, {} tweets had no location".format(counters["geo_not_found"]),logfile)
                log("In total, {} coordinates were added to the database".format(counters["coordinates_added"]),logfile)
                log(f"Finished processing {filename}\n",logfile)

def fillDatabase():
    # keyword = "#FeesMustFall"
    # start_time = '2015-10-12'
    # end_time = '2015-10-30'

    #filename = f"{keyword[1:]}_{start_time}_{end_time}"
    logfile = "log/fill_database.log"

    for filename in os.listdir("data"):
        if filename.endswith(".json"):
            if filename != 'FeesMustFall_2015-10-12_2015-10-30.json':
                log(f"Processing {filename}",logfile)
                with open(f"data/{filename}", "r") as json_file:
                    data = json.load(json_file)
                    counters = {"tweets":0,"users":0,"tweet_errors":0, "user_errors":0, "geo":0, "geo_errors":0, "hashtags":0, "hashtag_errors":0, "hashtag2tweet":0, "hashtag2tweet_errors":0}

                    for loop in data:
                        for tweet in loop["data"]:
                            #extract hashtags from tweet
                            hashtags = findallHashtags(tweet["text"])
                            for hashtag in hashtags:
                                hashtag_added, hashtag_already_existed = md.addHashtag(hashtag)
                                counters["hashtags"] += hashtag_added
                                counters["hashtag_errors"] += hashtag_already_existed
                                
                                hashtag2tweet, hashtag2tweet_already_existed = md.addHashtagToTweetConnection(tweet["id"],hashtag)
                                counters["hashtag2tweet"] += hashtag2tweet
                                counters["hashtag2tweet_errors"] += hashtag2tweet_already_existed


                            if "geo" in tweet.keys():
                                geo_id = tweet["geo"]["place_id"]
                            else:
                                geo_id = None

                            if "referenced_tweets" in tweet.keys():
                                type = tweet["referenced_tweets"][0]["type"]
                                reference_id = tweet["referenced_tweets"][0]["id"]
                            else:
                                type = "original"
                                reference_id = None

                            tweet_added, tweet_already_existed = md.addTweet(tweet["id"],tweet["text"],tweet["created_at"],
                                        tweet["lang"], type, reference_id, tweet["public_metrics"]["retweet_count"],
                                        tweet["public_metrics"]["reply_count"],tweet["public_metrics"]["like_count"],
                                        tweet["public_metrics"]["quote_count"],tweet["public_metrics"]["impression_count"],tweet["author_id"],geo_id)
                            counters["tweets"] += tweet_added
                            counters["tweet_errors"] += tweet_already_existed

                        for user in loop["includes"]["users"]:
                            if "location" in user.keys():
                                location = user["location"]
                            else:
                                location = None
                            
                            if user["verified"] == "false":
                                verified = 0
                            else: 
                                verified = 1


                            user_added, user_already_existed = md.addUser(user["id"],user["username"],user["description"],user["created_at"],user["name"],location,
                                    user["verified"], user["public_metrics"]["followers_count"],user["public_metrics"]["following_count"],
                                    user["public_metrics"]["tweet_count"],user["public_metrics"]["listed_count"])
                            counters["users"] += user_added
                            counters["user_errors"] += user_already_existed
                        if "places" in loop["includes"].keys():
                            for geo in loop["includes"]["places"]:
                                geo_added,geo_already_existed = md.addGeo(geo["id"],geo["name"],geo["country"])
                                counters["geo"] += geo_added
                                counters["geo_errors"] += geo_already_existed

                        
                        print(f"Added {counters['tweets']} tweets to database, {counters['tweet_errors']} tweets already existed")
                        print(f"Added {counters['users']} users to database, {counters['user_errors']} users already existed")
                        print(f"Added {counters['geo']} geo locations to database, {counters['geo_errors']} geo locations already existed")
                        print(f"Added {counters['hashtags']} hashtags to database, {counters['hashtag_errors']} hashtags already existed")
                    
                    print("------------------------------------------")
                    log(f"In total, {counters['tweets']} tweets were added to the database, {counters['tweet_errors']} tweets already existed",logfile)
                    log(f"In total, {counters['users']} users were added to the database, {counters['user_errors']} users already existed",logfile)
                    log(f"In total, {counters['geo']} geo locations were added to the database, {counters['geo_errors']} geo locations already existed",logfile)
                    log(f"In total {counters['hashtags']} hashtags were added to the database, {counters['hashtag_errors']} hashtags already existed",logfile)
                    log(f"Finished processing {filename}",logfile)

def validateAddingCoordinates():
    locations = md.fetchAllLocations()
    geos = md.fetchAllGeoLocations()
    counter = 0
    for index,location in enumerate(locations):
        location_id = location[0]
        found = False
        for geo in geos:
            if location_id == geo[0]:
                print(f"{index}: {location_id} found in geos: {geo[0]}")
        if found == False:
            counter += 1
            print(f"{index}: {location_id} not found in geos")

def moveRedundantHashtagToTweetContent():
    con = md.setupConnection()
    all_hashtags2Tweets = md.fetchAllHashtagsToTweets()
    stats = {"added":0,"already_existed":0}
    for hashtag2tweet in all_hashtags2Tweets:
        hashtag = hashtag2tweet[0]
        tweet_id = hashtag2tweet[1]
        completed = hashtag2tweet[2]
        result = md.insertIntoHashtag2Tweet(con,hashtag,tweet_id,completed)
        if result == 1:
            stats["added"] += 1
        elif result == 0:
            stats["already_existed"] += 1
        
    print(f"Added {stats['added']} hashtags to tweets, {stats['already_existed']} hashtags already existed")

def insertLitEvents():
    con = md.setupConnection("offline_events.db")
    id = 3
    date = 20151023
    location = "Bloemfontein"
    lat = -29.120424
    lon = 26.216335
    description = "protests of UFS and CUT students"

    md.insertEntryIntoEventsReduced(con, id, date, location, lat, lon, description)
        


if __name__ == '__main__':
    #addGeoCoordinates()
    #validateAddingCoordinates()
    #moveRedundantHashtagToTweetContent()
    insertLitEvents()