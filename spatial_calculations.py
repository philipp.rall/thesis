import numpy as np
from scipy.spatial import Voronoi, voronoi_plot_2d
import matplotlib.pyplot as plt
import fiona
from shapely.geometry import shape
import geopandas as gpd
import shapely as sh
import os
import json
import analysis as ana
import pyproj
from shapely.ops import transform
from functools import partial

def getUniversityLocations(type="universities"):
    with open("preprocessing/uni_keys.csv","r") as f:
        lines = f.readlines()
        uni_locations = []
        if type == "specialLocations":
            for line in lines[:-1]:
                uni_locations.append([float(line.split(",")[5]),float(line.split(",")[4])])
        elif type == "universities":
            for line in lines[:26]:
                uni_locations.append([float(line.split(",")[5]),float(line.split(",")[4])])
        elif type == "uni_cities":
            num_of_unis_per_city = {}
            for index,line in enumerate(lines[:-1]):
                city = line.split(",")[2]
                if city not in num_of_unis_per_city.keys():
                    num_of_unis_per_city.update({city:[index]})
                else:
                    num_of_unis_per_city[city].append(index)
            for city in num_of_unis_per_city.keys():
                if len(num_of_unis_per_city[city]) > 1:
                    unis_in_city = []
                    for uni_index in num_of_unis_per_city[city]:
                        unis_in_city.append([float(lines[uni_index].split(",")[5]),float(lines[uni_index].split(",")[4])])
                    #calculate middle point of unis_in_city
                    uni_locations.append(np.mean(unis_in_city,axis=0))
                else:
                    uni_locations.append([float(lines[num_of_unis_per_city[city][0]].split(",")[5]),float(lines[num_of_unis_per_city[city][0]].split(",")[4])])

    return np.array(uni_locations)

def splitListAtMinusOne(region):
    region1 = []
    region2 = []
    before_minus_one = True
    for region_v in region:
        if region_v != -1 and before_minus_one:
            region1.append(region_v)
        elif region_v != -1 and not before_minus_one:
            region2.append(region_v)
        else:
            before_minus_one = False
    return region1,region2

def getRegionByPoint(voronoi,point):
    region_idx = voronoi.point_region[point]
    return voronoi.regions[region_idx]
    
def getPointCoordinates(point_index,type="universities"):
    uni_locations = getUniversityLocations(type)
    return uni_locations[point_index]

def getVertices(folder="universities"):
    with open(f"analysis/spatial/{folder}/voronoi_vertices_manuallyAdded.txt","r") as f:
        lines = f.readlines()
        vertices = []
        for line in lines:
            vertices.append([float(line.split(":")[1].split(",")[0]),float(line.split(":")[1].split(",")[1])])
    return np.array(vertices)

def getRegions(folder="universities"):
    polygons = []
    region_indices = []
    for file in os.listdir(f"analysis/spatial/{folder}/geojsons"):
        with open(f"analysis/spatial/{folder}/geojsons/{file}","r") as f:
            lines = f.read()
            polygons.append(sh.from_geojson(lines))
            region_indices.append(file.split(".")[0])
    return polygons, region_indices

    ## Old polygons without national borders
    # with open(f"analysis/spatial/{folder}/voronoi_regions_manuallyAdded.txt","r") as f:
    #     lines = f.readlines()
    #     regions = []
    #     for line in lines:
    #         if line.split(":")[1].strip() == "[]":
    #             regions.append([])
    #             continue
    #         region_array = line.split(":")[1].strip()[1:-1]
    #         new_array = [int(x) for x in region_array.split(",")]
    #         regions.append(new_array)
    #     return regions

def drawRidgeEdges(ax,type):
    length_of_line = 5
    uni_locations = getUniversityLocations(type)
    voronoi = Voronoi(uni_locations)
    #ax.plot([voronoi.vertices[21][0],0],[0,voronoi.vertices[21][1]],color='g',linewidth=1,label="Ridge Edge")

    for ridge_point in voronoi.ridge_points:
        #print(f"Ridge Point: {ridge_point}")
        region1 = getRegionByPoint(voronoi,ridge_point[0])
        region2 = getRegionByPoint(voronoi,ridge_point[1])
        #print(f"Regions: {region1} and {region2}")
        if -1 in region1 and -1 in region2:
            print(f"Line between {ridge_point[0]} and {ridge_point[1]} needed")
            print(f"Regions: {region1} and {region2}")
            common_vertex = [x for x in getRegionByPoint(voronoi,ridge_point[0]) if x in getRegionByPoint(voronoi,ridge_point[1]) and x != -1]
            x1,y1 = getPointCoordinates(ridge_point[0],type)
            x2,y2 = getPointCoordinates(ridge_point[1],type)
            start_point = voronoi.vertices[common_vertex[0]]
            print(f"Start Point: {start_point}")
            direction_of_perpendicular = np.array([-y2+y1, x2-x1])
            direction_of_perpendicular = direction_of_perpendicular / np.linalg.norm(direction_of_perpendicular)
            print(f"Direction of perpendicular: {direction_of_perpendicular}")
            #plot line with length 10 from start_point in direction of perpendicular
            x_list_green = [start_point[0],start_point[0]+length_of_line*direction_of_perpendicular[0]]
            y_list_green = [transformYCoordinate(start_point[1]),transformYCoordinate(start_point[1]+length_of_line*direction_of_perpendicular[1])]
            print(f"New green Vertex: {x_list_green[1]},{transformYCoordinate(y_list_green[1])}")
            x_list_blue = [start_point[0],start_point[0]-length_of_line*direction_of_perpendicular[0]]
            y_list_blue =  [transformYCoordinate(start_point[1]),transformYCoordinate(start_point[1]-length_of_line*direction_of_perpendicular[1])]
            print(f"New blue Vertex: {x_list_blue[1]},{transformYCoordinate(y_list_blue[1])}")

            
            #Manuell entschieden welche Vertices wichtig sind
            if common_vertex[0] in [30,20]:
                print(f"New Vertex: {x_list_blue[1]},{y_list_blue[1]}")
            elif common_vertex[0] == 21:
                print(f"New Vertex: {x_list_green[1]},{y_list_green[1]}")
                
            ax.plot(x_list_green,y_list_green,color='g',linewidth=1)
            ax.plot(x_list_blue,y_list_blue,color='b',linewidth=1)

            print("Common Vertex: ",common_vertex)
            print("------------------")

def transformYCoordinate(y):
    if type(y) == list:
        transformed_y = []
        for y_value in y:
            transformed_y.append(transformYCoordinate(y_value))
        return transformed_y
    BBox = (15.82,35.367,-21.943,-35.343)
    return BBox[3] - y + BBox[2]

def drawSouthAfricanMap(withAnnotations=False,folder="universities",withRegionAnnotations=False,savePath=None):
    locations = getUniversityLocations(folder)
    region_colour = 'r'
    BBox = (15.82,35.367,-21.653,-35.343) #-21.943
    raw_map = plt.imread('/Users/philipp/Documents/GitHub/thesis/preprocessing/visualize/map.png')
    fig, ax = plt.subplots(figsize = (8,7))
    for location in locations:
        lat = BBox[3] - location[1] + BBox[2]
        ax.scatter(location[0],lat,zorder=1,alpha=1,c='b',s=15)
    ax.set_title(f'{folder} Locations in South Africa')
    ax.set_xlim(BBox[0],BBox[1])
    ax.set_ylim(BBox[2],BBox[3])
    ax.imshow(raw_map, zorder=0, extent = BBox, aspect= 'equal')

    #vertices = getVertices(folder=type)
    regions,regionIndices = getRegions(folder=folder)
    for index,region in enumerate(regions):
        if type(region) == sh.MultiPolygon:
            for geom in region.geoms:
                xs,ys = geom.exterior.xy
                if list(xs) == [] or list(ys) == []:
                    continue
                ys = transformYCoordinate(list(ys))
                ax.plot(xs,ys,"b")
            # Annotation stimmt nicht
            if withRegionAnnotations:
                ax.annotate(regionIndices[index],(np.mean(xs),np.mean(ys)), color=region_colour)
        else: 
            xs,ys = region.exterior.xy
            if list(xs) == [] or list(ys) == []:
                continue
            ys = transformYCoordinate(list(ys))
            ax.plot(xs,ys,"b")
            #Annotierung stimmt nicht
            if withRegionAnnotations:
                ax.annotate(regionIndices[index],(np.mean(xs),np.mean(ys)), color=region_colour)
        
        #old regions without national borders
        # if -1 not in region and region != []:
        #     polygon = vertices[region]
        #     polygon = np.vstack([polygon, polygon[0]])
        #     polygon[:,1] = BBox[3] - polygon[:,1] + BBox[2]
        #     xs, ys = zip(*polygon)
        #     ax.plot(xs,ys,color=region_colour,linewidth=1)
        #     if withRegionAnnotations:
        #         ax.annotate(regionIndex,(np.mean(xs),np.mean(ys)), color=region_colour)

    if withAnnotations:
        #draw all vertices with index next to vertex
        # for index,vertex in enumerate(vertices):
        #     ax.scatter(vertex[0],BBox[3] - vertex[1] + BBox[2],zorder=1,alpha=0.2,c='b',s=10)
        #     ax.annotate(index,(vertex[0],BBox[3] - vertex[1] + BBox[2]))

        #draw all university locations with index next to location
        for index,location in enumerate(locations):
            ax.scatter(location[0],BBox[3] - location[1] + BBox[2],zorder=1,alpha=0.2,c='r',s=10)
            #annote in red
            ax.annotate(index,(location[0],BBox[3] - location[1] + BBox[2]), color='r')

    if savePath != None:
        #plt.savefig(savePath)
        plt.show()
    else:
        plt.savefig(f"analysis/spatial/{folder}/voronoi_university_locations.pdf")
        plt.show()

def calculateVoronoiData(CreateFiles=False, type="univerities"):
    #get all Unis
    uni_locations = getUniversityLocations(type=type)
    folder = type
    voronoi = Voronoi(uni_locations)
    if CreateFiles:
    #write uni_locations
        with open(f"analysis/spatial/{folder}/university_locations","w") as f:
            for index,location in enumerate(uni_locations):
                f.write(f"University {index}: {location[0]},{location[1]}\n")

        with open(f"analysis/spatial/{folder}/voronoi_vertices","w") as f:
            for index,vertex in enumerate(voronoi.vertices):
                f.write(f"Vertex {index}: {vertex[0]},{vertex[1]}\n")
    
        # #write regions
        with open(f"analysis/spatial/{folder}/voronoi_regions","w") as f:
            for index,region in enumerate(voronoi.regions):
                f.write(f"Region {index}: {region}\n")

        #write ridge_points
        with open(f"analysis/spatial/{folder}/voronoi_ridge_points","w") as f:
            for index,ridge_point in enumerate(voronoi.ridge_points):
                f.write(f"Ridge Point {index}: {ridge_point}\n")
    
        #write ridge_vertices
        with open(f"analysis/spatial/{folder}/voronoi_ridge_vertices","w") as f:
            for index,ridge_vertex in enumerate(voronoi.ridge_vertices):
                f.write(f"Ridge Vertex {index}: {ridge_vertex}\n")

        #write point region
        with open(f"analysis/spatial/{folder}/voronoi_point_region","w") as f:
            for index,point_region in enumerate(voronoi.point_region):
                f.write(f"Point Region {index}: {point_region}\n")


    fig, ax = plt.subplots(figsize = (8,7))
    fig = voronoi_plot_2d(voronoi)
    plt.show()

#old function not needed anymore
def createGoogleMyMapsFile(folder):
    vertices = getVertices(folder)
    regions,_ = getRegions(folder)
    with open(f"analysis/spatial/{folder}/googleMyMaps.csv","w") as f:
        f.write("ID,WKT\n")
        for index,region in enumerate(regions):
            if region != []:
                polygon = vertices[region]
                polygon = np.vstack([polygon, polygon[0]])
                #polygon[:,1] = transformYCoordinate(polygon[:,1])
                vertex_string = "POLYGON (("
                for vertex in polygon:
                    vertex_string+=(f"{vertex[0]} {vertex[1]},")
                vertex_string = vertex_string[:-1]
                vertex_string+=f"))"
                f.write(str(index)+',"'+vertex_string+'"\n')

def getStatsOfClusterId(id):
    with open("analysis/anas/ana1/uni_metadata.csv","r") as f:
        lines = f.readlines()
        for line in lines[1:]:
            id_inFile = line.split(",")[0]
            if id_inFile == str(id):
                return line.replace("\n","")

def convertGeoJsonToGoogleMapsCSV(folder):
    with open(f"analysis/spatial/{folder}/googleMyMapsWithStats.csv","w") as f:
        f.write("clusterIdentifier,clusterName,numberOfProtests,dateFirstProtest,dateLastProtest,numberOfGeoImpactTweets,numberOfAllTweets,numberOfUsersGeoImpactTweets,numberOfAllUsers,dateFirstTweetGeoImpact,dateFirstTweetAll,dateLastTweetGeoImpact,dateLastTweetAll,dateMaxTweetsGeoImpact,dateMaxTweetsAll,originalTweetRatioGeoImpact,originalTweetRatioAll,averageTweetsPerGeoImpactUser,averageTweetsPerAllUser,fractionOfUsersSupportingFromOutside,fractionOfUsersSupportingFromAbroad,fractionUniqueUserTweets,fractionUniqueGeoTweets,fractionUniqueUserUsers,fractionUniqueGeoUsers,WKT\n")
        for file in os.listdir(f"analysis/spatial/{folder}/geojsons"):
            with open(f"analysis/spatial/{folder}/geojsons/{file}","r") as f2:
                lines = f2.read()
                polygon = sh.from_geojson(lines)  
                id = file.split(".")[0]
                print(id)
                if id != "17":
                    f.write(getStatsOfClusterId(id)+',"'+sh.to_wkt(polygon)+'"\n')

def plotPolygonOnSouthAfrica(multipolygon,polygon,title):
    fig, axs = plt.subplots()
    axs.set_aspect('equal', 'datalim')
    for geom in multipolygon.geoms:    
        xs, ys = geom.exterior.xy    
        axs.plot(xs, ys, c='r')
    if type(polygon) == sh.MultiPolygon:
        for geom in polygon.geoms:
            axs.plot(*geom.exterior.xy,"b")
    else: 
        axs.plot(*polygon.exterior.xy,"b")
    axs.set_title(title)
    plt.show()
    
#old function not needed anymore
def adjustPolygonsBasedOnBorder(type):
    with open("preprocessing/visualize/southAfrica.json","r") as f:
        lines = f.read()
        southAfricanBorder = sh.from_geojson(lines)
        multipolygon = southAfricanBorder.geoms[0]

        vertices = getVertices(type)
        regions,_ = getRegions(type) #regions are now polygons with borders
        intersections = []
        for index,region in enumerate(regions):
            polygon = sh.geometry.Polygon(vertices[region])
            intersection = sh.intersection(multipolygon,polygon)
            with open(f"analysis/spatial/{type}/geojsons/{index}.json","w") as f:
                f.write(sh.to_geojson(intersection))
            #plotPolygonOnSouthAfrica(multipolygon,intersection,f"Region {index} contained: {contained}")
        #print(intersections)

def calculateOverlappingOfClusterAndCity(cityName, clusterWithUniInCity):
    filename = f"analysis/spatial/metropolitan_areas/{cityName.lower()}.json"
    with open(filename,"r") as f:
        city_polygon = sh.from_geojson(f.read())
    geo_clusters = ana.Geo_Clusters('universities')
    intersection_areas = {}
    sumOfIntersectionAreas = 0
    for cluster in geo_clusters.geo_clusters:
        if int(cluster.identifier) in clusterWithUniInCity:
            if city_polygon.intersects(cluster.geometry):
                intersection_polygon = city_polygon.intersection(cluster.geometry)
                sumOfIntersectionAreas+=intersection_polygon.area
                intersection_areas.update({cluster.identifier:intersection_polygon.area})
    
    with open("analysis/spatial/metropolitan_areas/intersection_areas.csv","a") as f:
        for id in intersection_areas.keys():
            f.write(f"{id},{cityName},{intersection_areas[id]/sumOfIntersectionAreas}\n")

def calculateUniCircles():
    radius = 25000 #in meters
    with open("preprocessing/uni_keys.csv","r") as f:
        with open(f"analysis/spatial/uniCircles/circlesInWKT.csv","w") as f2:
            with open("analysis/spatial/uniCircles/nameMapping.csv","w") as f4:
                f2.write("ID,WKT\n")
                lines = f.readlines()
                for line in lines[:26]:
                    uni_key = line.split(",")[1]
                    uni_name = line.split(",")[3].strip()
                    f4.write(f"{uni_key},{uni_name}\n")
                    lat = line.split(",")[4].strip()
                    lon = line.split(",")[5].strip()
                    print(uni_key)
                    local_azimuthal_projection = "+proj=aeqd +units=m +lat_0={} +lon_0={}".format(
                        lat, lon
                    )
                    wgs84_to_aeqd = partial(
                        pyproj.transform,
                        pyproj.Proj("+proj=longlat +datum=WGS84 +no_defs"),
                        pyproj.Proj(local_azimuthal_projection),
                    )
                    aeqd_to_wgs84 = partial(
                        pyproj.transform,
                        pyproj.Proj(local_azimuthal_projection),
                        pyproj.Proj("+proj=longlat +datum=WGS84 +no_defs"),
                    )

                    uni_location = sh.geometry.Point(float(lon),float(lat))
                    point_transformed = transform(wgs84_to_aeqd, uni_location)
                    buffer = point_transformed.buffer(radius)
                    # Get the polygon with lat lon coordinates
                    circle_poly = transform(aeqd_to_wgs84, buffer)
                    f2.write(f'{uni_key},"{sh.to_wkt(circle_poly)}"\n')
                    with open(f"analysis/spatial/uniCircles/geojsons/{uni_key}.json","w") as f3:
                        f3.write(sh.to_geojson(circle_poly))

if __name__ == "__main__":
    #drawVoronoiDiagram(True,"university_and_specialLocations")
    #drawUniversityLocations(True)
    #drawRidgeEdges()
    #drawUniversityLocations(False,type="specialLocations",withRegionAnnotations=True,savePath=f"analysis/spatial/specialLocations_clusterLookup.pdf")
    #print(len(getUniversityLocations("uni_cities")))
    #calculateVoronoiData(False,"uni_cities")
    #createGoogleMyMapsFile("universities")
    #convertGeoJsonToGoogleMapsCSV("universities")
    calculateOverlappingOfClusterAndCity("pretoria",[22,26])
    calculateOverlappingOfClusterAndCity("johannesburg",[21,25])
    calculateOverlappingOfClusterAndCity("capetown",[1,2,3])
    calculateOverlappingOfClusterAndCity("durban",[6,14])
    #drawSouthAfricanMap(withAnnotations=False,folder="specialLocations",withRegionAnnotations=True,savePath=f"analysis/spatial/specialLocations_clusterLookup.pdf")