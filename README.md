# Masterthesis Philipp Rall
Online-Protest als Indikator für Offline-Protest? Die Verbreitung von #FeesMustFall in Südafrika - eine Twitter-Analyse

## Datenbanken
Die in der Masterarbeit analysierten Daten sind in zwei SQLite-Datenbanken zu finden und in folgenden Dateien abgelegt:
- `data.db`: Online-Protestdaten
- `offline_events.db`: Offline-Protestdaten

Relevant sind nur die in den ER-Diagrammen abgebildete Tabellen und Spalten, teilweise liegen leichte Namensunterschiede und zusätzliche Spalten vor. Zur einfachen Sichtung der Daten kann ein DB-Browser wie beispielsweise dieser kostenlose,quelloffene [SQLiteBrowser](https://sqlitebrowser.org/) verwendet werden.

## Wichtigste Python-Dateien
Die zentralen Python-Dateien der Masterarbeit liegen alle auf der höchsten Ebene der Ordnerstruktur. Daten, Ergebnisse, Logs,  Visualisierungen, etc. werden in Unterordnern weggeschrieben und von dort eingelesen.
### Datenverwaltung
- `get_data.py`: Diese Python-Datei beinhaltet den Code für das Abrufen der Twitter-Daten über die Twitter-API. Sie schreibt die Ergebnisse in den Ordner `data` als JSON-Dateien.
- `get_event_data.py`: Diese Python-Datei beinhaltet den Code für das Entpacken und Abspeichern der Protestdaten. 
- `fill_database.py`: In dieser Python-Datei steht der Code zum Einfügen von beispielsweise Twitter-Daten im JSON-Format in die Datenbankstruktur.
- `manage_database.py`: In dieser Python-Datei sind alle Funktionen für Datenbankzugriffe vom restlichen Coding abgekapselt.
### Vorverarbeitung
- `check_data.py`: Diese Python-Datei enthält einige Funktionen zum Überprüfen der abgerufenen Twitter-Daten auf Vollständigkeit und Sinnhaftigkeit.
- `preprocessing.py`: Diese Python-Datei enthält alle Vorverarbeitungsschritte außer der Zuordnung geographischer Koordinaten. Der Code für die Hashtag-Erweiterungen, die Zuordnung von lokalen Hashtags zu Universitäten, die Event-Relevanzfilterung und die Verschiebung von Usern aus Stadtzentren findet sich hier.
- `geolocation_mapping.py`: Diese Python-Datei beinhaltet den Code zum Parsen der Standortangaben von User-Profilen und die Konvertierung von Daten in ein für Google My Maps lesbares Format zur Visualisierung.
- `spatial_calculations.py`: Die Python-Datei beinhaltet den Code zur Voronoi- und Kreis-Clusterbildung sowie weitere geographische Berechnungen wie beispielsweise die Überlappungsanteile von Clustern für die Verschiebung von Usern aus Stadtzentren.
### Analyse
- `analysis.py`: Dies ist die zentrale Python-Datei zur Analyse der Online- und Offline-Protestdaten. Die wichtigsten Funktionen sind folgende:
    - `fillAnalysisTable(logfile, forCorrelation, clusterType)`: Diese Funktion generiert aus den Protestdaten für die Analyse in Excel eine `uni_metadata.csv`-CSV-Datei mit zahlreichen Metadaten je Cluster. Es muss beim Aufruf der Pfad einer Logdatei mitgegeben werden. Durch den Parameter `forCorrelation=True` lässt sich die Anzahl der Spalten auf die für die Korrelationsanalyse der zweiten Unterforschungsfrage relevanten Daten beschränken und die Datei `uni_metadata_regression.csv` wird generiert. Durch den Parameter `clusterType` muss die zu verwendende Clusterbildung angegeben werden. Für Voronoi-Clusterbildung muss `clusterType` den Wert „universities“, für Kreis-Clusterbildung den Wert „uniCircles“ annehmen.
    - `runAnalysis(logfile, thesisSheets, clusterType)`: Mit dem Aufruf dieser Funktion startet die Analyse. Es muss ebenfalls der Pfad zu einer Logdatei mitgegeben werden. Außerdem kann durch `thesisSheets=True` der Fokus auf die Analyse der Thesen der ersten Unterforschungsfrage gelegt werden. Die Zusammenfassung der Werte wird dann in der Datei `thesis_results.csv` weggeschrieben. Die Wahl der Clusterbildung erfolgt analog zur Funktion `fillAnalysisTable(logfile, forCorrelation, clusterType)` über den Paramter `clusterType`.
    - `analyse(...)`: In dieser Funktion steht der Code für die eigentliche Analyse der Protestdaten. Die Funktion ruft zur Visualisierung `visualizeOnlineOfflineOverTime(...)` oder `generateThesisSheet(...)` in `visualize.py` auf.
    - `calculateTweetsStats(...)`: Diese Funktion berechnet zahlreiche Eigenschaften der Online-Protestdaten.
    - `getDataPointsInCluster(...)`: Diese Funktion überprüft, welche Datenpunkte innerhalb eines Clusters liegen und schreibt die Ergebnisse in `withinFiles` weg. So können die Clusterzuordnungen zwischengespeichert werden und müssen nicht bei jeder Analyse erneut berechnet werden.

- `visualize.py`: Diese Python-Datei enthält den Code für die Visualisierungen der analysierten Protestdaten. Die wichtigsten Funktionen sind folgende:
    - `createStackedTweetsOverTimePlots(factor)`: Diese Funktion berechnet die gestapelten Liniendiagramme zur Visualisierung des Protestverlaufs aller Cluster wie in Abbildung 10 der Arbeit. Der angegebene `factor` beeinflusst die Wahl der Startzeitpunkte von Online-Protest, beim Standardwert von 0.25 entspricht die Wahl der in der Arbeit erklärten. Die Funktion kann direkt aufgerufen werden.
    - `visualizeOnlineOfflineOverTime(...)`: Diese Funktion wird von `analyse(...)` aufgerufen, um die Protestverläufe je Cluster zu visualisieren.
    - `generateThesisSheets(...)`: Diese Funktion wird bei `thesisSheets=True` von `analyse(...)` aufgerufen und visualisiert die Daten auf Thesen bezogen.

## Sonstige Ordnerstruktur
### analysis
Dieser Ordner enthält die Ergebnisse sämtlicher Analysen sowie deren Visualisierungen, die im Laufe der Bearbeitung der Masterarbeit entstanden sind. Die wichtigsten Unterordner sind:
- `spatial`: Hier liegen die errechneten Clusterbildungen, verwendet werden `universities` (Voronoi-Clusterbildung) und `uniCircles` (Kreis-Clusterbildung). 
- `anas`: Hier liegen die eigentlichen Analyseergebnisse untergliedert nach verschiedenen Analysezielen und Datenständen. Die Analysen aus der finalen Masterarbeit finden sich im Ordner `afMvmNewCorrectUserCast` für die ersten Unterforschungsfrage und in `uniCircles` für die zweite Unterforschungsfrage. Dort sind die `thesissheets` aller Cluster sowie die Ergebnis-CSV-Dateien `uni_metadata.csv` abgelegt. Auch für die Analyse zentrale Excel-Tabellen liegen dort (aber auch unter `ana1`).
- `withinChecks...`: In diesen Ordner liegen die zwischengespeicherten Ergebnisse der Berechnung, welche Datenpunkte in welchem Cluster liegen. Jeder dieser Ordner weist für jedes Cluster einen Unterordner mit vier Dateien auf: `offline_checked` für relevante Offline-Protestdaten, `online_geo` für geolokalisierte Tweets, `online_impact` für über Hashtags lokalisierte Tweets und `online_users` für über Profil-Standortangaben von Usern lokalisierte Tweets. Die Cluster sind intern durch Cluster-IDs benannt, die Zuordnung zu Universitäten ist durch `uni_keys.csv` (s.u.) oder `uni_metadata.csv` (s.o) möglich. Wie im Unterordner `anas` finden sich die in der finalen Masterarbeit verwendeten Daten in den Unterordnern mit den Endungen `AfMvmNewCorrectUserCast` und `UniCircles`.
### backups
Dieser Ordner enthält Backups der Datenbanken zu verschiedenen Zeitpunkten.
### data
In diesem Ordner liegen die eigentlichen Rohdaten der Tweets als JSON-Dateien. Die Dateien sind nach dem Hashtag-Suchbegriff und dem Suchzeitraum benannt.
### log
Dieser Ordner enthält alle während der Verarbeitung der Daten entstandenen Log-Dateien. Dies beinhaltet Log-Dateien der Datenerhebung, der Datenvorverarbeitung und Datenanalyse. Alle Logdateien besitzen ein einheitliches Format und werden durch die Funktion `log(...)` in `get_data.py` generiert.
### offline_data
In diesem Ordner liegen die Rohdaten der Proteste als CSV-Dateien sowie die Excel-Tabelle zur manuellen Sortierung der ACLED-Protestdaten.
### preprocessing
Dieser Ordner enthält Zwischenergebnisse aus der Datenvorverarbeitung wie beispielsweise der Relevanz- und Redundanzfilterung der Offline-Protestdaten, der Verschiebung von Usern aus Stadtzentren und diverse Visualisierungen von Zwischenergebnissen. Die wichtigsten Dateien in diesem Ordner sind:
- `city_keys.csv`: Diese Datei enthält eine Zuordnung von Städten, die in lokalen Hashtags vorkommen, zu geographischen Koordinaten.
- `uni_keys.csv`: Diese Datei beinhaltet die zentrale Zuordnung von Universitäten zu deren Abkürzungen, Städten, geographischen Koordinaten und der Anzahl an Studierenden.
- `hashtag_location_mapped_MANUALLY.csv`: Die Datei beinhaltet die finale und manuell überprüfte Zuordnung lokaler Hashtags zu Universitäten oder Städten. 
- `location_completion_rules.csv`: Die Datei enthält Regeln zur automatischen Zuordnung von Hashtag-Bestandteilen zu Universitäten basierend auf der Literatur.
- `truncated_completed.csv`: In dieser Datei sind vervollständigte Tweets, die durch die Twitter-API abgeschnitten wurden.

---
Bei Nachfragen oder Anmerkungen gerne unter ma_thesis@philipp-rall.de melden!