import requests, zipfile, io
import os, sqlite3 as sql
import manage_database as md


def getZipFile(date):
    url = f"http://data.gdeltproject.org/events/{date}.export.CSV.zip"
    r = requests.get(url)
    if r.status_code == 200:
        file = zipfile.ZipFile(io.BytesIO(r.content))
        file.extractall("offline_data/gdelt")
    else:
        print(r.status_code)

def convertToCsv(file):
    with open(f"offline_data/gdelt/{file}","r+", encoding="utf8") as f:
        lines = f.read()
        new_lines = lines.replace(",",";").replace("	",",")
        with open (f"offline_data/gdelt/{file[:8]}.csv","w", encoding="utf8") as w:
            w.write(new_lines)
    os.remove(f"offline_data/gdelt/{file}")

def importIntoDatabase():
    cmd_string = "sqlite3 offline_events.db -cmd '.mode csv'"
    for filename in os.listdir("offline_data/gdelt"):
        cmd_string += f" '.import offline_data/gdelt/{filename} events'"
    print(cmd_string)
    os.system(cmd_string)

def filterRelevantGDELTevents():
    con = md.setupConnection("offline_events.db")
    gdelt_data = md.fetchGDELTData("fee")
    stats = {"total":0, "included":0, "excluded":0}
    with open("log/geomapping/event_filtering.log","a") as f:
        for event in gdelt_data:
            stats["total"] += 1
            event_id = event[0]
            print(event_id)
            event_date = event[1]
            location = event[2]
            if location.__contains__(";"):
                location = location.split(";")[0]
            elif location == "South Africa" or location == "":
                #no relevant information
                stats["excluded"] += 1
                continue
            lat = event[3]
            lon = event[4]
            url = event[5]
            if url.__contains__("fees") or url.__contains__("fee-") or url.__contains__("fee_") or url.__contains__("fee.") or url.__contains__("fee/"):
                md.insertEventEntry(con, event_id, event_date, location, lat, lon, url)
                stats["included"] += 1
            else:
                f.write(f"{event_id} {url} not included\n")
                stats["excluded"] += 1
        f.write(f"Total: {stats['total']}, Included: {stats['included']}, Excluded: {stats['excluded']}\n")
    print(stats)
    
def explainDecisionOnFilteringBetter():
    con = md.setupConnection("offline_events.db")
    with open("log/geomapping/event_filtering.log","r") as f:
        with open("log/geomapping/event_filtering_explained.log","w") as w: 
            lines = f.readlines()
            for line in lines[:-1]:
                event_id = line.split(" ")[0]
                print(event_id)
                event = md.getGdeltEventById(con, event_id)
                explain_string = event_id
                location = event[2]
                if location.__contains__(";"):
                    location = location.split(";")[0]
                elif location == "South Africa" or location == "":
                    explain_string += f" location has no relevant information"
                    continue
                url = event[5].lower()
                lat = event[3]
                lon = event[4]
                if not (url.__contains__("fees") or url.__contains__("fee-") or url.__contains__("fee_") or url.__contains__("fee.") or url.__contains__("fee/")):
                    explain_string += f" url does not contain fee"
                else:
                    md.insertEventEntry(con, event_id, event[1], location, lat, lon, event[5])
                    print(f"inserted {event_id}")
                w.write(f"{explain_string}\n")

def deleteRedundantEvents():
    con = md.setupConnection("offline_events.db")
    events = md.fetchAllEvents()
    tmp_event_dict = {}
    with open("log/geomapping/event_deletion.log","a") as f:
        for event in events:
            id = event[0]
            key = f"{event[1]}_{event[2]}_{event[3]}_{event[4]}_{event[5]}"
            if key not in tmp_event_dict.keys():
                tmp_event_dict.update({key:id})
                #f.write(f"Not redundant {id}: {key}\n")
            else:
                md.deleteEventEntry(con, id)
                md.insertEntryIntoEvent2Ids(con,id,tmp_event_dict[key])
                #f.write(f"Redundant {id}: {key}\n")

def createEventsReduced():
    con = md.setupConnection("offline_events.db")
    events = md.fetchAllEvents()
    tmp_event_dict = {}
    for event in events:
        id = event[0]
        key = f"{event[1]}_{event[2]}_{event[3]}_{event[4]}"
        if key not in tmp_event_dict.keys():
            tmp_event_dict.update({key:id})
            md.insertEntryIntoEventsReduced(con, id, event[1], event[2], event[3], event[4], event[5])
            #f.write(f"Not redundant {id}: {key}\n")
        else:
            md.insertEntryIntoEvent2Ids(con,id,tmp_event_dict[key])
            #f.write(f"Redundant {id}: {key}\n")

if __name__ == '__main__':
    # start_date = "20151012"
    # end_date = "20151031"
    # for date in range(int(start_date), int(end_date)):
    #     getZipFile(date)
    #     convertToCsv(f"{date}.export.CSV")
    #importIntoDatabase()
    #filterRelevantGDELTevents()
    #explainDecisionOnFilteringBetter()
    #deleteRedundantEvents()
    createEventsReduced()