import sqlite3 as sql

def setupConnection(database_name="data.db"):
    con = sql.connect(database_name)
    return con

def createTables():
    con = sql.connect('data.db')
    with con:
        con.execute("""
            CREATE TABLE hashtags(
                hashtag TEXT PRIMARY KEY,
                location TEXT
                );"""
            )
        
        con.execute("""
            CREATE TABLE geos(
                geo_id TEXT PRIMARY KEY,
                location_name TEXT,
                country TEXT
                );"""
            )
        
        con.execute("""
            CREATE TABLE users(
                user_id INTEGER PRIMARY KEY,
                username TEXT,
                description TEXT,
                created_at DATETIME,
                name TEXT,
                location_raw TEXT,
                location_parsed TEXT,
                location_abroad INTEGER,
                verified INTEGER,
                url TEXT,
                followers_count INTEGER,
                following_count INTEGER,
                tweets_count INTEGER,
                listed_count INTEGER
            );

        """)
        con.execute("""
            CREATE TABLE tweets(
                tweet_id INTEGER PRIMARY KEY,
                text TEXT,
                created_at DATETIME,
                language TEXT,
                type TEXT,
                referenced_id INTEGER,
                url TEXT,
                retweet_count INTEGER,
                reply_count INTEGER,
                like_count INTEGER,
                quote_count INTEGER,
                impression_count INTEGER,
                user_id INTEGER,
                geo_id TEXT,
                FOREIGN KEY(referenced_id) REFERENCES tweets(tweet_id),
                FOREIGN KEY(user_id) REFERENCES users(user_id),
                FOREIGN KEY(geo_id) REFERENCES geos(geo_id)
            );
        """)

        con.execute("""
            CREATE TABLE hashtagToTweet(
                hashtag TEXT,
                tweet_id INTEGER,
                FOREIGN KEY(hashtag) REFERENCES hashtags(hashtag),
                FOREIGN KEY(tweet_id) REFERENCES tweets(tweet_id)
            );"""
            )
            
def createEventTables():
    con = sql.connect('offline_events.db')
    with con:
        con.execute("""
            CREATE TABLE events(
                event_id INT PRIMARY KEY,
                day INT,
                monthYear INT,
                year INT,
                FractionDate TEXT,
                Actor1Code TEXT,
                Actor1Name TEXT,
                Actor1CountryCode TEXT,
                Actor1KnownGroupCode TEXT,
                Actor1EthnicCode TEXT,
                Actor1Religion1Code TEXT,
                Actor1Religion2Code TEXT,
                Actor1Type1Code TEXT,
                Actor1Type2Code TEXT,
                Actor1Type3Code TEXT,
                Actor2Code TEXT,
                Actor2Name TEXT,
                Actor2CountryCode TEXT,
                Actor2KnownGroupCode TEXT,
                Actor2EthnicCode TEXT,
                Actor2Religion1Code TEXT,
                Actor2Religion2Code TEXT,
                Actor2Type1Code TEXT,
                Actor2Type2Code TEXT,
                Actor2Type3Code TEXT,
                IsRootEvent INT,
                EventCode TEXT,
                EventBaseCode TEXT,
                EventRootCode TEXT,
                QuadClass INT,
                GoldsteinScale REAL,
                NumMentions INT,
                NumSources INT,
                NumArticles INT,
                AvgTone REAL,
                Actor1Geo_Type INT,
                Actor1Geo_Fullname TEXT,
                Actor1Geo_CountryCode TEXT,
                Actor1Geo_ADM1Code TEXT,
                Actor1Geo_Lat REAL,
                Actor1Geo_Long REAL,
                Actor1Geo_FeatureID TEXT,
                Actor2Geo_Type INT,
                Actor2Geo_Fullname TEXT,
                Actor2Geo_CountryCode TEXT,
                Actor2Geo_ADM1Code TEXT,
                Actor2Geo_Lat REAL,
                Actor2Geo_Long REAL,
                Actor2Geo_FeatureID TEXT,
                ActionGeo_Type INT,
                ActionGeo_Fullname TEXT,
                ActionGeo_CountryCode TEXT,
                ActionGeo_ADM1Code TEXT,
                ActionGeo_Lat REAL,
                ActionGeo_Long REAL,
                ActionGeo_FeatureID TEXT,
                DateAdded TEXT,
                SourceURL TEXT
                );
        """)
def addTweet(tweet_id,text,created_at,language,type,referenced_id,retweet_count,
             reply_count,like_count,quote_count,impression_count,user_id,geo_id=None):
    
    tweets_added = 0
    tweets_already_existed = 0

    
    url = f"https://twitter.com/abc/status/{tweet_id}"

    con = sql.connect('data.db')
    try:
        with con:
            con.execute("""
                INSERT INTO tweets VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)
                """,(tweet_id,text,created_at,language,type,referenced_id,url,retweet_count,
                    reply_count,like_count,quote_count,impression_count,user_id,geo_id)
            )
            tweets_added += 1
    except sql.IntegrityError:
        tweets_already_existed += 1
    
    return tweets_added, tweets_already_existed

def addUser(user_id,username,description,created_at,name,location_raw,
            verified,followers_count,following_count,tweets_count,listed_count):
    
    users_added = 0
    users_already_existed = 0

    url = f"https://twitter.com/{username}"
    con = sql.connect('data.db')
    try:
        with con:
            con.execute("""
                INSERT INTO users(user_id,username,description,created_at,name,location_raw,
                verified,url,followers_count,following_count,tweets_count,listed_count) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)
                """,(user_id,username,description,created_at,name,location_raw,verified,url,
                    followers_count,following_count,tweets_count,listed_count)
            )
        users_added += 1
    except sql.IntegrityError:
        users_already_existed += 1
    return users_added, users_already_existed

def addHashtag(con, hashtag,truncated=0):
    hashtag_added = 0
    hashtag_already_existed = 0
    try:
        with con:
            con.execute("""INSERT INTO hashtags(hashtag,truncated) VALUES (?,?)""",(hashtag,truncated)
            )
            hashtag_added += 1
    except sql.IntegrityError:
        hashtag_already_existed += 1
    return hashtag_added, hashtag_already_existed

def addGeo(geo_id,location_name,country):
    con = sql.connect('data.db')
    geo_added = 0
    geo_already_existed = 0
    try: 
        with con:
            con.execute("""
                INSERT INTO geos(geo_id,location_name,country) VALUES (?,?,?)
                """,(geo_id,location_name,country)
            )
        geo_added += 1
    except sql.IntegrityError:
        geo_already_existed += 1

    return geo_added, geo_already_existed

def addHashtagToTweetConnection(con, hashtag,tweet_id):
    #con = sql.connect('data.db')
    hashtagToTweetConnectionAdded = 0
    hashtagToTweetConnectionAlreadyExisted = 0
    try:
        with con:
            con.execute("""
                INSERT INTO hashtagToTweet VALUES(?,?)
                """,(hashtag,tweet_id)
            )
        hashtagToTweetConnectionAdded += 1
    except sql.IntegrityError:
        hashtagToTweetConnectionAlreadyExisted += 1
    return hashtagToTweetConnectionAdded, hashtagToTweetConnectionAlreadyExisted

def getTweetById(tweet_id):
    con = sql.connect('data.db')
    with con:
        cur = con.cursor()
        cur.execute("""
            SELECT * FROM tweets WHERE tweet_id = ?
            """,(tweet_id,)
        )
        return cur.fetchone()

def fetchAllTweets():
    con = sql.connect('data.db')
    with con:
        cur = con.cursor()
        cur.execute("""
            SELECT * FROM tweets
            """)
        return cur.fetchall()
    
def fetchAllTweetsWithDate():
    con = sql.connect('data.db')
    with con:
        cur = con.cursor()
        cur.execute("""
            SELECT tweet_id, created_at FROM tweets
            """)
        return cur.fetchall()
    
def fetchAllUsers(orderByLat = False):
    con = sql.connect('data.db')
    with con:
        cur = con.cursor()
        if orderByLat:
            cur.execute("""
                SELECT * FROM users ORDER BY location_lat
                """)
        else:
            cur.execute("""
                SELECT * FROM users
                """)
        return cur.fetchall()
    
def fetachAllUserLocations():
    con = sql.connect('data.db')
    with con:
        cur = con.cursor()
        cur.execute("""
            SELECT lat, lon FROM users,locations WHERE users.location_id = locations.location_id;
            """)
        return cur.fetchall()

def fetchAllHashtags():
    con = sql.connect('data.db')
    with con:
        cur = con.cursor()
        cur.execute("""
            SELECT * FROM hashtags
            """)
        return cur.fetchall()

def fetchAllHashtagsToTweets():
    con = sql.connect('data.db')
    with con:
        cur = con.cursor()
        cur.execute("""
            SELECT * FROM hashtagToTweet
            """)
        return cur.fetchall()
    
def insertCompletedHashtag(con, hashtag, completedHashtag, stats):
    try: 
        with con:
            cur = con.cursor()
            updates = cur.execute("""
                UPDATE hashtags SET hashtag = ?, completed = 1 WHERE hashtag = ? AND truncated = 1
                """,(completedHashtag,hashtag)
            )
            stats["hashtag_updated_rows"] += 1
            updated_rows = cur.execute("""
                    UPDATE hashtagToTweet SET hashtag = ?, completed = 1 WHERE hashtag = ?
                            """,(completedHashtag,hashtag)
                )
            stats["h2t_updated_rows"] += updated_rows.rowcount
    except sql.IntegrityError:
        stats["deletion_necessary"] += 1
        try:
            with con:
                cur = con.cursor()
                deleted_rows = cur.execute("""
                    DELETE FROM hashtags WHERE hashtag = ? AND truncated = 1
                    """,(hashtag,)
                )
                stats["deleted_rows"] += deleted_rows.rowcount
                
        except sql.IntegrityError:
            stats["hashtag_already_completed"] += 1

def insertCityIntoHashtags(con, hashtag, city):
    try: 
        with con:
            cur = con.cursor()
            updates = cur.execute("""
                UPDATE hashtags SET city = ? WHERE hashtag = ?
                """,(city,hashtag)
            )
            return updates.rowcount
    except sql.IntegrityError:
        return 0
    
def insertUniversityIntoHashtags(con, hashtag, university):
    try: 
        with con:
            cur = con.cursor()
            updates = cur.execute("""
                UPDATE hashtags SET university = ? WHERE hashtag = ?
                """,(university,hashtag)
            )
            return updates.rowcount
    except sql.IntegrityError:
        return 0
    
def updateUserLocation(con,user_id,lat,lon,location_parsed,location_abroad,geocoder="nominatim"):
    location_checked = 1
    if geocoder == "photon":
        location_checked = 2
    try: 
        with con:
            cur = con.cursor()
            updates = cur.execute("""
                UPDATE users SET location_lat = ?, location_lon = ?, location_parsed = ?, location_abroad = ?, location_checked = ? WHERE user_id = ?
                """,(lat,lon,location_parsed,location_abroad,location_checked,user_id)
            )
            return updates.rowcount
    except sql.IntegrityError:
        return 0
    
def updateUserLocationId(con,user_id,locationId):
    try: 
        with con:
            cur = con.cursor()
            updates = cur.execute("""
                UPDATE users SET location_id = ?, moved = 2 WHERE user_id = ?
                """,(locationId,user_id)
            )
            return updates.rowcount
    except sql.IntegrityError:
        return 0
    
def setLocationCheckedForUser(con,user_id, geocoder = "nominatim"):
    location_checked = 1
    if geocoder == "photon":
        location_checked = 2
    try: 
        with con:
            cur = con.cursor()
            updates = cur.execute("""
                UPDATE users SET location_checked = ? WHERE user_id = ?
                """,(location_checked,user_id)
            )
            return updates.rowcount
    except sql.IntegrityError:
        return 0

def updateHashtagLocation(con,hashtag_id,lat,lon):
    try: 
        with con:
            cur = con.cursor()
            updates = cur.execute("""
                UPDATE hashtags SET location_lat = ?, location_lon = ? WHERE hashtag = ?
                """,(lat,lon,hashtag_id)
            )
            return updates.rowcount
    except sql.IntegrityError:
        return 0
    
def insertLocation(con,name,lat,lon,location_abroad):
    try:
        with con:
            cur = con.cursor()
            cur.execute("""
                INSERT INTO locations VALUES (?,?,?,?)
                """,(name,lat,lon,location_abroad)
            )
            return 1
    except sql.IntegrityError:  
        return 0
    
def insertTweetCoordinatesIntoLocations(con,geo_id,lat,lon,location_abroad):
    try: 
        with con:
            cur = con.cursor()
            updates = cur.execute("""
                INSERT INTO locations VALUES (?,?,?,?)
                """,(geo_id,lat,lon,location_abroad)
            )
            return updates.rowcount
    except sql.IntegrityError:
        return 0
    
def checkIfLoactionIsAbroad(con,geo_id):
    try:
        with con:
            cur = con.cursor()
            cur.execute("""
                SELECT country FROM geos WHERE geo_id = ?
                """,(geo_id,)
            )
            country =  cur.fetchone()[0]
            if country == "South Africa":
                return 0
            else:
                return 1
    except sql.IntegrityError:
        print("Error in checkIfLoactionIsAbroad, geo_id not found: ", geo_id)
        return -1
    
def fetchAllLocations(select="all"):
    con = sql.connect('data.db')
    sql_statement = ""
    if select == "hashtag":
        sql_statement = "SELECT * FROM locations WHERE location_id IN (SELECT location FROM hashtags)"
    elif select == "user":
        sql_statement = "SELECT * FROM locations WHERE location_id IN (SELECT location_id FROM users)" #geht nicht
    else:
        sql_statement = "SELECT lat,lon FROM locations WHERE location_abroad = 0"

    with con:
        cur = con.cursor()
        cur.execute(sql_statement)
        return cur.fetchall()

def fetchAllGeoLocations():
    con = sql.connect('data.db')
    with con:
        cur = con.cursor()
        cur.execute("""
            SELECT * FROM geos
            """)
        return cur.fetchall()

def fetchAllGeoLocationsWithLatLon():
    con = sql.connect('data.db')
    with con:
        cur = con.cursor()
        cur.execute("""
            SELECT location_id, lat, lon FROM locations WHERE location_type = 2 AND location_abroad = 0
            """)
        return cur.fetchall()

def getNumberOfTweetsOfGeoLocation(con,geo_id):
    with con:
        cur = con.cursor()
        cur.execute("""
            SELECT COUNT(*) FROM tweets WHERE geo_id = ?
            """,(geo_id,)
        )
        return cur.fetchone()[0]

def setLocationIdInUsersTable(con, user_id, location_id):
    try: 
        with con:
            cur = con.cursor()
            updates = cur.execute("""
                UPDATE users SET location_parsed = ? WHERE user_id = ?
                """,(location_id,user_id)
            )
            return updates.rowcount
    except sql.IntegrityError:
        return 0
    
def getGeoLocationById(con,geo_id):
    with con:
        cur = con.cursor()
        cur.execute("""
            SELECT location_name FROM geos WHERE geo_id = ?
            """,(geo_id,)
        )
        return cur.fetchone()[0]

def getUserLocationById(con,user_id,abroad=False):
    sql_statement = "SELECT location_id FROM users WHERE user_id = ?"
    if abroad:
        sql_statement = "SELECT l.location_id, l.location_abroad FROM users AS u, locations AS l WHERE u.user_id = ? AND u.location_id = CAST(l.location_id AS TEXT);"

    with con:
        cur = con.cursor()
        cur.execute(sql_statement,(user_id,)
        )
    if abroad:
        return cur.fetchone()
    else:
        return cur.fetchone()[0]

def getUserById(con,user_id):
    with con:
        cur = con.cursor()
        cur.execute("""
            SELECT * FROM users WHERE user_id = ?
            """,(user_id,)
        )
        return cur.fetchone()
    
def getCityToUniKey(uni_key):
    with open("preprocessing/uni_keys.csv","r") as f:
        lines = f.readlines()
        for line in lines:
            abbreviation = line.split(",")[1].strip()
            if abbreviation == uni_key:
                return line.split(",")[2].strip()
            
def getHashtagLocationsById(con,tweet_id):
    hashtag_locations = []
    with con:
        cur = con.cursor()
        cur.execute("""
            SELECT location FROM hashtags WHERE hashtag IN (SELECT hashtag FROM hashtagToTweet WHERE tweet_id = ?)
            """,(tweet_id,)
        )
        locations =  cur.fetchall()
        for location in locations:
            location_key = location[0]
            hashtag_locations.append(getCityToUniKey(location_key))
    return hashtag_locations
            
def constructTweetLocalizationOptions(con,tweet_id,user_id,geo_id):   
    user_location = getUserLocationById(con,user_id)
    geo_location = ""
    if geo_id != None:
        geo_location = getGeoLocationById(con,geo_id)
    
    hashtag_locations = getHashtagLocationsById(con,tweet_id)
    hashtag_location_1 = ""
    hashtag_location_2 = ""
    hashtag_location_3 = ""
    hashtag_location_4 = ""
    hashtag_location_5 = ""

    try:
        hashtag_location_1 = hashtag_locations[0]
        hashtag_location_2 = hashtag_locations[1]
        hashtag_location_3 = hashtag_locations[2]
        hashtag_location_4 = hashtag_locations[3]
        hashtag_location_5 = hashtag_locations[4]
    except IndexError:
        pass
    
    try: 
        with con: 
            cur = con.cursor()
            cur.execute("""
                INSERT INTO tweetLocalization VALUES (?,?,?,?,?,?,?,?,?)
                """,(tweet_id,geo_location,user_location,hashtag_location_1,hashtag_location_2,hashtag_location_3,hashtag_location_4,hashtag_location_5,len(hashtag_locations))
            )
            return 1
    except sql.IntegrityError:
        return 0
    
def fetchACLEDData():
    con = sql.connect('offline_events.db')
    with con:
        cur = con.cursor()
        cur.execute("""
            SELECT * FROM acled_events
            """)
        return cur.fetchall()
    
def fetchGDELTData(filterTerm=None):
    con = sql.connect('offline_events.db')
    sql_string = "SELECT event_id,day,ActionGeo_Fullname,ActionGeo_Lat,ActionGeo_Long,SourceURL FROM gdelt_events"
    if filterTerm != None:
        sql_string += f" WHERE SourceURL LIKE '%{filterTerm}%'"
    with con:
        cur = con.cursor()
        cur.execute(sql_string)
        return cur.fetchall()

def insertEventEntry(con,event_id,date,location,lat,lon,details):
    try: 
        with con:
            cur = con.cursor()
            cur.execute("""
                INSERT INTO events VALUES (?,?,?,?,?,?)
                """,(event_id,date,location,lat,lon,details)
            )
            return 1
    except sql.IntegrityError:
        return 0
    
def fetchAllEventLocations(withDate=False,reduced=False, onlyChecked=False):
    con = setupConnection('offline_events.db')
    sql_string = ""
    if withDate:
        if reduced:
            sql_string = "SELECT lat, lon, date, event_id, details FROM eventsReduced ORDER BY date DESC;"
        else:
            sql_string = f"SELECT lat, lon, date, event_id FROM events {'WHERE checked=1' if onlyChecked else ''} ORDER BY date DESC  ;"
    else:
        if reduced:
            sql_string = "SELECT lat, lon FROM eventsReduced ORDER BY date DESC;"
        else:
            sql_string = f"SELECT lat, lon FROM events {'WHERE checked=1' if onlyChecked else ''} ORDER BY date DESC ;"
    try:
        with con:
            cur = con.cursor()
            cur.execute(sql_string)
            return cur.fetchall()
    except sql.IntegrityError:
        return 0
    
def fetchAllEvents(eventsReduced=True, onlyChecked=False):
    con = setupConnection('offline_events.db')
    try:
        with con:
            cur = con.cursor()
            cur.execute(f"""
                SELECT * FROM {'eventsReduced' if eventsReduced else 'events'} {'WHERE checked=1' if onlyChecked else ''};
                """)
            return cur.fetchall()
    except sql.IntegrityError:
        return 0
    
def checkIfLocationAlreadyExists(con,lat,lon,abroad):
    try:
        with con:
            cur = con.cursor()
            cur.execute("""
                SELECT * FROM locations WHERE lat = ? AND lon = ? AND location_abroad = ?
                """,(lat,lon,abroad)
            )
            entry = cur.fetchone()
            if entry != None:
                return True, entry[0]
            else:
                return False, None
    except sql.IntegrityError:
        return 0

def insertIntoHashtag2Tweet(con,hashtag,tweet_id,completed):
    try:
        with con:
            cur = con.cursor()
            cur.execute("""
                INSERT INTO hashtag2Tweet VALUES (?,?,?)
                """,(hashtag,tweet_id,completed)
            )
            return 1
    except sql.IntegrityError:
        return 0
    
def getGdeltEventById(con,event_id):
    try:
        with con:
            cur = con.cursor()
            cur.execute("""
                SELECT event_id,day,ActionGeo_Fullname,ActionGeo_Lat,ActionGeo_Long,SourceURL FROM gdelt_events WHERE event_id = ?
                """,(event_id,)
            )
            return cur.fetchone()
    except sql.IntegrityError:
        return 0

def deleteEventEntry(con,event_id):
    try:
        with con:
            cur = con.cursor()
            cur.execute("""
                DELETE FROM events WHERE event_id = ?
                """,(event_id,)
            )
            return 1
    except sql.IntegrityError:
        return 0

def insertEntryIntoEvent2Ids(con,id,ref_id):
    try:
        with con:
            cur = con.cursor()
            cur.execute("""
                INSERT INTO event2Ids VALUES (?,?)
                """,(ref_id,id)
            )
            return 1
    except sql.IntegrityError:
        return 0

def insertEntryIntoEventsReduced(con,event_id,date,location,lat,lon,details):
    try:
        with con:
            cur = con.cursor()
            cur.execute("""
                INSERT INTO eventsReduced VALUES (?,?,?,?,?,?)
                """,(event_id,date,location,lat,lon,details)
            )
            return 1
    except sql.IntegrityError:
        return 0
    
def getUniqueUsersWithFirstTweetTime():
    con = setupConnection('data.db')
    try:
        with con:
            cur = con.cursor()
            cur.execute("""
                SELECT user_id, MIN(created_at) FROM tweets GROUP BY user_id;
                """)
            return cur.fetchall()
    except sql.IntegrityError:
        return 0

def getUsersWithTweetAmount():
    con = setupConnection('data.db')
    try:
        with con:
            cur = con.cursor()
            cur.execute("""
                SELECT user_id, COUNT(*) FROM tweets GROUP BY user_id;
                """)
            return cur.fetchall()
    except sql.IntegrityError:
        return 0

def fetchAllTweetImpactLocations(moreDetails=False, inSouthAfrica=False, onlyUniversityLocations=False):
    con = setupConnection('data.db')
    sql_statement = ""
    if moreDetails:
        sql_statement = f"""
                SELECT DISTINCT t.tweet_id, t.text, l.lat, l.lon, t.created_at, t.type, t.url, t.retweet_count, t.reply_count, t.like_count, t.quote_count, t.impression_count, t.user_id, t.geo_id, l.location_id FROM tweets as t, hashtag2Tweet as h2, hashtags as h, locations as l 
                        WHERE t.tweet_id = h2.tweet_id AND h2.hashtag = h.hashtag AND h.location = l.location_id {'AND l.location_abroad = 0' if inSouthAfrica else ''};
                """
    else:
        sql_statement = f"""
                SELECT DISTINCT l.lat, l.lon, t.created_at, t.tweet_id, t.user_id, t.type AS date FROM tweets as t, hashtag2Tweet as h2, hashtags as h, locations as l 
                        WHERE t.tweet_id = h2.tweet_id AND h2.hashtag = h.hashtag AND h.location = l.location_id {'AND l.location_abroad = 0' if inSouthAfrica else ''} {'AND l.location_type = 1' if onlyUniversityLocations else ''};
                """
    try:
        with con:
            cur = con.cursor()
            cur.execute(sql_statement)
            return cur.fetchall()
    except sql.IntegrityError:
        return 0
    
def fetchAllTweetUserLocations(inSouthAfrica=False, removeWrongSouthAfricaLocation=True, onlyUniversityLocations=False, withOutMovedUsers=False):
    con = setupConnection('data.db')
    try: 
        with con:
            cur = con.cursor()
            cur.execute(f"""
                SELECT l.lat, l.lon, t.created_at AS date, t.tweet_id, t.user_id, t.type FROM tweets as t, users as u, locations as l 
                        WHERE t.user_id = u.user_id {'AND l.location_type=1' if onlyUniversityLocations else ''} AND u.location_id =  CAST(l.location_id AS TEXT) {'AND u.location_id != "1811"' if removeWrongSouthAfricaLocation else ''} {'AND l.location_abroad = 0' if inSouthAfrica else ''} {'AND u.moved = 0' if withOutMovedUsers else ''};
                """)
            return cur.fetchall()
    except sql.IntegrityError:
        return 0

def fetchAllTweetGeoLocations(inSouthAfrica=False,onlyUniversityLocations=False):
    con = setupConnection('data.db')
    try: 
        with con:
            cur = con.cursor()
            cur.execute(f"""
                SELECT l.lat, l.lon, t.created_at AS date, t.tweet_id, t.user_id, t.type FROM tweets as t, locations as l 
                        WHERE t.geo_id = l.location_id {'AND l.location_abroad = 0' if inSouthAfrica else ''} {'AND l.location_type=3' if onlyUniversityLocations else ''};
                """)
            return cur.fetchall()
    except sql.IntegrityError:
        return 0
    
def getAllTweets():
    con = setupConnection('data.db')
    tweets = {}
    try: 
        with con:
            cur = con.cursor()
            print("Fetching geo tweets")
            cur.execute(f"""
                SELECT t.tweet_id,t.created_at,l.location_abroad FROM tweets as t, locations as l WHERE t.geo_id = CAST(l.location_id AS TEXT);
                """)
            geo_tweets = cur.fetchall()
            tweets.update({"geo_tweets":geo_tweets})
            print("Fetching user tweets")
            cur.execute(f"""
                SELECT t.tweet_id,t.created_at,l.location_abroad FROM tweets as t, users as u, locations as l WHERE t.user_id = u.user_id AND u.location_id = CAST(l.location_id AS TEXT);
                """)
            user_tweets = cur.fetchall()
            tweets.update({"user_tweets":user_tweets})
            print("Fetching hashtag tweets")
            cur.execute(f"""
                SELECT t.tweet_id,t.created_at,l.location_abroad FROM tweets as t, locations as l, hashtags as h, hashtag2Tweet as ht WHERE t.tweet_id = ht.tweet_id AND ht.hashtag = h.hashtag AND h.location = CAST(l.location_id AS TEXT);
                """)
            hashtag_tweets = cur.fetchall()
            tweets.update({"impact_tweets":hashtag_tweets})
            return tweets
    except sql.IntegrityError:
        return 0