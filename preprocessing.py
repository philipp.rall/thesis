import manage_database as md
import re
import sqlite3 as sql
import matplotlib.pyplot as plt
import json
import webbrowser
import requests
import datetime
import calendar
import random
import math
import os
import shapely as sh
import pyproj
from shapely.ops import transform
from functools import partial

def recreateHashtags():
    con = sql.connect('data.db')
    all_tweets = md.fetchAllTweets()
    counter_ending = 0
    truncated_counter = 0
    unique_hashtags = []
    with open("truncated_tweets2.csv", "w") as f:
        for counter, tweet in enumerate(all_tweets):
            # if counter % 10000 == 0:
            #     print(f"Processed {counter} tweets")
            tweet_id = tweet[0]
            text = tweet[1]
            url = tweet[6]

            # if text[-1] == '…':
            #     counter_ending += 1
            # truncated_hashtags = re.findall(r"#(\w+)…", text.lower())
            # if truncated_hashtags != []:
            #     hashtags = re.findall(r"#(\w+)", text.lower())
            #     if truncated_hashtags[0] not in hashtags:
            #         print(f"Hashtag {truncated_hashtags[0]} not in hashtags {hashtags}")
            #         truncated_counter += 1
            #     if truncated_hashtags[0] not in unique_hashtags:
            #         unique_hashtags.append(truncated_hashtags[0])
                
            hashtags = re.findall(r"#(\w+)", text.lower())
            for hashtag in hashtags:
                truncated = 0
                if hashtag in re.findall(r"#(\w+)…", text.lower()):
                    if hashtag not in unique_hashtags:
                        unique_hashtags.append(hashtag)
                    truncated_counter += 1
                    truncated = 1
                # md.addHashtag(con, hashtag,truncated)
                # md.addHashtagToTweetConnection(con,hashtag,tweet_id)
            #check if text ends with …
            # if re.findall(r"#(\w+)…", text) != [] and text[-1] == '…':
            #     f.write(f'{tweet_id},{url},{text}\n')
        sorted_hashtags = sorted(unique_hashtags)
        for hashtag in sorted_hashtags:
            f.write(f"{hashtag},\n")
    print(f"Found {counter_ending} tweets ending with …")
    print(f"Found {truncated_counter} truncated hashtags")
    print(f"Found {len(unique_hashtags)} unique truncated hashtags")

def insertCompletedHashtags():
    counter = 0
    stats = {"deletion_necessary":0,"deleted_rows":0,"h2t_updated_rows":0,"hashtag_updated_rows":0, "hashtag_already_completed":0}
    con = sql.connect('data.db')
    with open("truncated_completed.csv", "r") as f:
        lines = f.readlines()
        for line in lines:
            hashtag = line.split(",")[0]
            completed_hashtag = line.split(",")[1].strip()
            if completed_hashtag != "":
                counter += 1
                #print(f"Insert {hashtag} with {completed_hashtag}")
                md.insertCompletedHashtag(con,hashtag,completed_hashtag, stats)
    print(f"Inserted {counter} completed hashtags\n")

    print(f"Hashtag updated rows: {stats['hashtag_updated_rows']}")
    print(f"Deletion necessary: {stats['deletion_necessary']}")
    print(f"Deleted rows: {stats['deleted_rows']}")
    print(f"Hashtag2Tweet updated rows: {stats['h2t_updated_rows']}")
    print(f"Hashtag already completed: {stats['hashtag_already_completed']}")
    
def extractTruncatedHashtags():
    con = sql.connect('data.db')
    with con:
        cur = con.cursor()
        cur.execute("""
            SELECT hashtag FROM hashtags WHERE hashtag LIKE '%…';
            """)
        return cur.fetchall()

def createTruncatedHashtagFile():
    hashtags = extractTruncatedHashtags()
    con = sql.connect('data.db')
    with con:
        cur = con.cursor()
        cur.execute("""
            CREATE TABLE truncated_hashtags(
                hashtag TEXT PRIMARY KEY,
                completed_hashtag TEXT
            );
            """)
    for hashtag in hashtags:
        with con:
            cur = con.cursor()
            cur.execute("""
                INSERT INTO truncated_hashtags(hashtag) VALUES (?);
                """,(hashtag[0],)
            )

# def completeHashtags():
#     with open("preprocessing/truncated_hashtags.csv", "r") as f:
#         with open("preprocessing/autocompleted_hashtags.csv", "w") as e:
#             lines = f.readlines()
#             for line in lines:
#                 hashtag = line.split(",")[0]
#                 autocomplete = autocompleteHashtag(hashtag)
#                 e.write(f"{hashtag},{autocomplete}\n")


def correctInsertion():
    counter = 0
    with open("truncated_completed.csv", "r") as f:
        lines = f.readlines()
        for line in lines:
            hashtag = line.split(",")[0]
            completed_hashtag = line.split(",")[1].strip() 
            if hashtag == completed_hashtag:
                counter+=1
                print(hashtag)
    print(f"Found {counter} hashtags that are equal")

def dumpHashtags():
    hashtags = md.fetchAllHashtags()
    with open("preprocessing/hashtag_location_mapping.csv", "w") as f:
        for hashtag in hashtags:
            f.write(f"{hashtag[0]},\n")

def createLocationCompletionRules():
    completion_dict = {}
    with open("preprocessing/uni_keys.csv","r") as f2:
        lines = f2.readlines()
        for line in lines:
            abbreviation = line.split(",")[1].lower().strip()
            fees_must_fall = abbreviation + "feesmustfall"
            must_fall = abbreviation + "mustfall"
            shutdown = abbreviation + "shutdown"
            completion_dict.update({must_fall:abbreviation})
            completion_dict.update({fees_must_fall:abbreviation})
            completion_dict.update({shutdown:abbreviation})
    with open("preprocessing/location_completion_rulesNEW.csv","w") as f:
        for key in completion_dict.keys():
            f.write(f"{key},{completion_dict[key]}\n")

def autocompleteLocations():
    abbreviations = []
    locations_identified = 0
    completion_dict = {}
    with open("preprocessing/location_completion_rules.csv","r") as f:
        lines = f.readlines()
        for line in lines:
            hashtag = line.split(",")[0].lower().strip()
            completion = line.split(",")[1].lower().strip()
            completion_dict.update({hashtag:completion})
    with open("preprocessing/hashtag_location_mapping.csv","r") as f:    
        with open("preprocessing/uni_keys.csv","r") as f2:
            lines = f2.readlines()
            for line in lines:
                abbreviation = line.split(",")[1].lower().strip()
                abbreviations.append(abbreviation)

        hashtag_lines = f.readlines()
        with open("preprocessing/hashtag_location_mapped2.csv","w") as f3:
            for line in hashtag_lines:
                location = ""
                hashtag = line.split(",")[0].lower().strip()
                if hashtag in completion_dict.keys():
                    location = completion_dict[hashtag]
                else:
                    for abbreviation in abbreviations:
                        if len(abbreviation) > 2:
                            if hashtag.__contains__(abbreviation):
                                location = abbreviation
                                break
                if location != "":
                    locations_identified += 1
                f3.write(f"{hashtag},{location}\n")
    print(f"Identified {locations_identified} locations")

def mapUniversityToCity(university):
    with open("preprocessing/uni_keys.csv","r") as f:
        lines = f.readlines()
        for line in lines:
            abbreviation = line.split(",")[1]
            if abbreviation == university:
                return line.split(",")[2].strip().upper()

def insertUniversitiesAndLocationsInDatabase():
    stats = {"locations_found":0,"inserted_universities":0,"inserted_cities":0}
    with open("preprocessing/hashtag_location_mapped_MANUALLY.csv","r") as f:
        lines = f.readlines()
        for line in lines:
            location = line.split(',')[1].strip().upper()
            if location != "":
                stats["locations_found"] += 1
                hashtag = line.split(',')[0]
                con = sql.connect('data.db')
                #fixed set of cities
                if location not in ['GRAHAMSTOWN','BLOEMFONTEIN','CAPETOWN','DURBAN','JOHANNESBURG','PRETORIA']:
                    result = md.insertUniversityIntoHashtags(con, hashtag,location)
                    stats["inserted_universities"] += result
                    city = mapUniversityToCity(location)
                    result = md.insertCityIntoHashtags(con,hashtag,city)
                    stats["inserted_cities"] += result
                else:
                    result = md.insertCityIntoHashtags(con,hashtag,location)
                    stats["inserted_cities"] += result
    print(f"Found {stats['locations_found']} locations")
    print(f"Inserted {stats['inserted_universities']} universities")
    print(f"Inserted {stats['inserted_cities']} cities")

def moveRelevantAcledDataIntoEvents():
    relevant_entries = ['SAF6733','SAF6738','SAF6739','SAF6743','SAF6741','SAF6728','SAF6701','SAF6804','SAF6798','SAF6797','SAF6800','SAF6799','SAF6795','SAF6790','SAF6789','SAF6793','SAF6792','SAF6779','SAF6782','SAF6776','SAF6781','SAF6764','SAF6766','SAF6769','SAF6772','SAF6773','SAF6761','SAF6768','SAF6763','SAF6774','SAF6760','SAF6771','SAF6757','SAF6752','SAF6746','SAF6748','SAF6731','SAF6742','SAF6735','SAF6732','SAF6730','SAF6734','SAF6740','SAF6744','SAF6729','SAF6714','SAF6715','SAF6725','SAF6719','SAF6723','SAF6721','SAF6724','SAF6716','SAF6717','SAF6722','SAF6727','SAF6720','SAF6718','SAF6703','SAF6704','SAF6705','SAF6695','SAF6694','SAF6699','SAF6693','SAF6696','SAF6691','SAF6689','SAF6683','SAF6678','SAF6672','SAF6802','SAF6775','SAF6783','SAF6745','SAF6753','SAF6737','SAF6726','SAF6713','SAF6712','SAF6711']
    con = md.setupConnection("offline_events.db")
    acled_data = md.fetchACLEDData()
    for entry in acled_data:
        if entry[0] in relevant_entries:
            event_id = entry[0]
            date = f"201510{entry[1].split(' ')[0]}"
            location = entry[21]
            lat = entry[22]
            lon = entry[23]
            details = entry[27]
            md.insertEventEntry(con,event_id,date,location,lat,lon,details)

def checkSourceContent(date,location,text):
    date = str(date)
    day = date[-2:]
    month = date[-4:-2]
    year = date[:4]
    date_variants = [f"{day}-{month}-{year}",f"{day}/{month}/{year}",f"{month}-{day}-{year}",f"{month}/{day}/{year}",f"{year}-{month}-{day}",f"{year}/{month}/{day}",
                     f"{day}.{month}.{year}",f"{day} October",f"October {day}", f"{day} Oct", f"{day} Oct"]
    
    location_contained = False
    date_contained = False #tag der Berichterstattung
    correct_date = False #Tag des Ereignisses
    if text.__contains__(location):
        location_contained = True
    
    for date_variant in date_variants:
        if text.__contains__(date_variant):
            date_contained = True
    
    date = datetime.datetime.strptime(date, '%Y%m%d')
    day_name = calendar.day_name[date.weekday()]
    if text.__contains__(day_name) or text.__contains__("today") or text.__contains__("this evening") or text.__contains__("this afternoon") or text.__contains__("this morning"):
        correct_date = True

    return location_contained and date_contained, correct_date

def verifyEventSource(id,location,date,url):
    with open(f"preprocessing/eventReducing/sourceVerification.csv","r") as f3:
        lines = f3.readlines()
        for line in lines[1:]:
            if line.split(",")[0] == str(id):
                print(f"Event with ID {id} already checked")
                return
    print(f"Verifying event with ID {id}")
    with open(f"preprocessing/eventReducing/sourceVerification.csv","a") as f:
        accesable = False
        shouldbeChecked = False
        try:
            response = requests.get(url)
        except Exception as e:
            print(f"Could not connect to {url}")
            f.write(f"{id},{accesable},{shouldbeChecked},-,{date},{location},{url}\n")
            return
        #f.write(f"ID,accesable,shouldBeChecked,date,location,url\n")
        if response.status_code == 200:
            accesable = True
        text = response.text
        shouldbeChecked, correct_date = checkSourceContent(date,location,text)
        f.write(f"{id},{accesable},{shouldbeChecked},{correct_date},{date},{location},{url}\n")
        if shouldbeChecked:
            with open(f"preprocessing/eventReducing/toCheck.csv","a") as f2:
                f2.write(f"{id},{shouldbeChecked},{correct_date},{date},{location},{url}\n")

    print(f"ID: {id}, Accesable: {accesable}, Should be checked: {shouldbeChecked}, Correct date: {correct_date}")


def visualizeEventsVsEventsReduced():
    events = md.fetchAllEvents(False)
    events_reduced = md.fetchAllEvents(True)
    data = {}
    for event in events:
        id = event[0]
        location = event[2]
        date = event[1]
        details = event[5]
        coords = f"{event[3]},{event[4]}"
        if location not in data.keys():
            data.update({location:{date:[(id,coords,details)]}})
        else:
            if date not in data[location].keys():
                data[location].update({date:[(id,coords,details)]})
            else:
                data[location][date].append((id,coords,details))

    notReducedCounter = (0,0)
    with open("preprocessing/eventReducing/manualCheckout.csv","w") as f:
        for location in data.keys():
            line_added2 = False
            for date in data[location].keys():
                new_list = []
                line_added = False
                contains_ACLED = (False,'')
                for entry in data[location][date]:
                    if str(entry[0]).__contains__("SAF"):
                        contains_ACLED = (True, entry)
                if contains_ACLED[0]:
                    new_list = contains_ACLED[1]
                else:
                    new_list = data[location][date]
                    notReducedCounter = (notReducedCounter[0]+1,notReducedCounter[1]+len(new_list))
                    for entry in new_list:
                        f.write(f"{location},{date},{entry[0]},{entry[2]}\n")
                        line_added = True
                        line_added2 = True
                        verifyEventSource(entry[0],location,date,entry[2])
                data[location][date] = new_list
                if line_added:
                    f.write("\n")
            if line_added2:
                f.write("\n--------------------------\n")

    #visualize data
    print(f"Found {notReducedCounter[0]} locations with {notReducedCounter[1]} events")
    json.dump(data,open("preprocessing/eventReducing/eventsNewReduction.json","w"))

def openTenShouldBeCheckedURLs(startIdx):
    startIdx = startIdx-1
    with open(f"preprocessing/eventReducing/toCheckManualEdited.csv","r") as f:
        lines = f.readlines()
        for line in lines[startIdx:startIdx+10]:
            url = line.split(",")[-1].strip()
            print(f"Opened {line.split(',')[0]}")
            webbrowser.open(url)

def generateStatisticsOverEventReducing():
    with open("preprocessing/eventReducing/sourceVerification.csv","r") as f:
        lines = f.readlines()
        data = {"total":0,"accesible":0,"shouldBeChecked":0,"correctDate":0,"correctDateAndShouldBeChecked":0}
        for line in lines[1:]:
            data["total"] += 1
            if line.split(",")[1] == "True":
                data["accesible"] += 1
            if line.split(",")[2] == "True":
                data["shouldBeChecked"] += 1
            if line.split(",")[3] == "True":
                data["correctDate"] += 1
            if line.split(",")[2] == "True" and line.split(",")[3] == "True":
                data["correctDateAndShouldBeChecked"] += 1
        print(f"Total: {data['total']}")
        print(f"Accesible: {data['accesible']}, {round((data['accesible']/data['total'])*100,2)}%")
        print(f"Should be checked: {data['shouldBeChecked']}, {round((data['shouldBeChecked']/data['total'])*100,2)}%")
        print(f"Correct date: {data['correctDate']}, {round((data['correctDate']/data['total'])*100,2)}%")
        print(f"Correct date and should be checked: {data['correctDateAndShouldBeChecked']}, {round((data['correctDateAndShouldBeChecked']/data['total'])*100,2)}%")  

def constructUniqueEventsColumn():
    with open("preprocessing/eventReducing/toCheckManualEdited.csv","r") as f:
        lines = f.readlines()
        unique_ids = []
        for line in lines[1:]:
            if line[0] != " ":
                id = line.split(",")[0].strip()
                if id not in unique_ids:
                    unique_ids.append(id)

    print(unique_ids) #manual setting checked to 1

def getLocationOfCityCentre(cityName):
    with open("preprocessing/city_keys.csv","r") as f:
        lines = f.readlines()
        for line in lines:
            city = line.split(",")[0]
            if city == cityName:
                return line.split(",")[1],line.split(",")[2].strip("\n")
            
def getUsersInCityCentre(cityCentreId,cityName):
    usersInCityCentre = {}
    lat_city, lon_city = getLocationOfCityCentre(cityName.upper())
    with open(f"analysis/withinChecks/universities/{cityCentreId}/online_users","r") as f2:
        userLines = f2.readlines()
        random.shuffle(userLines)
        for user in userLines:
            lat = user.split("\t")[0]
            lon = user.split("\t")[1]
            user_id = user.split("\t")[4].strip()
            if lat == lat_city and lon == lon_city:
                if user_id not in usersInCityCentre.keys():
                    usersInCityCentre.update({user_id:1})
                else:
                    usersInCityCentre[user_id] += 1
    print(f"Found {len(usersInCityCentre)} users and {sum(usersInCityCentre.values())} tweets in {cityName}")
    return usersInCityCentre

def getAllUsersInSummaryTable():
    users = []
    with open(f"preprocessing/userMovingOutOfCityCentre/summary.csv","r") as f2:
        all_users = f2.readlines()
        for user in all_users:
            users.append(user.split(",")[0])
    return users

def getUniKeyOfCluster(clusterId):
    uni_name = ""
    with open("analysis/spatial/universities/nameMapping.csv","r") as f:
        lines = f.readlines()
        for line in lines:
            if line.split(",")[0] == str(clusterId):
                uni_name = line.split(",")[1].strip("\n")
    with open("preprocessing/uni_keys.csv","r") as f:
        lines = f.readlines()
        for line in lines:
            if line.split(",")[3] == uni_name:
                return line.split(",")[1]


def moveUsersOutOfCityCentres(cityCentreId,cityName):
    usersInCityCentre = getUsersInCityCentre(cityCentreId,cityName)
    numberOfAllTweets = sum(usersInCityCentre.values())
    newUserContainers = []
    #construct containers
    with open("analysis/spatial/metropolitan_areas/intersection_areas.csv","r") as f:
        lines = f.readlines()
        for line in lines[1:]:
            clusterId = line.split(",")[0]
            clusterIdCityCentre = line.split(",")[1]
            cityName = line.split(",")[2]
            if clusterIdCityCentre == str(cityCentreId):
                fraction = float(line.split(",")[3].strip("\n"))
                container = {"clusterId":clusterId,"newLocationKey":getUniKeyOfCluster(clusterId),"numberOfTweets":0,"numberOfUsers":0,"cityName":cityName,"fraction":fraction,"users":[]}
                newUserContainers.append(container)
    #sortedUsers = sorted(usersInCityCentre,key=usersInCityCentre.get, reverse=True)
    sortedUsers = list(usersInCityCentre.keys())
    random.shuffle(sortedUsers)
    #sort newUserContainers by fraction
    sortedUserContainer = sorted(newUserContainers, key=lambda k: k['fraction'],reverse=True)
    #fill containers
    numberOfMovedTweets = 0
    while len(sortedUsers) > 0:
        currentLength = len(sortedUsers)
        for container in sortedUserContainer:
            nextUser = sortedUsers.pop(0)
            numberOfTweets = usersInCityCentre[nextUser]
            if (container["numberOfTweets"]+numberOfTweets) <= (numberOfAllTweets * container["fraction"]):
                container["numberOfTweets"] += numberOfTweets
                numberOfMovedTweets += numberOfTweets
                container["numberOfUsers"] += 1
                container["users"].append(nextUser)
            else:
                sortedUsers.insert(0,nextUser)
        newLength = len(sortedUsers)
        if currentLength == newLength:
            print("Could not move all users")
            break
    for container in newUserContainers:
        print(f"Cluster {container['clusterId']} should have {math.floor(numberOfAllTweets * container['fraction'])} tweets and {math.floor(len(usersInCityCentre) * container['fraction'])} users")
        print(f"Cluster {container['clusterId']} has {container['numberOfTweets']} tweets and {container['numberOfUsers']} users ")
        os.makedirs(f"preprocessing/userMovingOutOfCityCentre/{container['cityName']}", exist_ok=True)
        with open(f"preprocessing/userMovingOutOfCityCentre/{container['cityName']}/{container['clusterId']}.csv","w") as f:
            for user in container["users"]:
                f.write(f"{user}\n")
        with open(f"preprocessing/userMovingOutOfCityCentre/summary.csv","a") as f:
            for user in container["users"]:
                if user not in getAllUsersInSummaryTable():
                    f.write(f"{user},{container['newLocationKey']}\n")
                else:
                    print(f"User {user} already in summary table")

def updateUserLocationsInDatabase():
    rows_updated = 0
    with open("preprocessing/userMovingOutOfCityCentre/summary.csv") as f:
        lines = f.readlines()
        con = sql.connect('data.db')
        for line in lines:
            user_id = line.split(",")[0]
            locationId = line.split(",")[1].strip("\n")
            rows_updated += md.updateUserLocationId(con,user_id,locationId)
    print(f"Updated {rows_updated} rows")

if __name__ == '__main__':
    #createTruncatedHashtagFile()
    #insertCompletedHashtags()
    #dumpHashtags()
    #autocompleteLocations()
    #autocompleteLocations()
    #insertUniversitiesAndLocationsInDatabase()
    #moveRelevantAcledDataIntoEvents()
    #visualizeEventsVsEventsReduced()
    #openTenShouldBeCheckedURLs(120)
    #generateStatisticsOverEventReducing()
    #constructUniqueEventsColumn()
    # moveUsersOutOfCityCentres(2,"capetown")
    # moveUsersOutOfCityCentres(21,"johannesburg")
    # moveUsersOutOfCityCentres(6,"durban")
    # moveUsersOutOfCityCentres(26,"pretoria")
    updateUserLocationsInDatabase()
    

