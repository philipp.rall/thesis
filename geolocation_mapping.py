import geopy 
import certifi
import ssl
from geopy.geocoders import Nominatim, Photon
import sqlite3 as sql
import manage_database as md
import time
from geopy.exc import GeocoderTimedOut
import numpy as np
import pandas as pd
from get_data import log
import matplotlib.pyplot as plt


def setUpClient(agent_name = "thesis", geocoder = "nominatim"):
    ctx = ssl.create_default_context(cafile=certifi.where())
    geopy.geocoders.options.default_ssl_context = ctx
    if geocoder == "nominatim":
        return Nominatim(user_agent=agent_name)
    elif geocoder == "photon":
        return Photon()

def tryOut(keyword,reverse=False):
    ctx = ssl.create_default_context(cafile=certifi.where())
    geopy.geocoders.options.default_ssl_context = ctx

    # instantiate a new Nominatim client
    app = Nominatim(user_agent="tutorial")

    # get location raw data
    if reverse:
        try:
            location = app.reverse(keyword,addressdetails=True,language='en')
        except ValueError:
            return None
    else:
        location = app.geocode(keyword,addressdetails=True,extratags=True)
    print(location.raw)

#print(location2.longitude, location2.latitude)

def tryPhoton(keyword):
    ctx = ssl.create_default_context(cafile=certifi.where())
    geopy.geocoders.options.default_ssl_context = ctx
    app = Photon()
    location = app.geocode(keyword,language='en')
    print(location.latitude, location.longitude)
    print(location.raw["properties"]["city"])

def parseRawLocationPhoton(client,location_raw):
    location = client.geocode(location_raw,language='en', timeout = 10)
    if location == None:
        try:
            location = client.reverse(location_raw, language='en', timeout = 10)
        except ValueError:
            return None,None,None,None
        if location == None:
            return None,None,None,None
   
    lat = location.latitude
    lon = location.longitude
    if 'city' in location.raw['properties']:
        city = location.raw['properties']['city']
    elif 'country' in location.raw['properties']:
        city = location.raw['properties']['country']
    else:
        city = None
    location_abroad = True
    if 'countrycode' in location.raw['properties']:
        if location.raw['properties']['countrycode'] == "ZA":
            location_abroad = False
    return lat,lon,city,location_abroad

def parseRawLocation(client,location_raw):
    try:
        location = client.geocode(location_raw,addressdetails=True,language='en', timeout = 3)
        time.sleep(1)
    except GeocoderTimedOut:
        print("GeocoderTimedOut")
        parseRawLocation(client,location_raw)
    if location == None:
        try:
            location = client.reverse(location_raw,addressdetails=True, language='en', timeout = 3)
            time.sleep(1)
        except ValueError:
            return None,None,None,None
        except GeocoderTimedOut:
            print("GeocoderTimedOut")
            parseRawLocation(client,location_raw)
        if location == None:
            return None,None,None,None
   
    lat = location.latitude
    lon = location.longitude
    if 'city' in location.raw['address']:
        city = location.raw['address']['city']
    elif 'country' in location.raw['address']:
        city = location.raw['address']['country']
    else:
        city = None
    location_abroad = True
    if 'country_code' in location.raw['address']:
        if location.raw['address']['country_code'] == "za":
            location_abroad = False
    return lat,lon,city,location_abroad

def extractLocations():
    all_users = md.fetchAllUsers()
    con = sql.connect('data.db')
    client = setUpClient("new_user", "photon")
    stats = {"location_found":0,"location_not_found":0}
    with open("log/geomapping/geomapping_log.csv", "a") as f:
        f.write(f"User ID,Location Raw,Location Parsed\n")
        for counter, user in enumerate(all_users):
            user_id = user[0]
            location_raw = user[5]
            location_checked = user[16]
            if location_raw != None and location_checked == 0:
                print(f"{user_id}")
                f.write(f"{user_id},{location_raw.replace(',',';')}")
                lat,lon,city,location_abroad = parseRawLocationPhoton(client, location_raw)
                f.write(f",{city}\n")
                if lat != None and lon != None and city != None and location_abroad != None:
                    md.updateUserLocation(con,user_id,lat,lon,city,location_abroad,geocoder = "photon")
                    stats["location_found"] += 1
                else:
                    stats["location_not_found"] += 1
                    md.setLocationCheckedForUser(con,user_id, geocoder = "photon")
            if counter % 100 == 0:
                print(f"-------------------------Processed {counter} users")
        f.write(f"Stats: {stats}\n")
    print(stats)

def addGeoLocationsToUniTable():
    client = setUpClient("hashtags")
    with open("preprocessing/uni_keys.csv","r+") as f:
        lines = f.readlines()
        for line in lines:
            if line != "\n":
                name = line.split(",")[3].strip()
                lat, lon, city, abroad = parseRawLocation(client,name)
                print(f"{name},{lat},{lon},{city},{abroad}")
                #f.write(f"{line},{lat},{lon}\n")

def getGeoLocationForUni(university):
    print(university)
    with open("preprocessing/uni_keys.csv","r") as f:
        lines = f.readlines()
        for line in lines:
            abbreviation = line.split(",")[1].strip()
            if abbreviation == university:
                lat = line.split(",")[4].strip()
                lon = line.split(",")[5].strip()
                return lat,lon
            
def getGeoLocationForCity(city):
    with open("preprocessing/city_keys.csv","r") as f:
        lines = f.readlines()
        for line in lines:
            abbreviation = line.split(",")[0].strip()
            if abbreviation == city:
                lat = line.split(",")[1].strip()
                lon = line.split(",")[2].strip()
                return lat,lon

def addGeoLocationsToHashtagTable():
    con = sql.connect('data.db')
    hashtags = md.fetchAllHashtags()
    for hashtag in hashtags:
        hashtag_id = hashtag[0]
        city = hashtag[1]
        university = hashtag[4]
        lat, lon = None, None
        if university != None:
            lat, lon = getGeoLocationForUni(university)
        elif city != None:
            lat, lon = getGeoLocationForCity(city)
        if lat != None and lon != None:
            md.updateHashtagLocation(con,hashtag_id,lat,lon)

def fillLocationsTable():
    con = sql.connect('data.db')
    with open("preprocessing/uni_keys.csv","r") as f:
        lines = f.readlines()
        for line in lines:
            abbreviation = line.split(",")[1].strip()
            lat = line.split(",")[4].strip()
            lon = line.split(",")[5].strip()
            md.insertLocation(con,abbreviation,lat,lon)
    with open("preprocessing/city_keys.csv","r") as f:
        lines = f.readlines()
        for line in lines:
            abbreviation = line.split(",")[0].strip()
            lat = line.split(",")[1].strip()
            lon = line.split(",")[2].strip()
            md.insertLocation(con,abbreviation,lat,lon)

def moveUserLocationsToLocationsTable():
    #exit noch zum Testen verwenden
    #bei korrekter Funktionsweise können anschließend die Spalten lat, lon, abroad aus users gelöscht werden
    users = md.fetchAllUsers(orderByLat= True)
    con = md.setupConnection()
    location_id = 1
    f = "log/geomapping/moveToLocations.log"
    for index,user in enumerate(users):
        user_id = user[0]
        lat = user[14]
        lon = user[15]
        abroad = user[7]
        parsed_location = user[6]
        if lat != None and lon != None:
            location_exists,location_id_existing = md.checkIfLocationAlreadyExists(con,lat,lon,abroad)
            if not location_exists:
                log(f"Location of user {user_id} does not exist yet",f)
                md.insertLocation(con,location_id,lat,lon,abroad)
                location_id_new = location_id
                location_id += 1
            else:
                log(f"Location of user {user_id} already exists with id {location_id_existing}",f)
                location_id_new = location_id_existing  
            md.setLocationIdInUsersTable(con,user_id,location_id_new) #overwrites parsed_location
            log(f"{user_id},{parsed_location},{location_id_new}",f) 
            log(f"------------------------------",f)   
            #exit()
            
            if index % 1000 == 0:
                print(f"-------------------------------Processed {index} users")

def reparseNominatimLocationsWithPhoton():
    all_users = md.fetchAllUsers()
    con = md.setupConnection()
    client = setUpClient("new_user", "photon")
    stats = {"location_found":0,"location_not_found":0}
    counter = 0
    distances = []
    with open("log/geomapping/geomapping_log.csv", "a") as f:
        with open ("log/geomapping/geocoder_comparison.csv", "a") as f2:
            f2.write(f"user_id,lat_old,lon_old,lat_new,lon_new,distance\n")
            for user in all_users:
                user_id = user[0]
                location_raw = user[5]
                location_checked = user[16]
                lat_old = user[14]
                lon_old = user[15]
                if location_raw != None and location_checked == 1:
                    print(f"{user_id}")
                    f.write(f"{user_id},{location_raw.replace(',',';')}")
                    lat,lon,city,location_abroad = parseRawLocationPhoton(client, location_raw)
                    f.write(f",{city}\n")
                    counter += 1
                    if lat != None and lon != None and city != None and location_abroad != None:
                        if lat_old != None and lon_old != None:
                            distance = calculateDistanceLatLonOldNew(user_id,lat_old,lon_old,lat,lon,f2)
                            distances.append(distance)
                        md.updateUserLocation(con,user_id,lat,lon,city,location_abroad,geocoder = "photon")
                        stats["location_found"] += 1
                    else:
                        stats["location_not_found"] += 1
                        md.setLocationCheckedForUser(con,user_id, geocoder = "photon")
                    if counter % 100 == 0:
                        print(f"-------------------------Processed {counter} users")
            print("Average distance: ", np.mean(distances))
            print("Median distance: ", np.median(distances))
            f.write(f"Stats: {stats}\n")
            f2.write(f"Average distance: {np.mean(distances)}\n")
            f2.write(f"Median distance: {np.median(distances)}\n")
            plotDistances(distances)
    print(stats)

def calculateDistanceLatLonOldNew(user_id,lat_old,lon_old,lat,lon,file):
    distance = np.sqrt((lat_old-lat)**2 + (lon_old-lon)**2)
    file.write(f"{user_id},{lat_old},{lon_old},{lat},{lon},{distance}\n")
    return distance

def plotDistances(distances):
    plt.hist(distances, bins = 100)
    plt.title("Distance between old and new coordinates")
    plt.xlabel("Distance")
    plt.ylabel("Frequency")
    plt.savefig("preprocessing/visualize/distance_histogram.png")
    plt.show()

def constructTweetLocalizationTable():
    con = md.setupConnection()
    tweets = md.fetchAllTweets()
    for index,tweet in enumerate(tweets):
        tweet_id = tweet[0]
        user_id = tweet[12]
        geo_id = tweet[13]
        md.constructTweetLocalizationOptions(con,tweet_id,user_id,geo_id)
        if index % 10000 == 0:
            print(f"Processed {index} tweets")

def addEventLocationsToGoogleMaps():
    locations = md.fetchAllEvents(eventsReduced=False,onlyChecked=True)
    with open("preprocessing/visualize/event_locationsCheckedGoogleMaps.csv","w") as f:
        f.write("ID,lat,lon,date,details\n")
        for location in locations:
            id = location[0]
            date = location[1]
            lon = location[4]
            lat = location[3]
            details = location[5]
            f.write(f"{id};{lat};{lon};{date};{details}\n")

def addHashtagLocationsToGoogleMaps():
    locations = md.fetchAllTweetImpactLocations(moreDetails=True)
    with open("preprocessing/visualize/impact_locationsGoogleMaps1.csv","w") as f:
        with open("preprocessing/visualize/impact_locationsGoogleMaps2.csv","w") as f2:
            with open("preprocessing/visualize/impact_locationsGoogleMaps3.csv","w") as f3:
                f.write("ID,text,lat,lon,date,type,url,retweet_count,reply_count,like_count,quote_count,impression_count,user_id,geo_id,location_id\n")
                f2.write("ID,text,lat,lon,date,type,url,retweet_count,reply_count,like_count,quote_count,impression_count,user_id,geo_id,location_id\n")
                f3.write("ID,text,lat,lon,date,type,url,retweet_count,reply_count,like_count,quote_count,impression_count,user_id,geo_id,location_id\n")
                for index,location in enumerate(locations):
                    text = location[1].replace("\n"," ").replace("\r"," ")
                    if index < len(locations)/3:
                        f.write(f"{location[0]},{text},{location[2]},{location[3]},{location[4]},{location[5]},{location[6]},{location[7]},{location[8]},{location[9]},{location[10]},{location[11]},{location[12]},{location[13]},{location[14]}\n")
                    elif index < len(locations)/3*2:
                        f2.write(f"{location[0]},{text},{location[2]},{location[3]},{location[4]},{location[5]},{location[6]},{location[7]},{location[8]},{location[9]},{location[10]},{location[11]},{location[12]},{location[13]},{location[14]}\n")
                    else:
                        f3.write(f"{location[0]},{text},{location[2]},{location[3]},{location[4]},{location[5]},{location[6]},{location[7]},{location[8]},{location[9]},{location[10]},{location[11]},{location[12]},{location[13]},{location[14]}\n")

if __name__ == "__main__":
    #reparseNominatimLocationsWithPhoton()
    #addGeoLocationsToHashtagTable()
    #visualizeCoordinates()
    #visualizeOfflineEventCoordinates()
    #moveUserLocationsToLocationsTable()
    #constructTweetLocalizationTable()
    addEventLocationsToGoogleMaps()
    #addHashtagLocationsToGoogleMaps()