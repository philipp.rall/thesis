import json
import matplotlib.pyplot as plt


def createGeneralStatistics(data):
    counters = {"Loops":0,"Tweets":0,"Users":0,"Places":0,"Geo":0}
    dates = {}
    counters["Loops"] = len(data)

    for loop_counter, loop in enumerate(data):
        counters["Tweets"] += len(loop["data"])
        counters["Users"] += len(loop["includes"]["users"])
        if "places" in loop["includes"]:
            counters["Places"] += len(loop["includes"]["places"])
        for tweet in loop["data"]:
            if "geo" in tweet:
                counters["Geo"] += 1
            day = tweet["created_at"][:10]
            if day not in dates:
                dates.update({day:0})
            else: 
                dates[day] += 1

    return counters,dates   

def printCounters(counters):
    print(f"{counters['Loops']} loops found")
    print(f"{counters['Tweets']} tweets found")
    print(f"{counters['Users']} users found")
    print(f"{counters['Places']} places found")
    print(f"{counters['Geo']} geo_locations in tweets found, {round(counters['Geo']/counters['Tweets'])}% of all tweets have a geo-location")
  
def showTimeOverview(dates,counters,filename):
    plt.figure()
    plt.title(f"{counters['Tweets']} tweets from {counters['Users']} users with {counters['Geo']} geo-locations")
    plt.ylabel("Number of tweets")
    plt.xlabel("Time")
    xlabels = []
    for date in dates.keys():
        xlabels.append(f"{date[-2:]}.{date[5:7]}")
    xlabels.reverse()
    values = list(dates.values())
    values.reverse()
    plt.plot(xlabels,values)

    plt.savefig(f"analysis/{filename}.pdf")
    plt.show()

def searchSpecificTerm(data, term, mode='text'):
    term_counter = 0
    print("Searching for term")
    for loop in data:
        for tweet in loop["data"]:
            if mode=='text':
                print("text")
                if tweet["text"].__contains__(term):
                    print(tweet["text"])
                    term_counter+=1
            if mode=='id':
                if tweet["id"] == (term):
                    print(tweet["text"])
                    term_counter+=1
            elif mode == 'retweet_id':
                if "referenced_tweets" in tweet:
                    if tweet["referenced_tweets"][0]["id"] == term:
                        term_counter+=1
            #else:
                #exit()
    print(f"{term} occured in {term_counter} tweets")
    return term_counter

def checkLocations(data, filename):
    with open(f"analysis/{filename}_locations.csv","w") as file:
        user_counter = 0
        location_counter = 0
        for loop in data:
            for user in loop["includes"]["users"]:
                user_counter += 1
                if "location" in user.keys():
                    location_counter += 1
                    file.write(user["location"] + "\n")
        
        file.write(f"\n----------------------\n {location_counter} of {user_counter} have a location, {(location_counter/user_counter)*100}%")


if __name__ == '__main__':
    keyword = "#FeesMustFall"
    start_time = '2015-10-12'
    end_time = '2015-10-30'

    filename = f"{keyword[1:]}_{start_time}_{end_time}"
    with open(f"data/{filename}.json", "r") as json_file:
        data = json.load(json_file)
        #counters, dates = createGeneralStatistics(data)
        #printCounters(counters)
        #showTimeOverview(dates,counters,filename)
        searchSpecificTerm(data,"…")
        #checkLocations(data,filename)