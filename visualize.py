import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import plotly.express as px
import manage_database as md
import pandas as pd
import numpy as np
import analysis as an
from datetime import datetime, timezone
import math
from numpy import trapz
import random

def visualizeUserLocations():
    locations = md.fetchAllLocations(select="hashtag")
    BBox = (15.82,35.367,-21.943,-35.343)
    raw_map = plt.imread('/Users/philipp/Documents/GitHub/thesis/preprocessing/visualize/map.png')
    fig, ax = plt.subplots(figsize = (8,7))
    for location in locations:
        if location[3] == 0:
            lat = BBox[3] - location[1] + BBox[2]
            ax.scatter(location[2],lat,zorder=1,alpha=0.2,c='b',s=10)
    ax.set_title('Impact Locations in South Africa')
    ax.set_xlim(BBox[0],BBox[1])
    ax.set_ylim(BBox[2],BBox[3])
    ax.imshow(raw_map, zorder=0, extent = BBox, aspect= 'equal')
    plt.savefig("preprocessing/visualize/impact_locations.png")
    plt.show()

def visualizeAsHeatmap(type = "events"):
    if type == "events":
        locations = md.fetchAllEventLocations()
    df = pd.DataFrame(locations,columns=["lat","lon"])
    df = df.assign(date=range(len(df)))
    print(df)
    fig = px.density_mapbox(df, lat='lat', lon='lon', z='date', center=dict(lat=-28.859408, lon=24.958851), zoom=4, radius=20, mapbox_style="stamen-terrain")
    fig.show()
    #fig.write_image(f"preprocessing/visualize/{type}_heatmap.pdf")

def visualizeOfflineProtestProgress():
    locations = md.fetchAllEventLocations(withDate=True)
    df = pd.DataFrame(locations,columns=["lat","lon","date"])
    df = df.sort_values(by=['date'])

    # visualize events for each day
    for date in df["date"].unique():
        df_date = df[df["date"] <= date]
        fig = px.density_mapbox(df_date, lat='lat', lon='lon', center=dict(lat=-28.859408, lon=24.958851), zoom=4, radius=20, mapbox_style="stamen-terrain", title=f"Events on {date}")
        fig.show()
        fig.write_image(f"preprocessing/visualize/spread/events_{date}.pdf")

def visualizeAllLocationsAsHeatMap():
    locations = md.fetchAllLocations()
    df = pd.DataFrame(locations,columns=["lat","lon"])
    fig = px.density_mapbox(df, lat='lat', lon='lon', center=dict(lat=-28.859408, lon=24.958851), zoom=4, radius=10, mapbox_style="stamen-terrain", title="All Locations")
    fig.show()
    fig.write_image(f"preprocessing/visualize/all_locations.pdf")

def visualizeCoordinates():
    locations = md.fetchAllLocations(select="all")
    BBox = (15.82,35.367,-21.943,-35.343)
    raw_map = plt.imread('/Users/philipp/Documents/GitHub/thesis/preprocessing/visualize/map.png')
    fig, ax = plt.subplots(figsize = (8,7))
    for location in locations:
        if location[3] == 0:
            lat = BBox[3] - location[1] + BBox[2]
            ax.scatter(location[2],lat,zorder=1,alpha=0.2,c='b',s=10)
    ax.set_title('User Locations in South Africa')
    ax.set_xlim(BBox[0],BBox[1])
    ax.set_ylim(BBox[2],BBox[3])
    ax.imshow(raw_map, zorder=0, extent = BBox, aspect= 'equal')
    plt.savefig("preprocessing/visualize/user_locations.png")
    plt.show()

def visualizeOfflineEventCoordinates(andTwitter = False):
    locations = md.fetchAllEventLocations()
    if andTwitter:
        twitter_locations = md.fetchAllLocations()
    BBox = (15.82,35.367,-21.943,-35.343)
    raw_map = plt.imread('/Users/philipp/Documents/GitHub/thesis/preprocessing/visualize/map.png')
    fig, ax = plt.subplots(figsize = (8,7))
    for location in locations:
        lat = BBox[3] - location[0] + BBox[2]
        ax.scatter(location[1],lat,zorder=1,alpha=0.2,c='r',s=10)
    if andTwitter:
        for location in twitter_locations:
            if location[3] == 0:
                lat = BBox[3] - location[1] + BBox[2]
                ax.scatter(location[2],lat,zorder=1,alpha=0.2,c='b',s=10)
        ax.set_title('Event and Tweet Locations in South Africa')
    else:
        ax.set_title('Event Locations in South Africa')
    ax.set_xlim(BBox[0],BBox[1])
    ax.set_ylim(BBox[2],BBox[3])
    ax.imshow(raw_map, zorder=0, extent = BBox, aspect= 'equal')
    plt.savefig("preprocessing/visualize/reduced_event_locations.png")
    plt.show()

def getProtestDataForVisualization(events = md.fetchAllEvents(),startDay=12,endDay=30):
    data = {}
    for day in range(startDay,endDay+1):
        number_of_events = 0
        for event in events:
            date = event[2]
            if type(date) == int and date == int(f"201510{day}"):# or type(date) == np. and date == f"201510{day}":
                number_of_events += 1
        data.update({float(day):number_of_events})
    return data

def putTweetsInDayContainers(tweets=md.fetchAllTweets()):
    count_tweets = 0
    for tweet in tweets:
        if tweet[2] > 20151012 and tweet[2] < 20151031:
            count_tweets += 1
    print(count_tweets)
    data = {}
    for day in range(12,31):
        number_of_tweets = 0
        for tweet in tweets:
            if str(tweet[2])[:10] == f"2015-10-{day}" or str(tweet[2]) == f"201510{day}":
                number_of_tweets += 1
        data.update({day:number_of_tweets})
    print(data)
    return data, count_tweets

def putTweetsInHourContainers(tweets,startDay=12,endDay=30):
    data = {}
    for day in range(startDay,endDay+1):
        for hour in range(0,24):
            number_of_tweets = 0
            if int(hour)<10:
                hour_str = "0"+str(hour)
            else:
                hour_str = str(hour)
            for tweet in tweets:
                if str(tweet[3])[:13] == f"2015-10-{day}T{hour_str}":
                    number_of_tweets += 1

            point_in_time = float(day)+hour/24
            data.update({point_in_time:number_of_tweets})
    return data

def putTweetsInQuarterDayContainers(tweets,startDay=12,endDay=30):
    data = {}
    for day in range(startDay,endDay+1):
        for hour in range(0,24,6):
            number_of_tweets = 0
            for tweet in tweets:
                if str(tweet[3])[:10] == f"2015-10-{day}":
                    hourTweet = str(tweet[3])[11:13]
                    if int(hourTweet) < (hour+6) and int(hourTweet) >= hour:
                        number_of_tweets += 1
            point_in_time = float(day)+hour/24
            data.update({point_in_time:number_of_tweets})
    return data

def putTweetsInHalfDayContainers(tweets,startDay=12,endDay=30):
    data = {}
    for day in range(startDay,endDay+1):
        for hour in range(0,24,12):
            number_of_tweets = 0
            for tweet in tweets:
                if str(tweet[3])[:10] == f"2015-10-{day}":
                    hourTweet = str(tweet[3])[11:13]
                    if int(hourTweet) < (hour+12) and int(hourTweet) >= hour:
                        number_of_tweets += 1
            point_in_time = float(day)+hour/24+0.25
            data.update({point_in_time:number_of_tweets})
    return data

def splitTweetsByType(tweets):
    assertion_tweets = []
    metavoicing_tweets = []
    for tweet in tweets:
        tweet_type = tweet[6]
        if tweet_type == "original":
            assertion_tweets.append(tweet)
        else:
            metavoicing_tweets.append(tweet)
    return assertion_tweets,metavoicing_tweets

def getUserDataForVisualization():
    users = md.getUniqueUsersWithFirstTweetTime()
    data = {}
    for day in range(12,31):
        number_of_users = 0
        for user in users:
            if user[1][:10] == f"2015-10-{day}":
                number_of_users += 1
        data.update({f"{day}.":number_of_users})
    return data

def getCulimnatedUsersForVisualization():
    users = md.getUniqueUsersWithFirstTweetTime()
    data = {}
    number_of_users = 0
    for day in range(12,31):
        for user in users:
            if user[1][:10] == f"2015-10-{day}":
                number_of_users += 1
        data.update({f"{day}.":number_of_users})
    return data

def plotData(data, xlabel,ylabel,title,filename,save=False, bar=False):
    plt.figure()
    if bar:
        plt.bar(data.keys(),data.values())
    else:
        plt.plot(data.keys(),data.values())
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.tight_layout()
    #plt.axis.set_major_formatter(ticker.FuncFormatter(lambda x, p: f"{int(x):,}".replace(",",".")))
    if save and bar:
        plt.savefig(f"preprocessing/visualize/general_analysis/{filename}_bar.pdf")
    elif save:
        plt.savefig(f"preprocessing/visualize/general_analysis/{filename}.pdf")
    plt.show()

def visualizeProtestsOverTime(save=False):
    data = getProtestDataForVisualization()
    plotData(data,"Oktober 2015","Anzahl an Protesten","Entwicklung der Proteste","protests_over_time",save)

def visualizeTweetsOverTime(save=False):
    data = putTweetsInDayContainers()
    plotData(data,"Oktober 2015","Anzahl an Tweets","Entwicklung der Tweets","tweets_over_time",save,bar=True)

def visualizeUsersOverTime(save=False):
    data = getCulimnatedUsersForVisualization()
    plotData(data,"Oktober 2015","Anzahl an Nutzenden","Entwicklung der beteiligten Nutzenden","culimnated_users_over_time",save)

def getAttributeNameAndUnit(name):
    if name == "numberOfTweets":
        return "all Tweets",False
    elif name == "numberOfProtests":
        return "all Protests",False
    elif name == "numberOfUsers":
        return "all Users",False
    elif name == "fractionUniqueUserTweets":
        return "Tweets only localized by User",True
    elif name == "daysOfProtest":
        return "days of protest",False
    elif name == "fractionUniqueGeoTweets":
        return "Tweets only localized by Geo",True
    elif name == "averageTweetsPerUser":
        return "average Tweets per User",False
    elif name == "originalTweetRatio":
        return "retweet ratio",True
    elif name == "fractionOfUsersSupportingFromAbroad":
        return "Users supporting from abroad",True
    elif name == "usersPerStudent":
        return "ratio of users to all students",True
    return name,False

def convertDateStringToDate(date):
    return f"{date[6:]}.{date[4:6]}.{date[0:4]}"

def getTextOfKPI(stats_dict, clusterIdentifier, name):
    attributeName,percentage = getAttributeNameAndUnit(name)
    returnText = ""
    bold = False
    returnValue = 0
    if name == "daysOfProtest":
        sortedKPI = sorted(stats_dict, key=lambda x: x[1][0], reverse=True)
    else:
        sortedKPI = sorted(stats_dict, key=lambda x: x[1], reverse=True)
    index = [x[0] for x in sortedKPI].index(clusterIdentifier) if clusterIdentifier in [x[0] for x in sortedKPI] else -1
    if percentage:
        outputValue = str(round(sortedKPI[index][1]*100,2))+"%"
    else:
        if not name == "daysOfProtest":
            outputValue = round(sortedKPI[index][1],2)
        else:
            outputValue = sortedKPI[index][1][0]
    if name == "daysOfProtest":
        values = [x[1][0] for x in sortedKPI if not x[1][0] == None]
        value = sortedKPI[index][1][0]
    else:
        values = [x[1] for x in sortedKPI]
        value = sortedKPI[index][1]
    top10 = np.percentile(values, 90)
    top25 = np.percentile(values, 75)
    bottom25 = np.percentile(values, 25)
    bottom10 = np.percentile(values, 10)
    if index == 0:
        returnText =  f"Top 1 of {attributeName} ({outputValue})"
        bold = True
        returnValue = 1
    elif value <= bottom10:
        returnText = f"Bottom 10% of {attributeName} ({outputValue})"
        returnValue = 0.1
    elif value <= bottom25:
        returnText = f"Bottom 25% of {attributeName} ({outputValue})"
        returnValue = 0.25
    elif value <= top25:
        returnText = f"Middle 50% of {attributeName} ({outputValue})"
        returnValue = 0.5
    elif value <= top10:
        returnText = f"Top 25% of {attributeName} ({outputValue})"
        bold = True
        returnValue = 0.75
    else:
        returnText = f"Top 10% of {attributeName} ({outputValue})"
        bold = True
        returnValue = 0.9

    if name == "daysOfProtest" and sortedKPI[index][1][0] > 0:
        firstDayOfProtest = str(sortedKPI[index][1][1])
        lastDayOfProtest = str(sortedKPI[index][1][2])
        returnText += f", {convertDateStringToDate(firstDayOfProtest)} - {convertDateStringToDate(lastDayOfProtest)}"
    return returnText,bold,returnValue

def generateStatsText(stats,clusterIdentifier,maxY):
    maxY *= 0.85
    for counter,key in enumerate(stats.keys()):
        print(f"   -{key}")
        text,bold,_ = getTextOfKPI(stats[key].items(),clusterIdentifier,key)
        plt.text(8, maxY-(maxY/10)*counter, text, ha="right", fontsize=10,fontweight=['normal','bold'][bold])
 
def generateThesisText(stats,clusterIdentifier,maxY,thesis3Stats,foldername):
    maxY *= 0.85
    startX = 8.5
    protestPredictors = 0

    if foldername == "uniCircles":
        filename = "analysis/anas/uniCircles/thesissheets/thesis_results.csv"
    else:
        filename = "analysis/anas/afMvmNewCorrectUserCast/thesissheets/thesis_results.csv"

    with open(filename,"a") as f:
        #f.write("ID,T1_tweetRatio,T1_TweetsPerUser,T1_decision,T2_usersPerStudent,T2_decision,T4_tweetRatio,T4_decision,T5_fractionOfUsersSupportingFromAbroad,T5_decision,T3_morningProtests,T3_positiveRelation,T3_reciprocalRelation,protestReality,protestPrediction\n")
        text1,bold1,value1 = getTextOfKPI(stats["numberOfProtests"].items(),clusterIdentifier,"numberOfProtests")
        text2,bold2,value2 = getTextOfKPI(stats["numberOfTweets"].items(),clusterIdentifier,"numberOfTweets")
        plt.text(startX, maxY-(maxY/10)*0, f"{text1} and {text2}", ha="right", fontsize=10,fontweight="normal")
        output_str = f"{clusterIdentifier},"

        #Thesis 1
        text1,bold1,value1 = getTextOfKPI(stats["originalTweetRatio"].items(),clusterIdentifier,"originalTweetRatio")
        text2,bold2,value2 = getTextOfKPI(stats["averageTweetsPerUser"].items(),clusterIdentifier,"averageTweetsPerUser")
        color = "tab:green"
        if value1 <= 0.25 or value2 <= 0.25:
            color = "tab:red"
        else:
            protestPredictors += 1
        text = f"T1: {text1}\nand {text2}"
        plt.text(startX, maxY-(maxY/10)*2, text, ha="right", fontsize=10,color=color)
        output_str += f"{value1},{value2},{['protest','no protest'][value1 <= 0.25 or value2 <= 0.25]},"

        #Thesis 2
        text,bold,value = getTextOfKPI(stats["usersPerStudent"].items(),clusterIdentifier,"usersPerStudent") #TODO KPI ergänzen
        color = "tab:green"
        if value <= 0.25:
            color = "tab:red"
        else: 
            protestPredictors += 1
        text = f"T2: {text}"
        plt.text(startX, maxY-(maxY/10)*3, text, ha="right", fontsize=10,color=color)
        output_str += f"{value},{['protest','no protest'][value <= 0.25]},"

        #Thesis 4
        text,bold,value = getTextOfKPI(stats["originalTweetRatio"].items(),clusterIdentifier,"originalTweetRatio")
        color = "tab:green"
        if value <= 0.25:
            color = "tab:red"
        else:
            protestPredictors += 1
        text = f"T4: {text}"
        plt.text(startX, maxY-(maxY/10)*4, text, ha="right", fontsize=10,color=color)
        output_str += f"{value},{['protest','no protest'][value <= 0.25]},"

        #Thesis 5
        text,bold,value = getTextOfKPI(stats["fractionOfUsersSupportingFromAbroad"].items(),clusterIdentifier,"fractionOfUsersSupportingFromAbroad")
        color = "tab:green"
        if value <= 0.25:
            color = "tab:red"
        else:
            protestPredictors += 1
        text = f"T5: {text}"
        plt.text(startX, maxY-(maxY/10)*5, text, ha="right", fontsize=10,color=color)
        output_str += f"{value},{['protest','no protest'][value <= 0.25]}"

        #Thesis 3 - bloße Werte reichen aus, keine KPI-Einordnung mit Perzentilen notwendig
        value = thesis3Stats['morningProtests']
        text = f"T3.1: Bei {str(int(round(value*100)))}% der Protesttage morgens mehr Online-Protest als abends"
        plt.text(startX, maxY-(maxY/10)*7, text, ha="right", fontsize=10,fontweight = "bold" if value >= 0.7 else "normal")
        output_str += f",{value}"

        value = thesis3Stats['positiveRelation']
        text = f"T3.2: Bei {str(int(round(value*100)))}% der Protesttage am Tag davor mehr Online-Protest als am Tag danach"
        plt.text(startX, maxY-(maxY/10)*8,text, ha="right", fontsize=10,fontweight = "bold" if value >= 0.7 else "normal")
        output_str += f",{value}"

        value = thesis3Stats['reciprocalRelation']
        text = f"T3.3: Bei {str(int(round(value*100)))}% der Protesttage mehr Online-Protest als im Durchschnitt"
        plt.text(startX, maxY-(maxY/10)*9,text, ha="right", fontsize=10,fontweight = "bold" if value >= 0.7 else "normal")
        output_str += f",{value}"

        protest_reality = ['no protest','protest'][stats["numberOfProtests"][clusterIdentifier] > 0]
        protest_prediction = ['no protest','protest'][protestPredictors >= 3]

        output_str += f",{thesis3Stats['reciprocalRelation2']},{thesis3Stats['positiveRelation2']},{thesis3Stats['positivityValue']},{thesis3Stats['reciprocityValue']},{protest_reality.upper()}"
        output_str += f",{protest_prediction}"
        output_str += f",{['wrong','correct'][protest_reality == protest_prediction]}"
        f.write(output_str+"\n")

def calculateThesis3Stats(tweet_data,event_data):
    thesis3Stats = {
        "morningProtests":0,
        "positiveRelation":0,
        "positiveRelation2":0,
        "reciprocalRelation":0,
        "reciprocalRelation2":0,
        "positivityValue":0,
        "reciprocityValue":0
    }
    morningMoreThanEveningCounter = 0
    dayBeforeMoreThanAfterCounter = 0
    dayBeforeMoreThanAverageCounter = 0
    moreTweetsThanAverage = 0
    moreTweetsThanAverageOnNotEventDays = 0
    sumOfPositivityValues = 0
    sumOfReciprocityValues = 0
    totalProtestDays = 0
    allTweets = sum(tweet_data.values())
    averageTweetsPerDay = 2*(allTweets/len(tweet_data.keys()))
    numberOfNotEventDays = 0
    averageTweetsOnNotEventDays = 0
    numberOfTweetsNotEventDays = 0
    numberOfEventDays = 0
    averageTweetsOnEventDays = 0
    numberOfTweetsEventDays = 0
    for day in event_data.keys():
        if event_data[day] == 0:
            numberOfNotEventDays += 1
            morningTimePoint = float(f"{int(day)}.25")
            eveningTimePoint = float(f"{int(day)}.75")
            numberOfTweetsNotEventDays += tweet_data[morningTimePoint] + tweet_data[eveningTimePoint]
        else:
            numberOfEventDays += 1
            morningTimePoint = float(f"{int(day)}.25")
            eveningTimePoint = float(f"{int(day)}.75")
            numberOfTweetsEventDays += tweet_data[morningTimePoint] + tweet_data[eveningTimePoint]

    print(f"Number of not event days: {numberOfNotEventDays}")
    print(f"Number of tweets on not event days: {numberOfTweetsNotEventDays}")
    averageTweetsOnNotEventDays = numberOfTweetsNotEventDays/numberOfNotEventDays
    if numberOfEventDays > 0:
        averageTweetsOnEventDays = numberOfTweetsEventDays/numberOfEventDays
    print(f"Average tweets on not event days: {averageTweetsOnNotEventDays}")

    for event_day in event_data.keys():
        if event_data[event_day] > 0:
            totalProtestDays += 1
            morningTimePoint = float(f"{int(event_day)}.25")
            eveningTimePoint = float(f"{int(event_day)}.75")
            amountOfMorningTweets = tweet_data[morningTimePoint]
            amountOfEveningTweets = tweet_data[eveningTimePoint]
            amountOfTweets = amountOfMorningTweets + amountOfEveningTweets
            if amountOfMorningTweets > amountOfEveningTweets:
                morningMoreThanEveningCounter += 1

            dayBeforeMorning = float(f"{int(event_day)-1}.25")
            dayBeforeEvening = float(f"{int(event_day)-1}.75")
            if event_day + 1 > 30:
                continue
            dayAfterMorning = float(f"{int(event_day)+1}.25")
            dayAfterEvening = float(f"{int(event_day)+1}.75")
            amountOfTweetsDayBefore = tweet_data[dayBeforeMorning] + tweet_data[dayBeforeEvening]
            amountOfTweetsDayAfter = tweet_data[dayAfterMorning] + tweet_data[dayAfterEvening]
            if amountOfTweetsDayBefore > amountOfTweetsDayAfter:
                dayBeforeMoreThanAfterCounter += 1
            
            if amountOfTweets > averageTweetsPerDay:
                moreTweetsThanAverage += 1
            
            if amountOfTweets > averageTweetsOnNotEventDays:
                moreTweetsThanAverageOnNotEventDays += 1

            if event_data[event_day-1] > 0:
                if amountOfTweetsDayBefore > averageTweetsOnEventDays:
                    dayBeforeMoreThanAverageCounter += 1
                sumOfPositivityValues += amountOfTweetsDayBefore/averageTweetsOnEventDays -1 #wie viel relativ mehr tweets als im durchschnitt, bei 0 gleich viele
            else:
                if amountOfTweetsDayBefore > averageTweetsOnNotEventDays:
                    dayBeforeMoreThanAverageCounter += 1
                sumOfPositivityValues += amountOfTweetsDayBefore/averageTweetsOnNotEventDays -1 #wie viel relativ mehr tweets als im durchschnitt, bei 0 gleich viele
            sumOfReciprocityValues += amountOfTweets/averageTweetsOnNotEventDays -1 #wie viel relativ mehr tweets als im durchschnitt, bei 0 gleich viele

    print(f"Total protest days: {totalProtestDays}")
    print(f"Events with more tweets than average not event days: {moreTweetsThanAverageOnNotEventDays}")
    if totalProtestDays == 0:
        return thesis3Stats
    thesis3Stats['morningProtests'] = morningMoreThanEveningCounter/totalProtestDays
    thesis3Stats['positiveRelation'] = dayBeforeMoreThanAfterCounter/totalProtestDays
    thesis3Stats['positiveRelation2'] = dayBeforeMoreThanAverageCounter/totalProtestDays
    thesis3Stats['positivityValue'] = dayBeforeMoreThanAverageCounter
    thesis3Stats['reciprocalRelation'] = moreTweetsThanAverage/totalProtestDays
    thesis3Stats['reciprocalRelation2'] = moreTweetsThanAverageOnNotEventDays/totalProtestDays
    thesis3Stats['reciprocityValue'] = moreTweetsThanAverageOnNotEventDays
    print(f"Reciprocal relation: {thesis3Stats['reciprocalRelation2']}")

    print(f"Positivity value: {thesis3Stats['positivityValue']}")
    print(f"Reciprocity value: {thesis3Stats['reciprocityValue']}")
    print(f"Positive relation: {thesis3Stats['positiveRelation2']}")


    return thesis3Stats


def generateThesisSheets(save=False,offline_data=None,online_data=None,user_data=None,geo_data=None,allTweets=None,clusterName="",clusterIdentifier="All",cluster_technique="",offline_data_type="",stats=None):
    event_data = getProtestDataForVisualization(offline_data)
    allTweets_data = putTweetsInHalfDayContainers(allTweets)

    assertion_tweets,_ = splitTweetsByType(allTweets)
    assertion_data = putTweetsInHalfDayContainers(assertion_tweets)
    fig, ax1 = plt.subplots()
    fig.set_figheight(5)
    fig.set_figwidth(12) #6
    
    labelAlreadyAdded = []
    for event in event_data.keys():
        numberOfProtests = event_data[event]
        if event_data[event] > 0:
            labeltext = ""
            if numberOfProtests not in labelAlreadyAdded:
                labeltext = f"{numberOfProtests} {'Proteste' if numberOfProtests > 1 else 'Protest'}"
                labelAlreadyAdded.append(numberOfProtests)
            plt.axvspan(event,event+1,color='tab:green',alpha=event_data[event]/8,label=labeltext)

    ax1.plot(allTweets_data.keys(), allTweets_data.values(),'-',color='tab:blue')
    ax1.fill_between(allTweets_data.keys(),allTweets_data.values(),color='tab:blue',alpha=0.5,label="Alle Tweets")

    ax1.plot(assertion_data.keys(), assertion_data.values(),'-',color='tab:orange')
    ax1.fill_between(assertion_data.keys(),assertion_data.values(),alpha=0.5,color='tab:orange',label="Assertion-Tweets")
    
    thesis3Stats = calculateThesis3Stats(allTweets_data,event_data)
    generateThesisText(stats,clusterIdentifier,max(allTweets_data.values()),thesis3Stats,cluster_technique)

    ax1.set_xlabel('Oktober 2015')
    ax1.set_ylabel('Anzahl an Tweets')
    ax1.legend()
    #ax1.set_ylabel('Number of Tweets', color='tab:blue')
    ticks = range(12,31)
    tickla = [f'{tick}.' for tick in ticks]
    ax1.xaxis.set_ticks(ticks)
    ax1.xaxis.set_ticklabels(tickla)
    ax1.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, p: f"{int(x):,}".replace(","," ")))
    #plt.suptitle(f"{clusterName} ({clusterIdentifier})")
    #plt.title("Protestaktivität in Südafrika")
    plt.tight_layout()
    if save:
        if cluster_technique == "uniCircles":
            path = "analysis/anas/uniCircles/thesissheets"
        else:
            path = "analysis/anas/afMvmNewCorrectUserCast/thesissheets"
        plt.savefig(f"{path}/{clusterIdentifier}_{clusterName.replace(' ','_')}.pdf")
        #plt.show()
    print(f"Visualized {clusterIdentifier}_{clusterName.replace(' ','_')}.pdf")

def generateSouthAfricaSheet(save=False,offline_data=None,online_data=None,user_data=None,geo_data=None,allTweets=None,clusterName="",clusterIdentifier="All",cluster_technique="",offline_data_type="",stats=None):
    event_data = getProtestDataForVisualization(offline_data)
    allTweets_data = putTweetsInHalfDayContainers(allTweets)

    assertion_tweets,_ = splitTweetsByType(allTweets)
    assertion_data = putTweetsInHalfDayContainers(assertion_tweets)
    fig, ax1 = plt.subplots()
    fig.set_figheight(5)
    fig.set_figwidth(12)
    
    labelAlreadyAdded = []
    event_data = dict(sorted(event_data.items(), key=lambda x: x[1], reverse=True))
    for idx,event in enumerate(event_data.keys()):
        if event_data[event] > 0:
            numberOfProtests = event_data[event]
            labeltext = ""
            if numberOfProtests not in labelAlreadyAdded:
                labeltext = f"{numberOfProtests} {'Proteste' if numberOfProtests > 1 else 'Protest'}"
                labelAlreadyAdded.append(numberOfProtests)
            plt.axvspan(event,event+1,color='tab:green',alpha=event_data[event]/18,label=labeltext)

    ax1.plot(allTweets_data.keys(), allTweets_data.values(),'-',color='tab:blue')
    ax1.fill_between(allTweets_data.keys(),allTweets_data.values(),color='tab:blue',alpha=0.5)

    # ax1.plot(assertion_data.keys(), assertion_data.values(),'-',color='tab:orange')
    # ax1.fill_between(assertion_data.keys(),assertion_data.values(),alpha=0.5,color='tab:orange',label="Assertion-Tweets")
    
    #thesis3Stats = calculateThesis3Stats(allTweets_data,event_data)
    #generateThesisText(stats,clusterIdentifier,max(allTweets_data.values()),thesis3Stats)

    ax1.set_xlabel('Oktober 2015')
    ax1.set_ylabel('Anzahl an Tweets', color='tab:blue')
    ax1.legend()
    #ax1.set_ylabel('Number of Tweets', color='tab:blue')
    ticks = range(12,31)
    tickla = [f'{tick}.' for tick in ticks]
    ax1.xaxis.set_ticks(ticks)
    ax1.xaxis.set_ticklabels(tickla)
    ax1.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, p: f"{int(x):,}".replace(","," ")))
    #plt.suptitle(f"{clusterName} ({clusterIdentifier})")
    #plt.title("Protestaktivität in Südafrika")
    plt.tight_layout()

    #plt.savefig(f"analysis/anas/{clusterIdentifier}_{clusterName.replace(' ','_')}.pdf")
    plt.show()
    print(f"Saved {clusterIdentifier}_{clusterName.replace(' ','_')}.pdf")

def visualizeUserGrowthOverTime(data,derivation,offline_data,clusterId,clusterName):
    event_data = getProtestDataForVisualization(offline_data)
    fig, ax1 = plt.subplots()
    fig.set_figheight(5)
    fig.set_figwidth(12)
    dayOfFirstProtest = 0
    for event in event_data.keys():
        if event_data[event] > 0 and dayOfFirstProtest == 0:
            dayOfFirstProtest = event
        plt.axvspan(event,event+1,color='g',alpha=event_data[event]/8)
    #ax1.plot(data.keys(), data.values(),'b-',label='Anzahl der beteiligten Nutzenden')
    ax1.plot(derivation.keys(), derivation.values(),'r-',label='Wachstumsfaktor zum Vortag')
    ax1.legend()
    ticks = range(12,31)
    ax1.xaxis.set_ticks(ticks)
    ax1.xaxis.set_ticklabels(ticks)

    plt.title(f"{clusterName} ({clusterId})")
    plt.savefig(f"analysis/anas/ana9/{clusterId}_{clusterName.replace(' ','_')}.pdf")
    #plt.show()

def visualizeOnlineOfflineOverTime(save=False,offline_data=None,online_data=None,user_data=None,geo_data=None,allTweets=None,clusterName="",clusterIdentifier="All",cluster_technique="",offline_data_type="",stats=None):
    event_data = getProtestDataForVisualization(offline_data)
    tweet_data = putTweetsInHalfDayContainers(online_data)
    allTweets_data = putTweetsInHalfDayContainers(allTweets)

    assertion_tweets,metavoicing_tweets = splitTweetsByType(online_data)

    # with open("log/analysis/factsheet_errors.log","a") as f:
    #     result = all_count - impact_count
    #     if result < 0:
    #         f.write(f"Error: {clusterName} ({clusterIdentifier}) has {-result} more impact tweets ({impact_count}) than all tweets ({all_count})\n")

    fig, ax1 = plt.subplots()
    fig.set_figheight(5)
    fig.set_figwidth(12)
 
    for event in event_data.keys():
        plt.axvspan(event,event+1,color='g',alpha=event_data[event]/8)

    ax1.plot(tweet_data.keys(), tweet_data.values(),'b-.',label='Über Hashtag lokalisiert')
    #ax1.plot(allTweets_data.keys(), allTweets_data.values(),'b-',label='Alle Tweets')
    ax1.fill_between(allTweets_data.keys(),allTweets_data.values(),alpha=0.5)
    if user_data != None:
        tweet_data_2 = putTweetsInHalfDayContainers(user_data)
        ax1.plot(tweet_data_2.keys(), tweet_data_2.values(),'b--',label='Über User lokalisiert')
    if geo_data != None:
        tweet_data_3 = putTweetsInHalfDayContainers(geo_data)
        ax1.plot(tweet_data_3.keys(), tweet_data_3.values(),'b:',label='Über Geolocation lokalisiert')

    #ax2.set_ylabel('Anzahl Proteste pro Tag', color='g')
    if stats is not None:
        generateStatsText(stats,clusterIdentifier,max(allTweets_data.values()))

    ax1.set_xlabel('Days in October 2015')
    ax1.set_ylabel('Number of Tweets per Day', color='b')
    ticks = range(12,31)
    #tickla = [f'{tick}.' for tick in ticks]
    ax1.xaxis.set_ticks(ticks)
    ax1.xaxis.set_ticklabels(ticks)
    ax1.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, p: f"{int(x):,}".replace(","," ")))
    plt.suptitle(f"{clusterName} ({clusterIdentifier})")
    #ax1.xaxis.grid(True)
    plt.tight_layout()
    if save:
        if cluster_technique == "uniCircles":
            path = "analysis/anas/uniCircles/factsheets"
        else:
            path = "analysis/anas/afMvmNewCorrectUserCast/factsheets"
        plt.savefig(f"{path}/{clusterIdentifier}_{clusterName.replace(' ','_')}.pdf")
    #plt.show()
    print(f"Visualized {clusterIdentifier}_{clusterName.replace(' ','_')}.pdf")
    

def visualizeOriginalRetweets():
    tweets = md.fetchAllTweets()
    data = {}
    number_of_org_tweets = 0
    number_of_retweets = 0
    for day in range(12,31):
        for tweet in tweets:
            if tweet[2][:10] == f"2015-10-{day}":
                if tweet[4] == "original":
                    number_of_org_tweets += 1
                else:
                    number_of_retweets += 1 #also includes quotes and replies
        data.update({f"{day}.":[number_of_org_tweets,number_of_retweets]})
    plt.plot(data.keys(),[x[0] for x in data.values()],label="Originale Tweets")
    plt.plot(data.keys(),[x[1] for x in data.values()],label="Retweets, Replies und Quotes")
    plt.legend()
    plt.xlabel("Oktober 2015")
    plt.ylabel("Anzahl neuer Beiträge")
    plt.title("Entwicklung der Tweets nach Typ")
    plt.savefig(f"preprocessing/visualize/general_analysis/culminated_original_retweets.pdf")
    plt.show()

def visualizetop10Users():
    users = md.getUsersWithTweetAmount()
    con = md.setupConnection()
    fig = plt.figure()
    #sort users after tweet amount
    users = sorted(users, key=lambda x: x[1], reverse=True)
    plt.bar([md.getUserById(con,x[0])[1] for x in users[:10]],[x[1] for x in users[:10]])
    plt.xlabel("Nutzer")
    plt.ylabel("Anzahl an Tweets")
    plt.title("Top 10 Nutzer")
    fig.autofmt_xdate()
    plt.savefig(f"preprocessing/visualize/general_analysis/top10_users.pdf")
    plt.show()

#nicht mehr verwendet
def checkMaxKeyViolations(tweet_data_QuarterDayContainer,tweet_data_DayContainer,id):
    max_day_QuarterDay = max(tweet_data_QuarterDayContainer, key=tweet_data_QuarterDayContainer.get)
    max_day_Day = max(tweet_data_DayContainer, key=tweet_data_DayContainer.get)
    if max_day_Day != math.floor(max_day_QuarterDay):
        print(f"{id} violates max key: Max day with high resoluation: {max_day_QuarterDay}, Max day with low resolution: {max_day_Day}")
    return max_day_Day,max_day_QuarterDay

def calculateNewOrdering(factor=0.25):
    ids = [str(i) for i in range(27) if i != 17]
    startPoints = {}
    for id in ids:
        print(id)
        tweets = an.getTweetsOfCluster(id)
        tweetsInHalfDayContainers = putTweetsInHalfDayContainers(tweets)
        print(tweetsInHalfDayContainers)

        #get container with most tweets
        max_halfday = max(tweetsInHalfDayContainers, key=tweetsInHalfDayContainers.get)
        print(f"Max halfday: {max_halfday}")
        #get timepoint with first 25% of most tweets
        for halfday in tweetsInHalfDayContainers.keys():
            if tweetsInHalfDayContainers[halfday] >= tweetsInHalfDayContainers[max_halfday]*factor:
                startPoints.update({id:halfday})
                break
        print(f"Startpoint: {startPoints[id]}")    
    return dict(sorted(startPoints.items(),key=lambda x:x[1]))

def calculateOrdering(factor=0.5):
    ids = [str(i) for i in range(27) if i != 17]
    halfPoints = {}
    for id in ids:
        tweets = an.getTweetsOfCluster(id)
        tweetsinHourContainers = putTweetsInHourContainers(tweets)
        tweetsInHalfDayContainers = putTweetsInHalfDayContainers(tweets)

        numberOfAllTweets = 0
        currentNumberOfTweets = 0
        for hour in tweetsinHourContainers.keys():
            numberOfAllTweets += tweetsinHourContainers[hour]

        for hour in tweetsinHourContainers.keys():
            if currentNumberOfTweets < numberOfAllTweets*factor:
                currentNumberOfTweets += tweetsinHourContainers[hour]
            else:
                halfPoints.update({id:hour})
                break
    return dict(sorted(halfPoints.items(),key=lambda x:x[1]))
        
def createStackedTweetsOverTimePlots(factor=0.25):
    ids = [str(i) for i in range(27) if i != 17]
    ids_orderedByStart = [21,15,13,2,0,26,20,4,14,23,6,3,7,16,11,9,24,10,18,19,5,12,8,1,25,22]
    ids_orderedByMax = ['0', '15', '3', '12', '18', '19', '5', '16', '7', '13', '9', '14', '2', '21', '20', '11', '25', '24', '1', '10', '8', '4', '6', '23', '22', '26']
    ids_orderedBy50 = ['0', '15', '13', '12', '2', '21', '7', '16', '3', '14', '5', '18', '9', '25', '4', '11', '20', '26', '24', '6', '10', '8', '1', '23', '22', '19']
    ids_orderedBy33 = ['0', '21', '15', '3', '7', '13', '2', '12', '18', '14', '26', '4', '11', '16', '9', '6', '20', '5', '24', '10', '25', '8', '22', '23', '19', '1']
    ids_orderedBy10 = ['21', '13', '15', '2', '25', '0', '20', '4', '26', '14', '3', '22', '9', '7', '11', '23', '16', '24', '10', '18', '5', '6', '19', '1', '12', '8']
    orderedValuesBy10 = {'21': 14.708333333333334, '13': 19.333333333333332, '15': 19.333333333333332, '2': 19.708333333333332, '25': 19.75, '0': 19.791666666666668, '20': 20.0, '4': 20.333333333333332, '26': 20.416666666666668, '14': 20.458333333333332, '3': 20.5, '22': 20.583333333333332, '9': 20.708333333333332, '7': 20.791666666666668, '11': 20.791666666666668, '23': 20.791666666666668, '16': 20.833333333333332, '24': 20.833333333333332, '10': 21.0, '18': 21.291666666666668, '5': 21.333333333333332, '6': 21.333333333333332, '19': 21.333333333333332, '1': 21.375, '12': 21.375, '8': 21.416666666666668}
    #ids_orderedAfterCastBy10 = ['21', '24', '15', '7', '13', '2', '25', '0', '4', '5', '20', '3', '14', '23', '26', '9', '10', '12', '22', '18', '8', '11', '16', '19', '6', '1']
    #orderedValuesAfterCastBy10 = {'21': 14.875, '24': 19.25, '15': 19.375, '7': 19.416666666666668, '13': 19.541666666666668, '2': 19.75, '25': 19.791666666666668, '0': 19.875, '4': 20.333333333333332, '5': 20.333333333333332, '20': 20.333333333333332, '3': 20.5, '14': 20.5, '23': 20.5, '26': 20.5, '9': 20.541666666666668, '10': 20.541666666666668, '12': 20.583333333333332, '22': 20.583333333333332, '18': 20.666666666666668, '8': 20.708333333333332, '11': 20.708333333333332, '16': 20.833333333333332, '19': 21.25, '6': 21.291666666666668, '1': 21.333333333333332}
    orderedValues = calculateNewOrdering(factor)
    ids = orderedValues.keys()
    
    #ids = ids_orderedAfterCastBy10
    #orderedValues = orderedValuesAfterCastBy10
    #ids = ids_orderedBy50
    ids = [str(i) for i in ids]
    fig, axs = plt.subplots(len(ids), sharex=True)
    #fig.suptitle('Tweet Amount over Time')
    fig.set_figheight(3)
    fig.set_figwidth(12)
    ticks = [i for i in range(12,31)]

    for idx,id in enumerate(ids):
        print("Starting with",id)
        print("Reading tweets ...")
        tweets = an.getTweetsOfCluster(id)
        tweet_data_DayContainer = putTweetsInHalfDayContainers(tweets)
        tweet_data_QuarterDayContainer = putTweetsInQuarterDayContainers(tweets)
        tweet_data_HourContainer = putTweetsInHourContainers(tweets)
        print("All tweets read")
        event_data = getProtestDataForVisualization(an.getEventsOfCluster(id))
        print("All events read")
        axs[idx].set_title(f" {ids[idx-1]}",loc = "left")
        for event in event_data.keys():
            if event_data[event] > 0:
                axs[idx].axvspan(event,event+1,color='tab:green',alpha=event_data[event]/3)
        axs[idx].axvline(x=orderedValues[id],color='tab:red')
        axs[idx].fill_between(tweet_data_DayContainer.keys(),tweet_data_DayContainer.values(),color="tab:blue",alpha=0.8)
        axs[idx].set_yticks([])
        axs[idx].set_yticklabels([])
        tickla = [f'{tick}.' for tick in ticks]
        axs[idx].set_xticks(ticks)
        axs[idx].set_xticklabels(tickla)
        if id == "8":
            axs[idx].set_xlabel("Oktober 2015")
    plt.savefig("analysis/anas/afMvmNewCorrectUserCast/newSorting.pdf")
    print(ids)
    print(orderedValues)
    plt.show()


def visualizeTweetsMoreDetailed(clusterId,startDay,endDay,hours=False):
    if hours:
        tweets = putTweetsInHourContainers(an.getTweetsOfCluster(clusterId),startDay,endDay)
    else:
        tweets = putTweetsInQuarterDayContainers(an.getTweetsOfCluster(clusterId),startDay,endDay)
    allTweetCount = 0
    for container in tweets.keys():
        allTweetCount += tweets[container]
    for container in tweets.keys():
        print(f"{container}: {tweets[container]} ({round(tweets[container]/allTweetCount*100,2)}%)")
    event_data = getProtestDataForVisualization(an.getEventsOfCluster(clusterId),startDay,endDay)
    fig, ax1 = plt.subplots()
    ax1.fill_between(tweets.keys(),tweets.values(),alpha=0.9)
    for event in event_data.keys():
        if event_data[event] > 0:
            ax1.axvspan(float(event),float(event+1),color='g',alpha=event_data[event]/8)
    #print(tweets)

    ticks = range(startDay,endDay+1)
    #tickla = [f'{tick}.' for tick in ticks]
    ax1.xaxis.set_ticks(ticks)
    tickla = [f'{tick}.' for tick in ticks]
    ax1.xaxis.set_ticklabels(tickla)
    plt.show()

    
    

if __name__ == '__main__':
    #visualizeAsHeatmap()
    createStackedTweetsOverTimePlots(0.25)
    #visualizeTweetsMoreDetailed(4,18,25)
    #orderMaxDays({'25': [22, 22], '0': [20, 20], '8': [22, 23]})
    #print(calculateOrdering())
    #print(calculateNewOrdering())