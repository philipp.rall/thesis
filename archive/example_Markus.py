import tweepy
import json

client = tweepy.Client(bearer_token='AAAAAAAAAAAAAAAAAAAAADWMMwEAAAAAr1xcrbJE9XBLmld1kScxkMhEAY0%3Dek8AbQN3844XGHppL259raKbjTrV3u8nV1VJ2Zbvpmt6q6DsHI', wait_on_rate_limit=True)

query = '#FeesMustFall'

start_time = '2015-10-12T00:00:00Z'
end_time = '2015-10-30T00:00:00Z'

tweet_fields = ['id', 'text', 'attachments', 'in_reply_to_user_id', 'public_metrics', 'created_at']
#user_fields = ['id', 'name', 'username', 'public_metrics', 'url']
#media_fields = ['type', 'url', 'alt_text', 'public_metrics']

tweet_list = []
i = 0
for tweet in tweepy.Paginator(client.search_all_tweets, query=query, start_time=start_time, end_time=end_time, 
            tweet_fields=tweet_fields,
            #user_fields=user_fields,
            #media_fields=media_fields,
            max_results=10).flatten(): # ADJUST max_results HERE

    tweet_dict = {}
    
    for field in tweet_fields:
        if field in tweet:
            tweet_dict[field] = str(tweet[field])

    tweet_list.append(tweet_dict)

    print(tweet_list)

    if i % 1000 == 0:
        print(i)

    i += 1

with open("raw_tweets_test.json", "w", encoding="UTF-8") as file:
    tweet_json = json.dump(tweet_list, file)

