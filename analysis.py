import numpy as np
import spatial_calculations as sc
import manage_database as md
import matplotlib.pyplot as plt
import visualize as vis
import shapely.geometry as sg
import shapely as sh
import os
from get_data import log
import json

types = ['offline_reduced','offline','offline_checked','online_users','online_impact','online_geo']
geo_clusters_types = ['universities']#['uniCircles']

class Geo_Clusters:
    def __init__(self,spatialFolderName):
        self.spatialFolderName = spatialFolderName
        self.regions,self.regionIndices = sc.getRegions(spatialFolderName)
        self.geo_clusters = self.createGeoClusters()
        #sc.drawSouthAfricanMap(withAnnotations=False,folder=self.spatialFolderName,withRegionAnnotations=True,
        #                           savePath=f"analysis/spatial_analysis/{self.spatialFolderName}_clusterLookup.pdf")

    def createGeoClusters(self):
        geo_clusters = []
        for index, region in enumerate(self.regions):
            geo_clusters.append(Geo_Cluster(region,self.regionIndices[index],self.getClusterName(self.regionIndices[index]),self.spatialFolderName))
        return geo_clusters
    
    def getClusterName(self,identifier):
        with open(f"analysis/spatial/{self.spatialFolderName}/nameMapping.csv","r") as f:
            lines = f.readlines()
            for line in lines:
                if line.split(",")[0] == str(identifier):
                    return line.split(",")[1].replace("\n","")
        
class Geo_Cluster:
    def __init__(self,geometry,identifier,name,folder):
        self.geometry = geometry
        self.identifier = identifier
        self.name = name
        self.folder = folder
    

class Data:
    def __init__(self,type,onlyUniversities=False):
        self.identifier = type
        if type == 'offline_reduced':
            self.data = md.fetchAllEventLocations(withDate=True,reduced=True,onlyChecked=False)
        elif type == 'offline':
            self.data = md.fetchAllEventLocations(withDate=True,reduced=False,onlyChecked=False)
        elif type == 'offline_checked':
            self.data = md.fetchAllEventLocations(withDate=True,reduced=False,onlyChecked=True)
        elif type == 'online_users':
            self.data =  md.fetchAllTweetUserLocations(inSouthAfrica=True,onlyUniversityLocations=onlyUniversities,withOutMovedUsers=False,removeWrongSouthAfricaLocation=True)
        elif type == 'online_impact':
            self.data = md.fetchAllTweetImpactLocations(inSouthAfrica=True,onlyUniversityLocations=onlyUniversities)
        elif type == 'online_geo':
            self.data = md.fetchAllTweetGeoLocations(inSouthAfrica=True,onlyUniversityLocations=onlyUniversities)

def getDataPointsInCluster(geo_cluster,data):
    if geo_cluster.folder == "uniCircles":
        folder = f"analysis/withinChecksUniCircles/{geo_cluster.identifier}"
    else:
        folder = f"analysis/withinChecksAfMvmNewCorrectUserCast/universities/{geo_cluster.identifier}"
    path = folder + f"/{data.identifier}"
    pointsWithinCluster = {}
    if os.path.exists(path):
        with open(path,"r") as f:
            returnValue = []
            print(f"Reading from {path}")
            lines = f.readlines()
            for line in lines:
                values = line.split("\t")
                parsed_values = []
                parsed_values.append(float(values[0]))
                parsed_values.append(float(values[1]))
                date = values[2]
                if date.__contains__("-"):
                    date = date.split("-")
                    date = date[0] + date[1] + date[2].split("T")[0]
                    parsed_values.append(int(date))
                    parsed_values.append(values[2])
                else:
                    parsed_values.append(int(values[2]))
                if len(values) > 3:
                    parsed_values = parsed_values + values[3:]
                    parsed_values[-1] = parsed_values[-1].replace("\n","")
                returnValue.append(parsed_values)
        return returnValue
    else:
        print(f"Writing to {path}")
        progress_already_printed = []
        for index,data_point in enumerate(data.data):
            progress = int((index/len(data.data))*100+0.5)
            if progress % 10 == 0 and progress not in progress_already_printed:
                print(f"{progress}% done")
                progress_already_printed.append(progress)             
            point = sg.Point(data_point[1],data_point[0])
            tweet_id = data_point[3]
            if geo_cluster.geometry.contains(point) and data_point[3] not in pointsWithinCluster.keys():
                pointsWithinCluster.update({data_point[3]:data_point})
        os.makedirs(folder,exist_ok=True)
        with open(path,"w") as f:
            f.write("\n".join(["\t".join([str(x) for x in point]) for point in pointsWithinCluster.values()]))
        return getDataPointsInCluster(geo_cluster,data)

def parseToIntIfNotStringNone(value):
    if value == "none":
        return None
    else:
        return int(value)

def analyse(geo_cluster_split,logfile,thesisSheets = False):
    spatialFolderName = geo_cluster_split.spatialFolderName
    if spatialFolderName == "uniCircles":
        file = f"analysis/anas/uniCircles/uni_metadata.csv"
    else: 
        file = f"analysis/anas/afMvmNewCorrectUserCast/uni_metadata.csv"
    offline_data = "offline_checked"
    stats_dicts = {
        "numberOfTweets":{},
        "numberOfProtests":{},
        "daysOfProtest":{},
        "numberOfUsers":{},
        "fractionOfUsersSupportingFromAbroad":{},
        "fractionUniqueUserTweets":{},
        "fractionUniqueGeoTweets":{},
        "originalTweetRatio":{},
        "averageTweetsPerUser":{},
        "usersPerStudent":{}
        }
    for cluster in geo_cluster_split.geo_clusters:
        stats_dict = {}
        alreadyCalculated = False

        if cluster.identifier == '17':
            continue
        log(f"Analysing {cluster.identifier}",logfile)
        if os.path.exists(file):
            with open(file,"r") as f:
                lines = f.readlines()
                for line in lines[1:]:
                    if line.split(",")[0] == str(cluster.identifier):
                        alreadyCalculated = True
                        firstDayofProtest = parseToIntIfNotStringNone(line.split(",")[3])
                        lastDayofProtest = parseToIntIfNotStringNone(line.split(",")[4])
                        stats_dict = {
                            'numberOfAllTweets':int(line.split(",")[6]),
                            'numberOfAllUsers':int(line.split(",")[8]),
                            'numberOfProtests':int(line.split(",")[2]),
                            'fractionUniqueUserTweets':float(line.split(",")[21]),
                            'fractionUniqueGeoTweets':float(line.split(",")[22]),
                            'averageTweetsPerAllUser':float(line.split(",")[18]),
                            'originalTweetRatioAll':float(line.split(",")[16]),
                            'fractionOfUsersSupportingFromAbroad':float(line.split(",")[20]),
                            'usersPerStudent':float(line.split(",")[26])
                        }
                        if firstDayofProtest is not None and lastDayofProtest is not None:
                            stats_dict.update({'daysOfProtest':[lastDayofProtest-firstDayofProtest+1,firstDayofProtest,lastDayofProtest]})
                        else:
                            stats_dict.update({'daysOfProtest':[0,None,None]})
                        break
        if not alreadyCalculated:
            impact_tweets = getDataPointsInCluster(cluster,Data("online_impact"))
            geo_tweets = getDataPointsInCluster(cluster,Data("online_geo"))
            user_tweets = getDataPointsInCluster(cluster,Data("online_users"))
            protests = np.array(getDataPointsInCluster(cluster,Data("offline_checked")))

            uniNumberOfStudentsMapping = {}
            with open("preprocessing/uni_keys.csv","r") as f1:
                lines = f1.readlines()
                for line in lines[:26]:
                    uni_name = line.split(",")[3].lower().strip()
                    number_of_students = line.split(",")[6].strip()
                    uniNumberOfStudentsMapping.update({uni_name:number_of_students})

            if len(protests) == 0:
                dateFirstProtest = None
                dateLastProtest = None
                daysOfProtests = 0
            else:
                dates = [int(x) for x in protests[:,2]]
                dateFirstProtest = np.min(dates)
                dateLastProtest = np.max(dates)
                daysOfProtests = dateLastProtest-dateFirstProtest + 1
            stats_dict,_,_ = calculateTweetsStats(impact_tweets,geoTweets=geo_tweets,userTweets=user_tweets)
            stats_dict.update({"numberOfProtests":len(protests)})
            stats_dict.update({"daysOfProtest":[daysOfProtests,dateFirstProtest,dateLastProtest]})
            stats_dict.update({"usersPerStudent":stats_dict['numberOfAllUsers']/int(uniNumberOfStudentsMapping[cluster.name.lower()])})

        stats_dicts['numberOfTweets'].update({cluster.identifier:stats_dict['numberOfAllTweets']})
        stats_dicts['numberOfUsers'].update({cluster.identifier:stats_dict['numberOfAllUsers']})
        stats_dicts['numberOfProtests'].update({cluster.identifier:stats_dict['numberOfProtests']})
        if stats_dict['daysOfProtest'] is not None:
            stats_dicts['daysOfProtest'].update({cluster.identifier:stats_dict['daysOfProtest']})
        stats_dicts['fractionUniqueUserTweets'].update({cluster.identifier:stats_dict['fractionUniqueUserTweets']})
        stats_dicts['fractionUniqueGeoTweets'].update({cluster.identifier:stats_dict['fractionUniqueGeoTweets']})
        stats_dicts['averageTweetsPerUser'].update({cluster.identifier:stats_dict['averageTweetsPerAllUser']})
        stats_dicts['originalTweetRatio'].update({cluster.identifier:1-stats_dict['originalTweetRatioAll']})
        stats_dicts['fractionOfUsersSupportingFromAbroad'].update({cluster.identifier:stats_dict['fractionOfUsersSupportingFromAbroad']})
        stats_dicts['usersPerStudent'].update({cluster.identifier:stats_dict['usersPerStudent']})

    log(f"Finished analysing clusters",logfile)
    log("----------------------------------------",logfile)
    for cluster in geo_cluster_split.geo_clusters:
        goThrough = True
        if spatialFolderName != "uniCircles" and cluster.identifier == '17':
            goThrough = False
        if goThrough:
            log("----------------------------------------",logfile)
            log(f"Visualizing {cluster.identifier}",logfile)
            impact_tweets = getDataPointsInCluster(cluster,Data("online_impact"))
            geo_tweets = getDataPointsInCluster(cluster,Data("online_geo"))
            user_tweets = getDataPointsInCluster(cluster,Data("online_users"))
            _,uniqueAllTweets,_ = calculateTweetsStats(impact_tweets,geoTweets=geo_tweets,userTweets=user_tweets)
            if thesisSheets:
                vis.generateThesisSheets(save=False,
                                                offline_data = getDataPointsInCluster(cluster,Data(offline_data)),
                                                online_data = impact_tweets,
                                                user_data = user_tweets,
                                                geo_data = geo_tweets,
                                                allTweets = uniqueAllTweets,
                                                clusterName = cluster.name,
                                                clusterIdentifier = cluster.identifier,
                                                cluster_technique = geo_cluster_split.spatialFolderName,
                                                offline_data_type = offline_data,
                                                stats = stats_dicts)
            else:
                vis.visualizeOnlineOfflineOverTime(save=True,
                                                offline_data = getDataPointsInCluster(cluster,Data(offline_data)),
                                                online_data = impact_tweets,
                                                user_data = user_tweets,
                                                geo_data = geo_tweets,
                                                allTweets = uniqueAllTweets,
                                                clusterName = cluster.name,
                                                clusterIdentifier = cluster.identifier,
                                                cluster_technique = geo_cluster_split.spatialFolderName,
                                                offline_data_type = offline_data,
                                                stats = stats_dicts)

def runAnalysis(logfile,thesisSheets=False,clusterType="uniCircles"):
    geo_cluster_split = Geo_Clusters(clusterType)
    log(f"Analysing {clusterType}",logfile)
    analyse(geo_cluster_split,logfile,thesisSheets)
    log(f"Finished {clusterType}",logfile)
    log("----------------------------------------",logfile)

def preprocessTweetDataforSouthAfrica(tweets):
    preprocessed_tweets = []
    for tweet in tweets:
        parsed_values = []
        parsed_values.append(float(tweet[0]))
        parsed_values.append(float(tweet[1]))
        date = tweet[2]
        if date.__contains__("-"):
            date = date.split("-")
            date = date[0] + date[1] + date[2].split("T")[0]
            parsed_values.append(int(date))
            parsed_values.append(tweet[2])
        for value in tweet[3:]:
            parsed_values.append(value)
        preprocessed_tweets.append(parsed_values)
    return preprocessed_tweets

def analyseSouthAfrica():
    offline_data = "offline_checked"
    online_data = "online_impact"
    online_data2 = "online_users"
    online_data3 = "online_geo"
    with open("preprocessing/visualize/southAfrica.json","r") as f:
        lines = f.read()
        southAfrica = Geo_Cluster(sh.from_geojson(lines),"42","SouthAfrica","southAfrica")
        impact_tweets = preprocessTweetDataforSouthAfrica(Data(online_data).data)
        geo_tweets = preprocessTweetDataforSouthAfrica(Data(online_data3).data)
        user_tweets = preprocessTweetDataforSouthAfrica(Data(online_data2).data)
        _,uniqueAllTweets,_ = calculateTweetsStats(impact_tweets,geoTweets=geo_tweets,userTweets=user_tweets)
        vis.generateSouthAfricaSheet(save=True,
                                offline_data=getDataPointsInCluster(southAfrica,Data(offline_data)),
                                online_data=impact_tweets, #checked via database
                                user_data=user_tweets, #checked via database
                                geo_data=geo_tweets, #checked via database
                                clusterIdentifier=southAfrica.identifier,
                                clusterName = southAfrica.name,
                                cluster_technique=southAfrica.folder,
                                offline_data_type=offline_data,
                                allTweets=uniqueAllTweets)

def fillAnalysisTable(logfile, forCorrelation = False, clusterType = "uniCircles"):
    if clusterType != "uniCircles":
        metadata_file = f"analysis/anas/afMvmNewCorrectUserCast/uni_metadata{'_regression' if forCorrelation else ''}.csv"
    else:
        metadata_file = f"analysis/anas/uniCircles/uni_metadata{'_regression' if forCorrelation else ''}.csv"
        
    with open(metadata_file,"a") as f:
        #f.write("""clusterIdentifier,clusterName,numberOfProtests,dateFirstProtest,dateLastProtest,numberOfGeoImpactTweets,numberOfAllTweets,
        #             numberOfUsersGeoImpactTweets,numberOfAllUsers,dateFirstTweetGeoImpact,dateFirstTweetAll,dateLastTweetGeoImpact,dateLastTweetAll,dateMaxTweetsGeoImpact,
        #             dateMaxTweetsAll,originalTweetRatioGeoImpact,originalTweetRatioAll,averageTweetsPerGeoImpactUser,averageTweetsPerAllUser,fractionUniqueUserTweets,
        #             fractionUniqueGeoTweets,fractionUniqueUserUsers,fractionUniqueGeoUsers\n""")
        university_clusters = Geo_Clusters(clusterType)
        offline_data = "offline_checked"

        uniNumberOfStudentsMapping = {}
        with open("preprocessing/uni_keys.csv","r") as f1:
            lines = f1.readlines()
            for line in lines[:26]:
                uni_name = line.split(",")[3].lower().strip()
                number_of_students = line.split(",")[6].strip()
                uniNumberOfStudentsMapping.update({uni_name:number_of_students})
        startOfOnlineProtest = {'21': 14.25, '2': 19.25, '13': 19.25, '15': 19.25, '24': 19.75, '0': 20.25, '3': 20.75, '5': 20.75, '8': 20.75, '9': 20.75, '10': 20.75, '11': 20.75, '16': 20.75, '4': 21.25, '12': 21.25, '14': 21.25, '18': 21.25, '19': 21.25, '20': 21.25, '23': 21.25, '25': 21.25, '26': 21.25, '1': 21.75, '6': 21.75, '7': 21.75, '22': 21.75}
        for cluster in university_clusters.geo_clusters:
            already_in_table = False
            with open(metadata_file,"r") as f2:
                current_lines = f2.readlines()
                if len(current_lines) > 1:
                    for line in current_lines[1:]:
                        if line.split(",")[0] == str(cluster.identifier):            
                            already_in_table = True
            if cluster.identifier in ["17"] or already_in_table:
                log(f"Skipping {cluster.name} ({cluster.identifier}) because it is already in the table",logfile)
                continue
            log(f"Analysing {cluster.name} ({cluster.identifier})",logfile)
            impact_tweets = getDataPointsInCluster(cluster,Data("online_impact",onlyUniversities=False))
            geo_tweets = getDataPointsInCluster(cluster,Data("online_geo",onlyUniversities=False))
            user_tweets = getDataPointsInCluster(cluster,Data("online_users",onlyUniversities=False))
            log(f"All data collected",logfile)
            stats_dict,uniqueAllTweets,uniqueAllUsers = calculateTweetsStats(impact_tweets,geoTweets=geo_tweets,userTweets=user_tweets)
            log(f"Stats calculated",logfile)
            protests = np.array(getDataPointsInCluster(cluster,Data(offline_data)))
            numberOfStudents = int(uniNumberOfStudentsMapping[cluster.name.lower()])
            if len(protests) == 0:
                dateFirstProtest = "none"
                dateLastProtest = "none"
                studentsProtestDay,studentsDayBefore = 0,0
                tweetsProtestDay,tweetsDayBefore = 0,0
                userGrowth1DayBefore = "none"
                userGrowth2DaysBefore = "none"
                userGrowth3DaysBefore = "none"
            else:
                dates = [int(x) for x in protests[:,2]]
                dateFirstProtest = np.min(dates)
                dateLastProtest = np.max(dates)
                studentsProtestDay,studentsDayBefore = calculateInvolvedUsersForClusterAtProtestDay(uniqueAllUsers,dateFirstProtest)
                tweetsProtestDay,tweetsDayBefore = calculateTweetsForClusterAtProtestDay(uniqueAllTweets,dateFirstProtest)

                day0025Percent = calculateDayWith1PercentStudents(uniqueAllUsers,numberOfStudents,0.005)
                retweetRatioFirstDays = calculateDayWithXPercentRetweets(uniqueAllTweets,day0025Percent,stats_dict['numberOfAllTweets']*0.01)
                log(f"Retweet ratio calculated",logfile)
                log(retweetRatioFirstDays,logfile)
                retweetEvaluation = {}
                for retweetRatio in retweetRatioFirstDays.keys():
                    day = retweetRatioFirstDays[retweetRatio]
                    #check day after or two days after
                    if dateFirstProtest - day in [20151001,20151002]:
                        retweetEvaluation.update({retweetRatio:1})
                    else:
                        retweetEvaluation.update({retweetRatio:0})
                # with open("analysis/anas/ana1/retweetEvaluation2.csv","a") as f3:
                #     new_line = f"{cluster.identifier}"
                #     for retweetRatio in retweetEvaluation.keys():
                #         new_line += f",{retweetEvaluation[retweetRatio]}"
                #     f3.write(f"{new_line}\n")
                userGrowth1DayBefore,userGrowth2DaysBefore,userGrowth3DaysBefore = calculateUserGraph(uniqueAllUsers,getDataPointsInCluster(cluster,Data(offline_data)),cluster,dateFirstProtest)
                

            day2Percent = calculateDayWith1PercentStudents(uniqueAllUsers,numberOfStudents,0.02)
            day1Percent = calculateDayWith1PercentStudents(uniqueAllUsers,numberOfStudents)
            day05Percent = calculateDayWith1PercentStudents(uniqueAllUsers,numberOfStudents,0.005)

            if forCorrelation:
                if dateFirstProtest != "none":
                    dateFirstProtest = dateFirstProtest - 20151000
                else:
                    dateFirstProtest = ''
                #headline = "protest,numberOfProtests,startOfOfflineProtest,numberOfTweets,numberOfUsers,startOfOnlineProtest,dateMaxTweets,originalTweetRatio,averageTweetsPerUser,supportFromOutside,supportingFromAbroad,numberOfStudents,usersPerStudent\n"
                #start of online protest missing: {startOfOnlineProtest[cluster.identifier]}
                f.write(f"{cluster.identifier},{cluster.name},{1 if len(protests) > 0 else 0},{len(protests)},{dateFirstProtest},{stats_dict['numberOfAllTweets']},{stats_dict['numberOfAllUsers']},0,{stats_dict['dateMaxTweetsAll']},{stats_dict['originalTweetRatioAll']},{stats_dict['fractionOfUsersSupportingFromOutside']},{stats_dict['fractionOfUsersSupportingFromAbroad']},{numberOfStudents}\n")
            else:
                #f.write(f"""{cluster.identifier},{cluster.name},{1 if len(protests) > 0 else 0},{len(protests)},{dateFirstProtest},{stats_dict['numberOfAllTweets']},{stats_dict['numberOfAllUsers']},{startOfOnlineProtest[cluster.identifier]},{stats_dict['dateMaxTweetsAll']},{stats_dict['originalTweetRatioAll']},{stats_dict['averageTweetsPerAllUser']},{stats_dict['fractionOfUsersSupportingFromOutside']},{stats_dict['fractionOfUsersSupportingFromAbroad']},{stats_dict['fractionOfTweetsWithUserLocationOutside']},{stats_dict['fractionOfTweetsWithUserLocationAbroad']},{uniNumberOfStudentsMapping[cluster.name.lower()]},{stats_dict['numberOfAllUsers']/numberOfStudents},{stats_dict['uniqueOriginalTweetUsers']/numberOfStudents}\n""")
                #start of online protest missing: {startOfOnlineProtest[cluster.identifier]}
                f.write(f"""{cluster.identifier},{cluster.name},{len(protests)},{dateFirstProtest},{dateLastProtest},{stats_dict['numberOfGeoImpactTweets']},{stats_dict['numberOfAllTweets']},{stats_dict['numberOfUsersGeoImpactTweets']},{stats_dict['numberOfAllUsers']},{stats_dict['dateFirstTweetGeoImpact']},0,{stats_dict['dateLastTweetGeoImpact']},{stats_dict['dateLastTweetAll']},{stats_dict['dateMaxTweetsGeoImpact']},{stats_dict['dateMaxTweetsAll']},{stats_dict['originalTweetRatioGeoImpact']},{stats_dict['originalTweetRatioAll']},{stats_dict['averageTweetsPerGeoImpactUser']},{stats_dict['averageTweetsPerAllUser']},{stats_dict['fractionOfUsersSupportingFromOutside']},{stats_dict['fractionOfUsersSupportingFromAbroad']},{stats_dict['fractionOfTweetsWithUserLocationOutside']},{stats_dict['fractionOfTweetsWithUserLocationAbroad']},{stats_dict['fractionUniqueUserTweets']},{stats_dict['fractionUniqueGeoTweets']},{stats_dict['fractionUniqueUserUsers']},{stats_dict['fractionUniqueGeoUsers']},{uniNumberOfStudentsMapping[cluster.name.lower()]},{stats_dict['numberOfAllUsers']/numberOfStudents},{stats_dict['uniqueOriginalTweetUsers']/numberOfStudents},{studentsDayBefore/numberOfStudents},{studentsProtestDay/numberOfStudents},{tweetsDayBefore/numberOfStudents},{tweetsProtestDay/numberOfStudents},{day05Percent},{day1Percent},{day2Percent},{userGrowth1DayBefore},{userGrowth2DaysBefore},{userGrowth3DaysBefore}\n""")
            log(f"Finished {cluster.name} ({cluster.identifier})",logfile)
            log("----------------------------------------",logfile)

def getDateOfMaxTweets(tweets):
    tweetsPerHour = vis.putTweetsInHourContainers(tweets)
    return max(tweetsPerHour,key=tweetsPerHour.get)

def reduceToTweetsWithinTimeframe(tweets,startDay=12,endDay=30):
    reducedTweets = {}
    for tweet in tweets.keys():
        date = tweets[tweet][2]
        if int(date) >= int(f"201510{startDay}") and int(date) <= int(f"201510{endDay}"):
            reducedTweets.update({tweet:tweets[tweet]})
    print(f"Reducing tweets from {len(tweets.keys())} to {len(reducedTweets.keys())}")
    return reducedTweets

def reduceToUsersWithinTimeframe(users,startDay=12,endDay=30):
    reducedUsers = {}
    for user in users.keys():
        datesOfUserInTimeframe = []
        for date in users[user]:
            if int(date) >= int(f"201510{startDay}") and int(date) <= int(f"201510{endDay}"):
                datesOfUserInTimeframe.append(date)
        if len(datesOfUserInTimeframe) > 0:
            reducedUsers.update({user:min(datesOfUserInTimeframe)})
    print(f"Reducing users from {len(users.keys())} to {len(reducedUsers.keys())}")
    return reducedUsers

def calculateTweetsStats(impactTweets,geoTweets=None,userTweets=None,startDay=12,endDay=30):
    uniqueAllTweets = {}
    uniqueGeoImpactTweets = {}
    geoTweetsUnique = {}
    userTweetsUnique = {}

    uniqueAllUsers = {}
    uniqueGeoImpactUsers = {}
    userUsersUnique = {}
    geoUsersUnique = {}
    uniqueUsersInCluster = {}

    originalTweetsAllTweets = {}
    originalTweetsGeoImpactTweets = {}
    uniqueOriginalAllTweetsUsers = {}

    usersWithoutLocationInside = {}
    tweetsWithUserLocationOutside = {}
    tweetsWithUserLocationAbroad = {}

    for tweet in impactTweets:
        tweet_id = tweet[4]
        user_id = tweet[5]
        tweet_type = tweet[6]
        date = tweet[2]
        uniqueAllTweets.update({tweet_id:tweet})
        uniqueGeoImpactTweets.update({tweet_id:tweet})
        if user_id not in uniqueAllUsers.keys():
            uniqueAllUsers.update({user_id:[date]})
            uniqueGeoImpactUsers.update({user_id:[date]})
        else:
            uniqueAllUsers[user_id].append(date)
            uniqueGeoImpactUsers[user_id].append(date)
        if tweet_type == "original":
            originalTweetsAllTweets.update({tweet_id:tweet})
            originalTweetsGeoImpactTweets.update({tweet_id:tweet})
            if user_id not in uniqueOriginalAllTweetsUsers.keys():
                uniqueOriginalAllTweetsUsers.update({user_id:[date]})
            
    if geoTweets is not None:
        for tweet in geoTweets:
            tweet_id = tweet[4]
            if tweet_id not in uniqueAllTweets.keys():
                geoTweetsUnique.update({tweet_id:tweet})
                uniqueAllTweets.update({tweet_id:tweet})
                uniqueGeoImpactTweets.update({tweet_id:tweet})

            user_id = tweet[5]
            date = tweet[2]
            if user_id in uniqueAllUsers.keys():
                uniqueAllUsers[user_id].append(date)
                uniqueGeoImpactUsers[user_id].append(date)
            else:
                geoUsersUnique.update({user_id:[date]})
                uniqueAllUsers.update({user_id:[date]})
                uniqueGeoImpactUsers.update({user_id:[date]})

            tweet_type = tweet[6]
            if tweet_type == "original":
                if tweet_id not in originalTweetsAllTweets.keys():
                    originalTweetsAllTweets.update({tweet_id:tweet})
                    originalTweetsGeoImpactTweets.update({tweet_id:tweet})
                if user_id not in uniqueOriginalAllTweetsUsers.keys():
                    uniqueOriginalAllTweetsUsers.update({user_id:[date]})

    if userTweets is not None:
        for tweet in userTweets:
            tweet_id = tweet[4]
            if tweet_id not in uniqueAllTweets.keys():
                userTweetsUnique.update({tweet_id:tweet})
                uniqueAllTweets.update({tweet_id:tweet})
            user_id = tweet[5]
            date = tweet[2]
            if user_id in uniqueAllUsers.keys():
                uniqueAllUsers[user_id].append(date)
            else:
                uniqueAllUsers.update({user_id:[date]})
                userUsersUnique.update({user_id:[date]})
            if user_id not in uniqueUsersInCluster.keys():
                uniqueUsersInCluster.update({user_id:[date]})
            else:
                uniqueUsersInCluster[user_id].append(date)
                
            tweet_type = tweet[6]
            if tweet_type == "original":
                if tweet_id not in originalTweetsAllTweets.keys():
                    originalTweetsAllTweets.update({tweet_id:tweet})
                if user_id not in uniqueOriginalAllTweetsUsers.keys():
                    uniqueOriginalAllTweetsUsers.update({user_id:[date]})

    for user in uniqueAllUsers.keys():
        if user not in uniqueUsersInCluster.keys():
            usersWithoutLocationInside.update({user:uniqueGeoImpactUsers[user]})

    usersWithoutLocationInside = reduceToUsersWithinTimeframe(usersWithoutLocationInside,startDay,endDay)
    uniqueAllUsers = reduceToUsersWithinTimeframe(uniqueAllUsers,startDay,endDay)
    uniqueGeoImpactUsers = reduceToUsersWithinTimeframe(uniqueGeoImpactUsers,startDay,endDay)
    userUsersUnique = reduceToUsersWithinTimeframe(userUsersUnique,startDay,endDay)
    geoUsersUnique = reduceToUsersWithinTimeframe(geoUsersUnique,startDay,endDay)
    uniqueOriginalAllTweetsUsers = reduceToUsersWithinTimeframe(uniqueOriginalAllTweetsUsers,startDay,endDay)

    con = md.setupConnection()
    usersSupportingFromOutside = []
    usersSupportingFromAbroad = []
    for user_id in usersWithoutLocationInside:
        result = md.getUserLocationById(con,user_id,abroad=True)
        if result is not None:
            user_location = result[0]
            user_abroad = result[1]
            if user_location is not None:
                usersSupportingFromOutside.append(user_id)
            if user_abroad == 1:
                usersSupportingFromAbroad.append(user)

    for tweet in uniqueAllTweets.values():
        user_id = tweet[5]
        if user_id not in uniqueUsersInCluster.keys():
            tweetsWithUserLocationOutside.update({tweet[4]:tweet})
        if user_id in usersSupportingFromAbroad:
            tweetsWithUserLocationAbroad.update({tweet[4]:tweet})

    uniqueAllTweets = reduceToTweetsWithinTimeframe(uniqueAllTweets,startDay,endDay)
    uniqueGeoImpactTweets = reduceToTweetsWithinTimeframe(uniqueGeoImpactTweets,startDay,endDay)
    userTweetsUnique = reduceToTweetsWithinTimeframe(userTweetsUnique,startDay,endDay)
    geoTweetsUnique = reduceToTweetsWithinTimeframe(geoTweetsUnique,startDay,endDay)
    originalTweetsAllTweets = reduceToTweetsWithinTimeframe(originalTweetsAllTweets,startDay,endDay)
    originalTweetsGeoImpactTweets = reduceToTweetsWithinTimeframe(originalTweetsGeoImpactTweets,startDay,endDay)
    tweetsWithUserLocationOutside = reduceToTweetsWithinTimeframe(tweetsWithUserLocationOutside,startDay,endDay)
    tweetsWithUserLocationAbroad = reduceToTweetsWithinTimeframe(tweetsWithUserLocationAbroad,startDay,endDay)
    
    numberOfGeoImpactTweets = len(uniqueGeoImpactTweets.keys())
    numberOfAllTweets = len(uniqueAllTweets.keys())
    fractionUniqueUserTweets = len(userTweetsUnique.keys())/numberOfAllTweets
    fractionUniqueGeoTweets = len(geoTweetsUnique.keys())/numberOfAllTweets
    numberOfAllUsers = len(uniqueAllUsers.keys())
    numberOfUsersGeoImpactTweets = len(uniqueGeoImpactUsers.keys())
    averageTweetsPerAllUser = numberOfAllTweets/numberOfAllUsers
    if numberOfUsersGeoImpactTweets != 0:
        averageTweetsPerGeoImpactUser = numberOfGeoImpactTweets/numberOfUsersGeoImpactTweets
    else:
        averageTweetsPerGeoImpactUser = 0
    fractionUniqueUserUsers = len(userUsersUnique.keys())/numberOfAllUsers
    fractionUniqueGeoUsers = len(geoUsersUnique.keys())/numberOfAllUsers
    originalTweetRatioAll = len(originalTweetsAllTweets)/numberOfAllTweets
    if numberOfUsersGeoImpactTweets != 0:
        originalTweetRatioGeoImpact = len(originalTweetsGeoImpactTweets)/numberOfGeoImpactTweets
    else:
        originalTweetRatioGeoImpact = 0
    uniqueOriginalTweetUsers = len(uniqueOriginalAllTweetsUsers.keys())

    dateFirstTweetAll = np.min([int(x[2]) for x in uniqueAllTweets.values()])
    dateLastTweetAll = np.max([int(x[2]) for x in uniqueAllTweets.values()])
    if uniqueGeoImpactTweets != {}:
        dateFirstTweetGeoImpact = np.min([int(x[2]) for x in uniqueGeoImpactTweets.values()])
        dateLastTweetGeoImpact = np.max([int(x[2]) for x in uniqueGeoImpactTweets.values()])
    else:
        dateFirstTweetGeoImpact = None
        dateLastTweetGeoImpact = None
    dateMaxTweetsAll = getDateOfMaxTweets(uniqueAllTweets.values())
    dateMaxTweetsGeoImpact = getDateOfMaxTweets(uniqueGeoImpactTweets.values())

    fractionOfUsersSupportingFromOutside = len(usersSupportingFromOutside)/numberOfAllUsers
    fractionOfUsersSupportingFromAbroad = len(usersSupportingFromAbroad)/numberOfAllUsers
    fractionOfTweetsWithUserLocationOutside = len(tweetsWithUserLocationOutside)/numberOfAllTweets
    fractionOfTweetsWithUserLocationAbroad = len(tweetsWithUserLocationAbroad)/numberOfAllTweets

    return_dict = {
        "numberOfGeoImpactTweets":numberOfGeoImpactTweets,
        "numberOfAllTweets":numberOfAllTweets,
        "numberOfUsersGeoImpactTweets":numberOfUsersGeoImpactTweets,
        "numberOfAllUsers":numberOfAllUsers,
        "fractionUniqueUserTweets":fractionUniqueUserTweets,
        "fractionUniqueGeoTweets":fractionUniqueGeoTweets,
        "averageTweetsPerAllUser":averageTweetsPerAllUser,
        "averageTweetsPerGeoImpactUser":averageTweetsPerGeoImpactUser,
        "fractionUniqueUserUsers":fractionUniqueUserUsers,
        "fractionUniqueGeoUsers":fractionUniqueGeoUsers,
        "originalTweetRatioAll":originalTweetRatioAll,
        "originalTweetRatioGeoImpact":originalTweetRatioGeoImpact,
        "dateFirstTweetAll":dateFirstTweetAll,
        "dateLastTweetAll":dateLastTweetAll,
        "dateFirstTweetGeoImpact":dateFirstTweetGeoImpact,
        "dateLastTweetGeoImpact":dateLastTweetGeoImpact,
        "dateMaxTweetsAll":dateMaxTweetsAll,
        "dateMaxTweetsGeoImpact":dateMaxTweetsGeoImpact,
        "fractionOfUsersSupportingFromOutside":fractionOfUsersSupportingFromOutside,
        "fractionOfUsersSupportingFromAbroad":fractionOfUsersSupportingFromAbroad,
        "fractionOfTweetsWithUserLocationOutside":fractionOfTweetsWithUserLocationOutside,
        "fractionOfTweetsWithUserLocationAbroad":fractionOfTweetsWithUserLocationAbroad,
        "uniqueOriginalTweetUsers":uniqueOriginalTweetUsers
    }
    return return_dict,uniqueAllTweets.values(),uniqueAllUsers
    
def generateTweetsOverTimeOverview():
    all_tweet_data = {}
    geo_cluster_split = Geo_Clusters("universities")
    print("Load Data ...")
    for cluster in geo_cluster_split.geo_clusters:
        if cluster.identifier == '17':
            continue
        impact_tweets = getDataPointsInCluster(cluster,Data("online_impact"))
        geo_tweets = getDataPointsInCluster(cluster,Data("online_geo"))
        user_tweets = getDataPointsInCluster(cluster,Data("online_users"))
        _,uniqueAllTweets,_ = calculateTweetsStats(impact_tweets,geoTweets=geo_tweets,userTweets=user_tweets)
        all_tweet_data.update({cluster.identifier:uniqueAllTweets})
    print("Data succesfully loaded")
    vis.createStackedTweetsOverTimePlots(all_tweet_data)

def getTweetsOfCluster(clusterId):
    geo_cluster_split = Geo_Clusters("universities")
    for cluster in geo_cluster_split.geo_clusters:
        if cluster.identifier == str(clusterId):
            impact_tweets = getDataPointsInCluster(cluster,Data("online_impact"))
            geo_tweets = getDataPointsInCluster(cluster,Data("online_geo"))
            user_tweets = getDataPointsInCluster(cluster,Data("online_users"))
            _,uniqueAllTweets,_ = calculateTweetsStats(impact_tweets,geoTweets=geo_tweets,userTweets=user_tweets)
            return uniqueAllTweets
        
def getEventsOfCluster(clusterId):
    geo_cluster_split = Geo_Clusters("universities")
    for cluster in geo_cluster_split.geo_clusters:
        if cluster.identifier == str(clusterId):
            return getDataPointsInCluster(cluster,Data("offline_checked"))

def calculateInvolvedUsersForClusterAtProtestDay(uniqueAllUsers,protestDate):
    numberOfInvolvedUsers = 0
    data = {}
    protestDay = int(str(protestDate)[6:])
    if protestDay == 6:
        protestDay = 14
    for day in range(12,31):
        for user in uniqueAllUsers.keys():
            if str(uniqueAllUsers[user]) == f"201510{day}":
                numberOfInvolvedUsers += 1
        data.update({day:numberOfInvolvedUsers})
    return data[protestDay],data[protestDay-1]

def calculateTweetsForClusterAtProtestDay(uniqueAllTweets,protestDate):
    numberOfTweets = 0
    data = {}
    protestDay = int(str(protestDate)[6:])
    if protestDay == 6:
        protestDay = 14
    for day in range(12,31):
        for tweet in uniqueAllTweets:
            if str(tweet[2]) == f"201510{day}":
                numberOfTweets += 1
        data.update({day:numberOfTweets})
    print(data)
    return data[protestDay],data[protestDay-1]

def calculateDayWith1PercentStudents(uniqueAllUsers,numberOfStudents,ratio=0.01):
    data = {}
    numberOfInvolvedUsers = 0
    for day in range(12,31):
        for user in uniqueAllUsers.keys():
            if str(uniqueAllUsers[user]) == f"201510{day}":
                numberOfInvolvedUsers += 1
        data.update({day:numberOfInvolvedUsers})
    for day in range(12,31):
        if data[day] >= numberOfStudents*ratio:
            return day
    return None

def calculateDayWithXPercentRetweets(uniqueAllTweets,thresholdDay,thresholdTweetAmount):
    data = {}
    return_data = {}
    decideOnThreshold1 = False
    if thresholdDay is not None:
        decideOnThreshold1 = True

    for day in range(12,31):
        numberOfRetweets = 0
        numberOfAllTweets = 0
        for tweet in uniqueAllTweets:
            if str(tweet[2]) == f"201510{day}":
                numberOfAllTweets += 1
                if tweet[6] != "original":
                    numberOfRetweets += 1
        if decideOnThreshold1:
            if day >= thresholdDay and numberOfAllTweets > 0:
                data.update({day:numberOfRetweets/numberOfAllTweets})
            else:
                data.update({day:0})
        else:
            if numberOfAllTweets >= thresholdTweetAmount:
                data.update({day:numberOfRetweets/numberOfAllTweets})
            else:
                data.update({day:0})
    for percentage in range(0,100,5):
        added = False
        for day in range(12,31):
            if data[day] >= percentage/100:
                return_data.update({percentage:day})
                added = True
                break
        if not added:
            return_data.update({percentage:42})
    return return_data
    
def calculateUserGraph(uniqueAllUsers,offline_data,cluster,dateOfFirstProtest):
    data = {}
    derivation = {}
    numberOfInvolvedUsers = 0
    for day in range(12,31):
        for user in uniqueAllUsers.keys():
            if str(uniqueAllUsers[user]) == f"201510{day}":
                numberOfInvolvedUsers += 1
        data.update({day:numberOfInvolvedUsers})
    for day in range(12,31):
        if day == 12:
            derivation.update({day:0})
        else:
            derivation.update({day:data[day]-data[day-1]})
    dayOfFirstProtest = int(str(dateOfFirstProtest)[6:])
    if dayOfFirstProtest == 6:
        dayOfFirstProtest = 14
    growthFactor1DayBeforeProtest = data[dayOfFirstProtest-1]/data[dayOfFirstProtest-2]
    if dayOfFirstProtest >= 15 and data[dayOfFirstProtest-3] != 0:
        growthFactor2DaysBeforeProtest = data[dayOfFirstProtest-1]/data[dayOfFirstProtest-3]
    else: 
        growthFactor2DaysBeforeProtest = None
    if dayOfFirstProtest >= 16 and data[dayOfFirstProtest-4] != 0:
        growthFactor3DaysBeforeProtest = data[dayOfFirstProtest-1]/data[dayOfFirstProtest-4]
    else:
        growthFactor3DaysBeforeProtest = None

    growthFactor = {}
    for day in range(12,31):
        if day == 12:
            growthFactor.update({day:0})
        else:
            if data[day-1] == 0:
                growthFactor.update({day:0})
            else:
                growthFactor.update({day:data[day]/data[day-1]})

    vis.visualizeUserGrowthOverTime(data,growthFactor,offline_data,cluster.identifier,cluster.name)
    print(f"User growth on day before protest: {growthFactor1DayBeforeProtest}")
    print(f"User growth on two days before protest: {growthFactor2DaysBeforeProtest}")
    print(f"User growth on three days before protest: {growthFactor3DaysBeforeProtest}")
    return growthFactor1DayBeforeProtest,growthFactor2DaysBeforeProtest,growthFactor3DaysBeforeProtest

def reduceAllTweetsToTimeFrame(tweets,startDay=12,endDay=30):
    reducedTweets = []
    for tweet in tweets:
        date = tweet[2][:10].replace("-","")
        if int(date) >= int(f"201510{startDay}") and int(date) <= int(f"201510{endDay}"):
            reducedTweets.append(tweet)
    return reducedTweets

def checkTweetDuplicates():
    all_tweets = md.getAllTweets()
    # with open("data/all_tweets.json","w") as f:
    #     json.dump(all_tweets,f)
    allUniqueTweets = []
    allUniqueTweetsOfCategory = {}
    all_tweets = json.load(open("data/all_tweets.json","r"))
    for category in ["impact_tweets","geo_tweets","user_tweets"]:
        print(category)
        all_tweets[category] = reduceAllTweetsToTimeFrame(all_tweets[category])
        print(len(all_tweets[category]))

        for idx,tweet in enumerate(all_tweets[category]):
            if idx % 1000 == 0:
                print(f"{idx}/{len(all_tweets[category])}")
            if tweet[2] == 0 and int(tweet[0]) not in allUniqueTweets:
                allUniqueTweets.append(int(tweet[0]))
                if category not in allUniqueTweetsOfCategory.keys():
                    allUniqueTweetsOfCategory.update({category:[]})
                allUniqueTweetsOfCategory[category].append(tweet[0])
        print(len(allUniqueTweets))
    print(len(allUniqueTweets))

    with open("analysis/anas/afterMovementNew/tweetAmountAnalysis/allUniqueTweets.json","w") as f:
        json.dump(allUniqueTweets,f)
    with open("analysis/anas/afterMovementNew/tweetAmountAnalysis/allUniqueTweetsOfCategory.json","w") as f:
        json.dump(allUniqueTweetsOfCategory,f)

def analyseWithinFiles():
    allTweetsWithLocation = {}
    duplicates = {}
    tweetUsageStats = {}
    folder = "analysis/withinChecksAfMvmNewCorrectUserCast/universities"
    #clusterIndices = ['CPUT','CUT','DUT','MUT','NMU','NWU','RU','SMU','SPU','SU','TUT','UCT','UFH','UFS','UJ','UKZN','UL','UMP','UNISA','UP','UV','UWC','UZ','VUT','WITS','WSU']
    clusterIndices = range(0,27)
    for clusterIdx in clusterIndices:
        if clusterIdx != 17:
            for location_type in ["impact","geo","users"]:
                with open(f"{folder}/{clusterIdx}/online_{location_type}","r") as f:
                    lines = f.readlines()
                    constructedTweets = []
                    for line in lines:
                        tweet_id = line.split("\t")[3]
                        date = line.split("\t")[2]
                        constructedTweets.append([tweet_id,date,date])
                    reducedTweets = reduceAllTweetsToTimeFrame(constructedTweets)
                    for tweet in reducedTweets:
                        tweet_id = tweet[0]
                        key = f"{tweet_id}_{clusterIdx}"
                        if tweet_id not in tweetUsageStats:
                            tweetUsageStats.update({tweet_id:0})
                        tweetUsageStats[tweet_id] += 1
                        if key not in allTweetsWithLocation.keys():
                            allTweetsWithLocation.update({key:location_type})
                        elif key not in duplicates.keys():
                            duplicates.update({key:[location_type]})
                        else:
                            duplicates[key].append(location_type)
                        
    print(len(allTweetsWithLocation.keys()))
    print(len(duplicates.keys()))
    # with open("analysis/anas/uniCircles/tweetAmountAnalysis/allTweetsWithLocationReduced.json","w") as f:
    #     json.dump(allTweetsWithLocation,f)
    # with open("analysis/anas/uniCircles/tweetAmountAnalysis/WithinDuplicatesReduced.json","w") as f:
    #     json.dump(duplicates,f)
    print(len(tweetUsageStats.keys()))
    #print max and average of tweetUsageStats
    print(f"Average tweet usage: {sum(tweetUsageStats.values())/len(tweetUsageStats.keys())}")

def compareWithinFilesWithAllTweets():
    all_tweets = json.load(open("analysis/anas/afterMovementNew/tweetAmountAnalysis/allUniqueTweets.json","r"))
    allTweetsWithLocation = json.load(open("analysis/anas/afterMovementNew/tweetAmountAnalysis/allTweetsWithLocation.json","r"))
    counter = 0
    tweetIdsLocations = [x[:18] for x in allTweetsWithLocation.keys()]
    missingTweets = []

    for idx,tweet_id in enumerate(all_tweets):
        if idx % 1000 == 0:
            print(f"{idx}/{len(all_tweets)}")
        if str(tweet_id) not in tweetIdsLocations:
            counter += 1
            print(tweet_id)
            missingTweets.append(tweet_id)
    print(counter)
    with open("analysis/anas/afterMovementNew/tweetAmountAnalysis/missingTweets.json","w") as f:
        json.dump(missingTweets,f)

def duplicateCheckMissingTweets():
    missingTweets = json.load(open("analysis/anas/afterMovementNew/tweetAmountAnalysis/afterUserCast/allTweetsWithLocationReduced.json","r"))
    print(len(missingTweets))
    tweetIdsLocations = [x.split("_")[0] for x in missingTweets.keys()]
    withoutDuplicates = set(tweetIdsLocations)
    print(len(withoutDuplicates))
    allTweets = json.load(open("analysis/anas/afterMovementNew/tweetAmountAnalysis/allUniqueTweets.json","r"))
    allTweets = [str(x) for x in allTweets]
    tweetsNotInAllTweetsButLocation = []
    tweetsInAllTweetsButNotLocation = []

    # for idx,tweet_id in enumerate(allTweets):
    #     if idx % 1000 == 0:
    #         print(f"{idx}/{len(allTweets)}")
    #     if str(tweet_id) not in withoutDuplicates:
    #         tweetsInAllTweetsButNotLocation.append(tweet_id)
    # print(len(tweetsInAllTweetsButNotLocation))
    # with open("analysis/anas/afterMovementNew/tweetAmountAnalysis/afterUserCast/tweetsInAllTweetsButNotLocation.json","w") as f:
    #     json.dump(tweetsInAllTweetsButNotLocation,f)

    for idx,tweet_id in enumerate(list(withoutDuplicates)):
        if idx % 1000 == 0:
            print(f"{idx}/{len(withoutDuplicates)}")
        if tweet_id not in allTweets:
            tweetsNotInAllTweetsButLocation.append(tweet_id)
    print(len(tweetsNotInAllTweetsButLocation))
    with open("analysis/anas/afterMovementNew/tweetAmountAnalysis/afterUserCast/tweetsNotInAllTweetsButLocation.json","w") as f:
        json.dump(tweetsNotInAllTweetsButLocation,f)

def countTweetsinTimeFrame():
    all_tweets = md.fetchAllTweetUserLocations(inSouthAfrica=False,removeWrongSouthAfricaLocation=False)
    reduced_tweets = reduceAllTweetsToTimeFrame(all_tweets)
    print(f"Number of tweets in all tweets: {len(all_tweets)}")
    print(f"Number of tweets in time frame: {len(reduced_tweets)}")

    #Remove wrong South Africa location
    all_tweets = md.fetchAllTweetUserLocations(inSouthAfrica=True,removeWrongSouthAfricaLocation=True)
    reduced_tweets = reduceAllTweetsToTimeFrame(all_tweets)
    print(f"Number of tweets in all tweets: {len(all_tweets)}")
    print(f"Number of tweets in time frame: {len(reduced_tweets)}")

def countGeoTweetsinTimeFrame():
    print("GeoLocation")
    all_tweets = md.fetchAllTweetGeoLocations(inSouthAfrica=True)
    reduced_tweets = reduceAllTweetsToTimeFrame(all_tweets)
    print(f"Number of tweets in all tweets: {len(all_tweets)}")
    print(f"Number of tweets in time frame: {len(reduced_tweets)}")

def countImpactTweetsinTimeFrame():
    print("Impact")
    all_tweets = md.fetchAllTweetImpactLocations(inSouthAfrica=False)
    reduced_tweets = reduceAllTweetsToTimeFrame(all_tweets)
    print(f"Number of tweets in all tweets: {len(all_tweets)}")
    print(f"Number of tweets in time frame: {len(reduced_tweets)}")

def getWKTofOnlineUsers(id):
    geos = {}
    sum = 0
    with open(f"analysis/withinChecksAfMvmNewCorrectUserCast/universities/{id}/online_users","r") as f:
        lines = f.readlines()
        for line in lines:
            lat = line.split("\t")[0]
            lon = line.split("\t")[1]
            coords = f"{lat}_{lon}"
            sum += 1
            if coords not in geos.keys():
                geos.update({coords:[lat,lon,1]})
            else:
                geos[coords][2] += 1
    print(len(geos))
    print(sum)
    with open(f"analysis/withinChecksAfMvmNewCorrectUserCast/universities/{id}/visualizeOnlineUsers.csv","w") as f:
        f.write("lat,lon,amount\n")
        for coord in geos.keys():
            f.write(f"{geos[coord][0]},{geos[coord][1]},{geos[coord][2]}\n")

def geoLocationsToWKT():
    geo_locations = md.fetchAllGeoLocationsWithLatLon()
    with open(f"preprocessing/visualize/geolocations.csv","w") as f:
        f.write("id,lat,lon\n")
        con = md.setupConnection()
        for location in geo_locations:
            id = location[0]
            numberOfTweets = md.getNumberOfTweetsOfGeoLocation(con,id)
            f.write(f"{id},{location[1]},{location[2]},{numberOfTweets}\n")

def countEventsInWithinFiles():
    folder = "analysis/withinChecksAfMvmNewCorrectUserCast/universities"
    all_events = []
    for clusterIdx in range(0,27):
        if clusterIdx != 17:
            with open(f"{folder}/{clusterIdx}/offline_checked","r") as f:
                lines = f.readlines()
                for line in lines:
                    event_id = line.split("\t")[3].replace("\n","")
                    all_events.append(event_id)
    print(len(all_events))
    print(all_events)

if __name__ == '__main__':
    logfile = "log/analysis/ana1.log"
    #analyseSouthAfrica()
    #generateTweetsOverTimeOverview()
    #fillAnalysisTable(logfile,False,"universities")
    runAnalysis(logfile,False,"universities")
    #checkTweetDuplicates()
    #compareWithinFilesWithAllTweets()
    #duplicateCheckMissingTweets()
    #analyseWithinFiles()
    #getWKTofOnlineUsers('24')
    #geoLocationsToWKT()
    #countEventsInWithinFiles()
    #analyseWithinFiles()

