import requests
import os
import json
import pandas as pd
import csv
import datetime
import dateutil.parser
import unicodedata
import time
from datetime import datetime

def auth():
    return 'AAAAAAAAAAAAAAAAAAAAADWMMwEAAAAAr1xcrbJE9XBLmld1kScxkMhEAY0%3Dek8AbQN3844XGHppL259raKbjTrV3u8nV1VJ2Zbvpmt6q6DsHI'

def create_headers(bearer_token):
    headers = {"Authorization": "Bearer {}".format(bearer_token)}
    return headers


def create_urlForTweetSearch(keyword, start_date, end_date, max_results = 10):
    search_url = "https://api.twitter.com/2/tweets/search/all" #Change to the endpoint you want to collect data from

    #change params based on the endpoint you are using
    query_params = {'query': keyword,
                    'start_time': start_date,
                    'end_time': end_date,
                    'max_results': max_results,
                    'expansions': 'author_id,in_reply_to_user_id,geo.place_id',
                    'tweet.fields': 'id,text,author_id,in_reply_to_user_id,geo,conversation_id,created_at,lang,public_metrics,referenced_tweets,reply_settings,source',
                    'user.fields': 'id,name,username,created_at,description,public_metrics,verified,location',
                    'place.fields': 'full_name,id,country,country_code,geo,name,place_type',
                    'next_token': {}}
    return (search_url, query_params)

def create_urlForUserRetweetSearch(tweet_id, max_results = 10):
    #search_url = f"https://api.twitter.com/2/tweets/{tweet_id}/retweeted_by"
    search_url = f"https://api.twitter.com/2/tweets/{tweet_id}/liking_users"

    #change params based on the endpoint you are using
    query_params = {'max_results': max_results,
                    'tweet.fields': 'attachments,author_id,context_annotations,conversation_id,created_at,edit_controls,entities,geo,id,in_reply_to_user_id,lang,public_metrics,possibly_sensitive,referenced_tweets,reply_settings,source,text,withheld',
                    'user.fields': 'id,name,username,created_at,description,public_metrics,verified,location',
                    'pagination_token': {}}
    return (search_url, query_params)

def connect_to_endpoint(url, headers, params, logfile, next_token = None):
    params['pagination_token'] = next_token   #params object received from create_url function
    response = requests.request("GET", url, headers = headers, params = params)
    log("Endpoint Response Code: " + str(response.status_code), logfile)
    if response.status_code != 200:
        log(f"Error {response.status_code} {response.text}", logfile)
        raise Exception(response.status_code, response.text)
    return response.json()


def createFile():
    csvFile = open("tweets.csv", "a", newline="", encoding='utf-8')
    csvWriter = csv.writer(csvFile)

    #Create headers for the data you want to save, in this example, we only want save these columns in our dataset
    csvWriter.writerow(['author id', 'created_at', 'geo', 'id','lang', 'like_count', 'quote_count', 'reply_count','retweet_count','source','tweet'])
    csvFile.close()

def append_to_csv(json_response, fileName):
    counter = 0

    #Open OR create the target CSV file
    csvFile = open("tweets.csv", "a", newline="", encoding='utf-8')
    csvWriter = csv.writer(csvFile)

    #Loop through each tweet
    for tweet in json_response['data']:
        author_id = tweet['author_id']

        created_at = dateutil.parser.parse(tweet['created_at'])

        if ('geo' in tweet):   
            geo = tweet['geo']['place_id']
        else:
            geo = " "

        if 'referenced_tweets' in tweet:
            referenced_tweets_type = tweet['referenced_tweets']['type']

        tweet_id = tweet['id']
        lang = tweet['lang']

        retweet_count = tweet['public_metrics']['retweet_count']
        reply_count = tweet['public_metrics']['reply_count']
        like_count = tweet['public_metrics']['like_count']
        quote_count = tweet['public_metrics']['quote_count']
        impression_count = tweet['public_metrics']['impression_count']

        source = tweet['source']
        text = tweet['text']
        
        # Assemble all data in a list
        res = [author_id, created_at, geo, tweet_id, lang, like_count, quote_count, impression_count, reply_count, retweet_count, source, text]
        
        # Append the result to the CSV file
        csvWriter.writerow(res)
        counter += 1

    # When done, close the CSV file
    csvFile.close()

    # Print the number of tweets for this iteration
    print("# of Tweets added from this response: ", counter) 

def log(text,logfile):
    print(text)
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S.%f")[:-3]
    with open(logfile,"a") as logfile:
        logfile.write(f"{current_time}   {text}\n")

def getAllTweetIdsWithLikes(filename,logfile):
    tweet_ids = []
    like_stats = []
    with open(f"data/{filename}.json", "r") as json_file:
        data = json.load(json_file)
        for loop in data:
            for tweet in loop["data"]:
                if tweet["public_metrics"]["like_count"] > 0:
                    like_stats.append(tweet["public_metrics"]["like_count"])
                    tweet_id = tweet["id"]
                    tweet_ids.append(tweet_id)
    log(f"Total number of tweets with likes: {len(tweet_ids)}",logfile)
    log(f"Total number of likes: {sum(like_stats)}",logfile)
    return tweet_ids,like_stats

def getTweets(keyword,start_time,end_time, max_results):
    loop_count = 0
    result_count = 0
    next_token = None
    filename = f"{keyword[1:]}_{start_time[:10]}_{end_time[:10]}"
    logfile = f"log/{filename}.log"
    while True:
        log(f"Start of loop {loop_count}",logfile)

        next_token_exists = True

        url = create_urlForTweetSearch(keyword,start_time,end_time,max_results)
        json_response = connect_to_endpoint(url[0],headers,url[1], logfile, next_token)
        log(f"Result count {loop_count}: {json_response['meta']['result_count']}",logfile)
        result_count += json_response['meta']['result_count']

        if 'next_token' in json_response['meta']:
            log(f"Next_token {loop_count}: {json_response['meta']['next_token']}", logfile)
            next_token = json_response['meta']['next_token']
        else:
            log("No next token. End of search reached.", logfile)
            next_token_exists = False

        with open(f"data/{filename}.json","a") as f:
            if loop_count == 0:
                f.write("[")
            json.dump(json_response, f)
            if next_token_exists:
                f.write(",")
            else:
                f.write("]")
            log("Data added to file", logfile)
            
        log(f"End of loop {loop_count}",logfile)
        log("--------------------------------------------------------", logfile)
        if next_token_exists:
            loop_count += 1
            time.sleep(5)
        else:
            log("--------------------------------------------------------", logfile)
            log(f"{result_count} results found in {loop_count+1} loops", logfile)
            log("Exit", logfile)
            exit()

def getLikingUsers(keyword, start_time, end_time, filename=None):
    if filename == None:
        filename = f"{keyword[1:]}_{start_time[:10]}_{end_time[:10]}"
    logfile = f"log/likingUsers/{filename}_5.log"
    os.mkdir(f"data/likingUsers/{filename}_5")
    total_result_count = 0
    error_likes_missing = 0
    tweet_ids,like_stats = getAllTweetIdsWithLikes(filename,logfile)
    start_time = 0
    request_counter = 0
    start_time_set = False

    for tweet_id_counter, tweet_id in enumerate(tweet_ids):
        log("========================================================", logfile)
        log(f"Tweet id: {tweet_id}. ID {tweet_id_counter} of {len(tweet_ids)} with {like_stats[tweet_id_counter]} likes",logfile)
        loop_count = 0
        result_count = 0
        next_token = None
        while True:
            log(f"Start of loop {loop_count}",logfile)
            next_token_exists = True
            url = create_urlForUserRetweetSearch(tweet_id, max_results)

            if not start_time_set:
                start_time = time.time()
                start_time_set = True
            
            current_time = time.time()
            if request_counter == 75 and current_time-start_time < 900:
                log(f"Sleeping for {900-current_time+start_time} seconds", logfile)
                time.sleep(900-current_time+start_time)
                request_counter = 0
                start_time = time.time()
                start_time_set = True
            json_response = connect_to_endpoint(url[0],headers,url[1], logfile, next_token)
            request_counter += 1
            if "errors" in json_response:
                log(f"Error: {json_response['errors']}", logfile)
                error_likes_missing += like_stats[tweet_id_counter]
                break
            if "meta" not in json_response:
                print("Error: No meta in json response")

            log(f"Result count in loop {loop_count}: {json_response['meta']['result_count']}",logfile)
            result_count += json_response['meta']['result_count']

            if 'next_token' in json_response['meta']:
                log(f"Next_token in loop {loop_count}: {json_response['meta']['next_token']}", logfile)
                next_token = json_response['meta']['next_token']
            else:
                log("No next token. End of search reached.", logfile)
                next_token_exists = False

            with open(f"data/likingUsers/{filename}_5/{tweet_id}.json","a") as f:
                if loop_count == 0:
                    f.write("[")
                json.dump(json_response, f)
                if next_token_exists:
                    f.write(",")
                else:
                    f.write("]")
                log("Data added to file", logfile)
                
            log(f"End of loop {loop_count}",logfile)
            log("--------------------------------------------------------", logfile)
            if next_token_exists:
                loop_count += 1
                time.sleep(5)
            else:
                log("--------------------------------------------------------", logfile)
                log(f"{result_count} users of {like_stats[tweet_id_counter]} likes found in {loop_count+1} loops", logfile)
                log(f"Exit tweet id {tweet_id}", logfile)
                total_result_count += result_count
                break
    log(f"Total number of users found: {total_result_count} of {sum(like_stats)}, {error_likes_missing} likes missing due to authoriztation errors", logfile)

def getAllLikingUsers():
    for filename in os.listdir("data/hashtags"):
        getLikingUsers(filename=filename[:-5])


if __name__ == '__main__':
    headers = create_headers(auth())
    keyword = "#FeesMustFall"
    start_time = '2015-10-12T00:00:00Z'
    end_time = '2015-10-13T00:00:00Z'
    max_results = 100

    #getTweets(keyword, start_time, end_time, max_results)

    
    #getLikingUsers(keyword, start_time, end_time)
    #getAllLikingUsers()
    # next_token = None
    # filename = f"{keyword[1:]}_{start_time[:10]}_{end_time[:10]}"

    # filename = f"{keyword[1:]}_{start_time[:10]}_{end_time[:10]}"
    # logfile = f"log/likingUsers/{filename}.log"


    # url = create_urlForUserRetweetSearch('654405233838305281', max_results)
    # json_response = connect_to_endpoint(url[0],headers,url[1], logfile, next_token)
    # print(json_response)

    

    